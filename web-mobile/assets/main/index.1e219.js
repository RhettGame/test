window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  AdBanner: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "64dcajS8stDwamkkKsbYAUa", "AdBanner");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.UMAdBanner = exports.TTAdBanner = exports.NoneAdBanner = exports.AdBanner = exports.BannerStyle = void 0;
    var BannerStyle;
    (function(BannerStyle) {
      BannerStyle[BannerStyle["Center"] = 0] = "Center";
      BannerStyle[BannerStyle["Left"] = 1] = "Left";
      BannerStyle[BannerStyle["Right"] = 2] = "Right";
    })(BannerStyle = exports.BannerStyle || (exports.BannerStyle = {}));
    var AdBanner = function() {
      function AdBanner() {}
      return AdBanner;
    }();
    exports.AdBanner = AdBanner;
    var NoneAdBanner = function() {
      function NoneAdBanner() {}
      NoneAdBanner.prototype.Init = function() {};
      NoneAdBanner.prototype.Show = function(style) {};
      NoneAdBanner.prototype.Hide = function() {};
      return NoneAdBanner;
    }();
    exports.NoneAdBanner = NoneAdBanner;
    var TTAdBanner = function() {
      function TTAdBanner(id) {
        this.AdId = id;
        this.Init();
      }
      TTAdBanner.prototype.Init = function() {
        var _this = this;
        var _a = tt.getSystemInfoSync(), windowWidth = _a.windowWidth, windowHeight = _a.windowHeight;
        this.ADBanner = tt.createBannerAd({
          adUnitId: this.AdId,
          style: {
            width: windowWidth
          }
        });
        this.ADBanner.onError(function(err) {
          console.log("banner \u9519\u8bef", err);
        });
        this.ADBanner.onLoad(function() {
          console.log("banner \u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
        });
        this.ADBanner.onResize(function(size) {
          _this.ADBanner.style.top = windowHeight - size.height;
          _this.ADBanner.style.left = (windowWidth - size.width) / 2;
        });
        this.ADBanner.hide();
      };
      TTAdBanner.prototype.Show = function(style) {
        this.ADBanner && this.ADBanner.show();
      };
      TTAdBanner.prototype.Hide = function() {
        this.ADBanner && this.ADBanner.hide();
      };
      return TTAdBanner;
    }();
    exports.TTAdBanner = TTAdBanner;
    var UMAdBanner = function() {
      function UMAdBanner(umsdk, slotId) {
        this.BannerStyle = BannerStyle.Center;
        this._showBannerIndex = 0;
        var _a = wx.getSystemInfoSync(), windowWidth = _a.windowWidth, windowHeight = _a.windowHeight;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this._umsdk = umsdk;
        this._slotId = slotId;
        this._bannerMap = [];
      }
      UMAdBanner.prototype.Init = function() {
        var _this = this;
        var _a = tt.getSystemInfoSync(), windowWidth = _a.windowWidth, windowHeight = _a.windowHeight;
        this.ADBanner = tt.createBannerAd({
          adUnitId: this._slotId,
          style: {
            width: windowWidth
          }
        });
        this.ADBanner.onError(function(err) {
          console.log("banner \u9519\u8bef", err);
        });
        this.ADBanner.onLoad(function() {
          console.log("banner \u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
        });
        this.ADBanner.onResize(function(size) {
          _this.ADBanner.style.top = windowHeight - size.height;
          _this.ADBanner.style.left = (windowWidth - size.width) / 2;
        });
        this.ADBanner.hide();
      };
      UMAdBanner.prototype.Show = function(style) {
        console.log("Banner Show");
        this.BannerStyle = style;
        this.ADBanner && this.ADBanner.show().then(function(res) {
          console.log(95, "Banner Already Show");
        }).catch(function(err) {
          console.log(91, err);
        });
      };
      UMAdBanner.prototype.Hide = function() {};
      UMAdBanner.prototype._setStyle = function(adBanner, style, height, width) {
        void 0 === height && (height = 0);
        void 0 === width && (width = 0);
        this.BannerStyle = style;
        0 != height && (this.height = height);
        0 != width && (this.width = width);
        if (adBanner) switch (this.BannerStyle) {
         case BannerStyle.Center:
          adBanner.style.top = this.windowHeight - height;
          adBanner.style.left = (this.windowWidth - width) / 2;
          break;

         case BannerStyle.Left:
          adBanner.style.top = this.windowHeight - height;
          adBanner.style.left = 0;
          break;

         case BannerStyle.Right:
          adBanner.style.top = this.windowHeight - height;
          adBanner.style.left = this.windowWidth - width;
        }
      };
      return UMAdBanner;
    }();
    exports.UMAdBanner = UMAdBanner;
    cc._RF.pop();
  }, {} ],
  AdInsert: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "14e68QkuclEbabSuAcGl6D9", "AdInsert");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.UMAdInsert = exports.TTAdInsert = exports.NoneAdInsert = exports.AdInsert = void 0;
    var AdInsert = function() {
      function AdInsert() {}
      return AdInsert;
    }();
    exports.AdInsert = AdInsert;
    var NoneAdInsert = function() {
      function NoneAdInsert() {}
      NoneAdInsert.prototype.Show = function() {};
      NoneAdInsert.prototype.Init = function(time) {};
      return NoneAdInsert;
    }();
    exports.NoneAdInsert = NoneAdInsert;
    var TTAdInsert = function() {
      function TTAdInsert(id) {
        this.AdId = id;
      }
      TTAdInsert.prototype.Show = function() {};
      TTAdInsert.prototype.Init = function(time) {};
      return TTAdInsert;
    }();
    exports.TTAdInsert = TTAdInsert;
    var UMAdInsert = function() {
      function UMAdInsert(umsdk, id, slotId) {
        this._time = 5;
        this._canshow = false;
        this._umsdk = umsdk;
        this._id = id;
        this._slotId = slotId;
      }
      UMAdInsert.prototype.Init = function(time) {
        var _this = this;
        this.Insert = this._umsdk.createInterstitialAd({
          adUnitId: this._id,
          slotId: this._slotId
        });
        this.Insert.onError(function(err) {
          console.log("\u63d2\u5c4f\u5e7f\u544a\u52a0\u8f7d\u5931\u8d25 : " + err.errMsg + err.errCode);
          setTimeout(function() {
            _this.Insert.load();
          }, 1e3);
        });
        this.Insert.onClose(function() {
          console.log("\u63d2\u5c4f\u5e7f\u544a\u5173\u95ed");
          _this.Insert.load();
        });
        this.Insert.onLoad(function() {
          console.log("\u63d2\u5c4f\u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
        });
        this.Insert.load();
        this._time = time;
        this._canshow = true;
      };
      UMAdInsert.prototype.Show = function() {
        var _this = this;
        var _a;
        if (this._canshow) {
          null === (_a = this.Insert) || void 0 === _a ? void 0 : _a.show();
          this._canshow = false;
          setTimeout(function() {
            _this._canshow = true;
          }, 1e3 * this._time);
        }
      };
      return UMAdInsert;
    }();
    exports.UMAdInsert = UMAdInsert;
    cc._RF.pop();
  }, {} ],
  AdRecord: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "28591nkX7RBp5Hc1kJzUOte", "AdRecord");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.TTAdRecord = exports.NoneAdRecord = exports.AdRecord = void 0;
    var AdRecord = function() {
      function AdRecord() {}
      return AdRecord;
    }();
    exports.AdRecord = AdRecord;
    var NoneAdRecord = function() {
      function NoneAdRecord() {}
      NoneAdRecord.prototype.Start = function() {};
      NoneAdRecord.prototype.Stop = function() {};
      NoneAdRecord.prototype.Share = function(success, fail) {
        success();
      };
      NoneAdRecord.prototype.GetRewardTime = function() {
        return 300;
      };
      return NoneAdRecord;
    }();
    exports.NoneAdRecord = NoneAdRecord;
    var TTAdRecord = function() {
      function TTAdRecord(data) {
        this.Topics = [ "" ];
        this.title = data.title;
        this.desc = data.desc;
        this.Topics = data.topics;
        var gameRecorderManager = tt.getGameRecorderManager();
        gameRecorderManager.onStart(function() {
          console.log("\u5f55\u5c4f\u5f00\u59cb");
        });
        gameRecorderManager.onPause(function() {
          console.log("\u5f55\u5c4f\u6682\u505c");
        });
        gameRecorderManager.onResume(function() {
          console.log("\u5f55\u5c4f\u6062\u590d");
        });
        gameRecorderManager.onStop(function(res) {
          console.log("\u5f55\u5c4f\u505c\u6b62" + res.videoPath);
          this.videoPath = res.videoPath;
        }.bind(this));
      }
      TTAdRecord.prototype.Start = function() {
        var gameRecorderManager = tt.getGameRecorderManager();
        gameRecorderManager.stop();
        setTimeout(function() {
          gameRecorderManager.start({
            duration: 300
          });
        }, 100);
      };
      TTAdRecord.prototype.Stop = function() {
        var gameRecorderManager = tt.getGameRecorderManager();
        gameRecorderManager.stop();
      };
      TTAdRecord.prototype.Share = function(success, fail) {
        null != this.videoPath ? tt.shareAppMessage({
          channel: "video",
          title: this.title,
          desc: this.desc,
          query: "",
          extra: {
            withVideoId: true,
            videoPath: this.videoPath,
            videoTopics: this.Topics,
            createChallenge: true
          },
          success: function() {
            console.log("\u5206\u4eab\u89c6\u9891\u6210\u529f");
            success && success();
          },
          fail: function(e) {
            console.log("\u5206\u4eab\u89c6\u9891\u5931\u8d25", e);
            fail && fail();
          }
        }) : console.log("\u5206\u4eab\u89c6\u9891 \u5730\u5740\u4e3a\u7a7a");
      };
      TTAdRecord.prototype.GetRewardTime = function() {
        return 300;
      };
      return TTAdRecord;
    }();
    exports.TTAdRecord = TTAdRecord;
    cc._RF.pop();
  }, {} ],
  AdVideo: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a60e9huR81MW7FDS0xXDkve", "AdVideo");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.UMAdVideo = exports.WXAdVideo = exports.TTAdVideo = exports.NoneAdVideo = exports.AdVideo = void 0;
    var AdVideo = function() {
      function AdVideo() {}
      return AdVideo;
    }();
    exports.AdVideo = AdVideo;
    var NoneAdVideo = function() {
      function NoneAdVideo() {}
      NoneAdVideo.prototype.Show = function(type, success, fail) {
        success();
      };
      NoneAdVideo.prototype.isAvailable = function() {
        return true;
      };
      return NoneAdVideo;
    }();
    exports.NoneAdVideo = NoneAdVideo;
    var TTAdVideo = function() {
      function TTAdVideo(id) {
        var _this = this;
        this.RewardVideoAvalibleAd = false;
        this.AdId = id;
        this.ADVideo = tt.createRewardedVideoAd({
          adUnitId: this.AdId
        });
        this.ADVideo.onError(function(err) {
          console.log("video \u52a0\u8f7d\u5931\u8d25 : " + err.errMsg);
          setTimeout(function() {
            _this.ADVideo.load();
          }, 1e3);
        });
        this.ADVideo.onClose(function(_a) {
          var isEnded = _a.isEnded;
          isEnded ? _this.RewardSuccessCallback && _this.RewardSuccessCallback() : _this.RewardFailCallback && _this.RewardFailCallback();
          _this.Load();
        });
        this.ADVideo.onLoad(function() {
          console.log(" \u6fc0\u52b1\u89c6\u9891 \u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
          _this.RewardVideoAvalibleAd = true;
        });
        this.Load();
      }
      TTAdVideo.prototype.Load = function() {
        this.RewardVideoAvalibleAd = false;
        this.ADVideo.load().then(function() {
          console.log("\u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
        }).catch(function(err) {
          console.log("\u5e7f\u544a\u7ec4\u4ef6\u52a0\u8f7d\u51fa\u73b0\u95ee\u9898", err);
        });
      };
      TTAdVideo.prototype.isAvailable = function() {
        return this.RewardVideoAvalibleAd;
      };
      TTAdVideo.prototype.Show = function(type, success, fail) {
        this.RewardSuccessCallback = success;
        this.RewardFailCallback = fail;
        if (this.isAvailable()) {
          this.ADVideo.show();
          this.RewardVideoAvalibleAd = false;
        }
      };
      return TTAdVideo;
    }();
    exports.TTAdVideo = TTAdVideo;
    var WXAdVideo = function() {
      function WXAdVideo(id) {
        var _this = this;
        this.RewardVideoAvalibleAd = false;
        this.ADVideo = wx.createRewardedVideoAd({
          adUnitId: id
        });
        this.ADVideo.onError(function(err) {
          console.log("video \u52a0\u8f7d\u5931\u8d25 : " + err.errMsg + err.errCode);
          setTimeout(function() {
            _this.ADVideo.load();
          }, 1e3);
        });
        this.ADVideo.onClose(function(res) {
          res.isEnded ? _this.RewardSuccessCallback && _this.RewardSuccessCallback() : _this.RewardFailCallback && _this.RewardFailCallback();
          _this.ADVideo.load();
          _this.RewardVideoAvalibleAd = false;
        });
        this.ADVideo.onLoad(function() {
          console.log(" \u6fc0\u52b1\u89c6\u9891 \u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
          _this.RewardVideoAvalibleAd = true;
        });
      }
      WXAdVideo.prototype.isAvailable = function() {
        console.log(" \u6fc0\u52b1\u89c6\u9891 \u53ef\u7528", this.RewardVideoAvalibleAd);
        return this.RewardVideoAvalibleAd;
      };
      WXAdVideo.prototype.Show = function(type, success, fail) {
        this.RewardSuccessCallback = success;
        this.RewardFailCallback = fail;
        if (this.isAvailable()) {
          this.ADVideo.show();
          this.RewardVideoAvalibleAd = false;
        }
      };
      return WXAdVideo;
    }();
    exports.WXAdVideo = WXAdVideo;
    var UMAdVideo = function() {
      function UMAdVideo(umsdk, id, slotId) {
        var _this = this;
        this.RewardVideoAvalibleAd = false;
        this.ADVideo = umsdk.createRewardedVideoAd({
          adUnitId: id,
          slotId: slotId
        });
        this.ADVideo.onError(function(err) {
          console.log("video \u52a0\u8f7d\u5931\u8d25 : " + err.errMsg + err.errCode);
          setTimeout(function() {
            _this.ADVideo.load();
          }, 1e3);
        });
        this.ADVideo.onClose(function(res) {
          res.isEnded ? _this.RewardSuccessCallback && _this.RewardSuccessCallback() : _this.RewardFailCallback && _this.RewardFailCallback();
          _this.ADVideo.load();
          _this.RewardVideoAvalibleAd = false;
        });
        this.ADVideo.onLoad(function() {
          console.log(" \u6fc0\u52b1\u89c6\u9891 \u5e7f\u544a\u52a0\u8f7d\u6210\u529f");
          _this.RewardVideoAvalibleAd = true;
        });
      }
      UMAdVideo.prototype.isAvailable = function() {
        console.log(" \u6fc0\u52b1\u89c6\u9891 \u53ef\u7528", this.RewardVideoAvalibleAd);
        return this.RewardVideoAvalibleAd;
      };
      UMAdVideo.prototype.Show = function(type, success, fail) {
        this.RewardSuccessCallback = success;
        this.RewardFailCallback = fail;
        if (this.isAvailable()) {
          this.ADVideo.show();
          this.RewardVideoAvalibleAd = false;
        }
      };
      return UMAdVideo;
    }();
    exports.UMAdVideo = UMAdVideo;
    cc._RF.pop();
  }, {} ],
  AddMoneyView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "54f6cw8TglB3KBcJ9E8H2fr", "AddMoneyView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.AddMoneyView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var AddMoneyView = function(_super) {
      __extends(AddMoneyView, _super);
      function AddMoneyView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnClose = void 0;
        _this.labelAdReward = void 0;
        _this.btnAdReward = void 0;
        return _this;
      }
      AddMoneyView.prototype.click_btn_close = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.AddMoneyView);
      };
      AddMoneyView.prototype.click_btn_ad_reward = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u52a0\u91d1\u5e01\u5f39\u7a97", function() {
          GameData_1.GameData.UserData.Money += GameData_1.GameData.AdMoney;
          GameData_1.GameData.UserData.SaveData();
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.AddMoneyView);
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
        });
      };
      __decorate([ property(cc.Button) ], AddMoneyView.prototype, "btnClose", void 0);
      __decorate([ property(cc.Label) ], AddMoneyView.prototype, "labelAdReward", void 0);
      __decorate([ property(cc.Button) ], AddMoneyView.prototype, "btnAdReward", void 0);
      AddMoneyView = __decorate([ ccclass ], AddMoneyView);
      return AddMoneyView;
    }(cc.Component);
    exports.AddMoneyView = AddMoneyView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  AddPowerView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3b44bkmH9pMP6vPRyY+hmuw", "AddPowerView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.AddPowerView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var AddPowerView = function(_super) {
      __extends(AddPowerView, _super);
      function AddPowerView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnClose = void 0;
        _this.labelAdReward = void 0;
        _this.btnAdReward = void 0;
        return _this;
      }
      AddPowerView.prototype.click_btn_close = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.AddPowerView);
      };
      AddPowerView.prototype.click_btn_ad_reward = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u52a0\u4f53\u529b\u5f39\u7a97", function() {
          GameData_1.GameData.UserData.Power += GameData_1.GameData.AdPower;
          GameData_1.GameData.UserData.SaveData();
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.AddPowerView);
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
        });
      };
      AddPowerView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.labelAdReward.string = "" + GameData_1.GameData.AdPower;
        }
      };
      __decorate([ property(cc.Button) ], AddPowerView.prototype, "btnClose", void 0);
      __decorate([ property(cc.Label) ], AddPowerView.prototype, "labelAdReward", void 0);
      __decorate([ property(cc.Button) ], AddPowerView.prototype, "btnAdReward", void 0);
      AddPowerView = __decorate([ ccclass ], AddPowerView);
      return AddPowerView;
    }(cc.Component);
    exports.AddPowerView = AddPowerView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  Analytics: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "eb3d59zfFRBf4RC6xE+GmYv", "Analytics");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.WXAnalytics = exports.TTAnalytics = exports.NoneAnalytics = exports.Analytics = void 0;
    var Analytics = function() {
      function Analytics() {}
      return Analytics;
    }();
    exports.Analytics = Analytics;
    var NoneAnalytics = function() {
      function NoneAnalytics() {}
      NoneAnalytics.prototype.Report = function(eventName, data) {};
      return NoneAnalytics;
    }();
    exports.NoneAnalytics = NoneAnalytics;
    var TTAnalytics = function() {
      function TTAnalytics() {}
      TTAnalytics.prototype.Report = function(eventName, data) {
        console.log("reportAnalytics", eventName, data);
        tt.reportAnalytics(eventName, data);
      };
      return TTAnalytics;
    }();
    exports.TTAnalytics = TTAnalytics;
    var WXAnalytics = function() {
      function WXAnalytics() {}
      WXAnalytics.prototype.Report = function(eventName, data) {
        tt.reportAnalytics(eventName, data);
      };
      return WXAnalytics;
    }();
    exports.WXAnalytics = WXAnalytics;
    cc._RF.pop();
  }, {} ],
  Audio: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "511b8vte5RCWbDkQA/yLABC", "Audio");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.audio = void 0;
    var Audio = function() {
      function Audio() {
        this._isInit = false;
        this.audios = [];
        this.isMute = false;
        this.isMuteBgMusic = false;
        this.isMuteEffectMusic = false;
        this.bgmId = 0;
        this.bgmPlaying = false;
        this.bgmName = null;
        this.keyIds = [];
      }
      Audio.prototype.init = function(isMute, bgmName) {
        void 0 === bgmName && (bgmName = "bgm");
        if (this._isInit) return;
        this._isInit = true;
        this.isMute = isMute;
        this.isMute || this.playBGM(bgmName);
      };
      Audio.prototype.playBGM = function(name) {
        return __awaiter(this, void 0, Promise, function() {
          var audio;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.bgmPlaying = true;
              this.bgmName = name;
              if (this.isMute) return [ 2 ];
              if (this.isMuteBgMusic) return [ 2 ];
              return [ 4, this.getAudio(name) ];

             case 1:
              audio = _a.sent();
              this.bgmId = cc.audioEngine.play(audio.clip, true, audio.volume);
              return [ 2 ];
            }
          });
        });
      };
      Audio.prototype.stopBGM = function() {
        this.bgmPlaying = false;
        if (this.isMute) return;
        cc.audioEngine.stop(this.bgmId);
        cc.audioEngine.uncache(this.bgmId);
        delete this.bgmId;
      };
      Audio.prototype.playSound = function(name, loop, cb) {
        void 0 === loop && (loop = false);
        return __awaiter(this, void 0, Promise, function() {
          var audio, id;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this.isMute) return [ 2 ];
              if (this.isMuteEffectMusic) return [ 2 ];
              return [ 4, this.getAudio(name) ];

             case 1:
              audio = _a.sent();
              id = cc.audioEngine.play(audio.clip, loop, audio.volume);
              this.keyIds.push({
                name: name,
                id: id
              });
              if (loop) return [ 2 ];
              cc.audioEngine.setFinishCallback(id, function() {
                _this.stopSound(name);
                cb && cb();
              });
              return [ 2 ];
            }
          });
        });
      };
      Audio.prototype.stopSound = function(name) {
        var idx = this.keyIds.findIndex(function(data) {
          return name === data.name;
        });
        if (-1 != idx) {
          var effectId = this.keyIds[idx].effectId;
          this.keyIds.splice(idx, 1);
          cc.audioEngine.stop(effectId);
          cc.audioEngine.uncache(effectId);
        }
      };
      Audio.prototype.switchMute = function() {
        this.isMute = !this.isMute;
        if (this.isMute) {
          cc.audioEngine.stopAll();
          cc.audioEngine.uncacheAll();
          delete this.bgmId;
        } else this.bgmPlaying && this.playBGM(this.bgmName);
      };
      Audio.prototype.switchMuteBgMusic = function() {
        this.isMuteBgMusic = !this.isMuteBgMusic;
        this.isMuteBgMusic ? this.stopBGM() : this.playBGM(this.bgmName);
      };
      Audio.prototype.switchMuteEffectMusic = function() {
        this.isMuteEffectMusic = !this.isMuteEffectMusic;
      };
      Audio.prototype.getAudio = function(name) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.audios.find(function(audio) {
                return audio.name === name;
              }) ];

             case 1:
              return [ 2, _a.sent() || this.createAudio(name) ];
            }
          });
        });
      };
      Audio.prototype.createAudio = function(name) {
        var _this = this;
        return new Promise(function(resolve, reject) {
          cc.loader.loadRes("audio/" + name, cc.AudioClip, function(err, res) {
            if (err) {
              reject(null);
              return;
            }
            var audio = Object.assign({}, {
              name: name,
              clip: res,
              volume: 1
            });
            _this.audios.push(audio);
            resolve(audio);
          });
        });
      };
      return Audio;
    }();
    exports.audio = new Audio();
    cc._RF.pop();
  }, {} ],
  BasePipe: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "85497vxXU9EGZwcVEJo2o3u", "BasePipe");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.Liquid = void 0;
    var Joint_1 = require("./Joint");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Liquid;
    (function(Liquid) {
      Liquid[Liquid["Empty"] = 0] = "Empty";
      Liquid[Liquid["Water"] = 101] = "Water";
      Liquid[Liquid["Poison"] = 102] = "Poison";
      Liquid[Liquid["Health"] = 104] = "Health";
      Liquid[Liquid["Fire"] = 103] = "Fire";
      Liquid[Liquid["Ice"] = 105] = "Ice";
    })(Liquid = exports.Liquid || (exports.Liquid = {}));
    var BasePipe = function(_super) {
      __extends(BasePipe, _super);
      function BasePipe() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.Liquid = Liquid.Empty;
        return _this;
      }
      Object.defineProperty(BasePipe.prototype, "Joint", {
        get: function() {
          var _a;
          null == this._Joint && (this._Joint = null === (_a = this.node) || void 0 === _a ? void 0 : _a.getComponentsInChildren(Joint_1.default));
          return this._Joint;
        },
        set: function(v) {
          this._Joint = v;
        },
        enumerable: false,
        configurable: true
      });
      __decorate([ property(Joint_1.default) ], BasePipe.prototype, "Joint", null);
      BasePipe = __decorate([ ccclass ], BasePipe);
      return BasePipe;
    }(cc.Component);
    exports.default = BasePipe;
    cc._RF.pop();
  }, {
    "./Joint": "Joint"
  } ],
  BaseRole: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "feea7hl3MVNO4gP+9OHI81f", "BaseRole");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UIManager_1 = require("../../Core/UIManager");
    var EnumDef_1 = require("../EnumDef");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BaseRole = function(_super) {
      __extends(BaseRole, _super);
      function BaseRole() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.playAni = false;
        _this.Liquid = BasePipe_1.Liquid.Empty;
        return _this;
      }
      Object.defineProperty(BaseRole.prototype, "Ani", {
        get: function() {
          null == this._ani && (this._ani = this.node.getComponentInChildren(dragonBones.ArmatureDisplay));
          return this._ani;
        },
        set: function(v) {
          this._ani = v;
        },
        enumerable: false,
        configurable: true
      });
      BaseRole.prototype.DeadAni = function(aniName) {
        var _this = this;
        this.playAni = true;
        this.Ani.playAnimation("\u5f85\u673a", 3);
        this.Ani.once(dragonBones.EventObject.COMPLETE, function() {
          _this.Ani.playAnimation(aniName, 1);
          _this.Ani.once(dragonBones.EventObject.COMPLETE, function() {
            UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckWin);
          });
        });
      };
      BaseRole.prototype.ChangeAni = function() {
        var _this = this;
        this.playAni = true;
        this.Ani.playAnimation("\u5f85\u673a", 3);
        this.Ani.once(dragonBones.EventObject.COMPLETE, function() {
          _this.Ani.playAnimation("\u53d8\u8eab", 1);
          _this.Ani.once(dragonBones.EventObject.COMPLETE, function() {
            UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.BoomTank, _this);
            UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            _this.Ani.playAnimation("\u53d8\u8eab\u540e\u5f85\u673a", 0);
          });
        });
      };
      __decorate([ property(dragonBones.ArmatureDisplay) ], BaseRole.prototype, "Ani", null);
      BaseRole = __decorate([ ccclass ], BaseRole);
      return BaseRole;
    }(cc.Component);
    exports.default = BaseRole;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "./BasePipe": "BasePipe"
  } ],
  BeginView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "54c22CvrHFEf5d7Q5ETEhyO", "BeginView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.BeginView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var BeginView = function(_super) {
      __extends(BeginView, _super);
      function BeginView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeRole = void 0;
        _this.btnBegin = void 0;
        _this.btnSuit = void 0;
        _this.btnSet = void 0;
        _this.btnSign = void 0;
        _this.btnBuild = void 0;
        _this.btnRoom = void 0;
        _this.btnRank = void 0;
        _this.labelMoneyNum = void 0;
        _this.btnMoney = void 0;
        _this.spritePowerPercent = void 0;
        _this.labelPowerNum = void 0;
        _this.btnPower = void 0;
        _this.labelMission = void 0;
        _this.role = void 0;
        _this._DanceName = [];
        return _this;
      }
      BeginView.prototype.click_btn_begin = function(e) {
        if (GameData_1.GameData.UserData.Power <= 0) UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.LackPowerView); else {
          UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.GameView);
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.BeginView);
        }
        SDKManager_1.SDKManager.ReportAnalytics("\u70b9\u51fb", {
          status: "\u5f00\u59cb\u6e38\u620f",
          mission: GameData_1.GameData.UserData.Mission
        });
      };
      BeginView.prototype.click_btn_suit = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.FashionView);
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.BeginView);
      };
      BeginView.prototype.click_btn_set = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.SetView);
      };
      BeginView.prototype.click_btn_sign = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.SignView);
      };
      BeginView.prototype.click_btn_build = function(e) {};
      BeginView.prototype.click_btn_room = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.RoomView);
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.BeginView);
      };
      BeginView.prototype.click_btn_rank = function(e) {};
      BeginView.prototype.click_btn_money = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.AddMoneyView);
      };
      BeginView.prototype.click_btn_power = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.AddPowerView);
      };
      BeginView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          SDKManager_1.SDKManager.ReportAnalytics("\u4e3b\u9875", {
            status: "\u5c55\u793a"
          });
          this.labelMission.string = "\u5173\u5361" + GameData_1.GameData.UserData.Mission;
          this.PlayRoleAni();
          this.RefreshToken();
          break;

         case EnumDef_1.UIInfo.RefreshToken:
          this.RefreshToken();
          break;

         case EnumDef_1.UIInfo.CloseView:
        }
      };
      BeginView.prototype.RefreshToken = function() {
        this.labelPowerNum.string = GameData_1.GameData.UserData.Power + "/" + GameData_1.GameData.MaxPower;
        this.spritePowerPercent.fillRange = GameData_1.GameData.UserData.Power / GameData_1.GameData.MaxPower;
        this.labelMoneyNum.string = "" + GameData_1.GameData.UserData.Money;
      };
      BeginView.prototype.PlayRoleAni = function() {
        var _this = this;
        if (0 == this._DanceName.length) {
          this._DanceName = [ "\u8df3\u821e1", "\u8df3\u821e2" ];
          Util_1.Util.Random.ArrayBreakOrder(this._DanceName);
        }
        cc.Tween.stopAllByTarget(this.nodeRole);
        this.nodeRole.x = 0;
        var aniName = this._DanceName.shift();
        this.role.timeScale = 1.1;
        var timer = 2 * this.role.timeScale;
        var time = this.role.armature().armatureData.getAnimation(GameData_1.GameData.getSuitName() + aniName).duration * timer / this.role.timeScale;
        if ("\u8df3\u821e2" == aniName) {
          this._DanceName.unshift("\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3", "\u8df3\u821e3");
          cc.tween(this.nodeRole).call(function() {
            _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, 2 * timer);
          }).delay(2 * time).call(function() {
            _this.PlayRoleAni();
          }).start();
        } else "\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3" == aniName ? cc.tween(this.nodeRole).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer / timer);
        }).delay(time / timer).call(function() {
          _this.PlayRoleAni();
        }).start() : "\u8df3\u821e1" == aniName ? cc.tween(this.nodeRole).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: -300
        }).to(0, {
          scaleX: -1
        }).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, 2 * timer);
        }).by(2 * time, {
          x: 600
        }).to(0, {
          scaleX: 1
        }).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: -300
        }).call(function() {
          _this.PlayRoleAni();
        }).start() : cc.tween(this.nodeRole).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: 250
        }).to(0, {
          scaleX: -1
        }).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, 2 * timer);
        }).by(2 * time, {
          x: -500
        }).to(0, {
          scaleX: 1
        }).call(function() {
          _this.role.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: 250
        }).call(function() {
          _this.PlayRoleAni();
        }).start();
      };
      __decorate([ property(cc.Node) ], BeginView.prototype, "nodeRole", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnBegin", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnSuit", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnSet", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnSign", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnBuild", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnRoom", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnRank", void 0);
      __decorate([ property(cc.Label) ], BeginView.prototype, "labelMoneyNum", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnMoney", void 0);
      __decorate([ property(cc.Sprite) ], BeginView.prototype, "spritePowerPercent", void 0);
      __decorate([ property(cc.Label) ], BeginView.prototype, "labelPowerNum", void 0);
      __decorate([ property(cc.Button) ], BeginView.prototype, "btnPower", void 0);
      __decorate([ property(cc.Label) ], BeginView.prototype, "labelMission", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], BeginView.prototype, "role", void 0);
      BeginView = __decorate([ ccclass ], BeginView);
      return BeginView;
    }(cc.Component);
    exports.BeginView = BeginView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  BendsPipe: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2a637D79AZI7qwnU4lnUFpl", "BendsPipe");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameData_1 = require("../data/GameData");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BendsPipe = function(_super) {
      __extends(BendsPipe, _super);
      function BendsPipe() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.Water = [];
        _this.HighLight = [];
        _this._fill = false;
        return _this;
      }
      Object.defineProperty(BendsPipe.prototype, "Ani", {
        get: function() {
          null == this._ani && (this._ani = this.node.getComponent(cc.Animation));
          return this._ani;
        },
        set: function(v) {
          this._ani = v;
        },
        enumerable: false,
        configurable: true
      });
      BendsPipe.prototype.AddJoint = function(Joint) {
        this._fill && this.Liquid != BasePipe_1.Liquid.Empty && this.Output();
      };
      BendsPipe.prototype.Input = function(liquid, joint) {
        this.Liquid = liquid;
        this._From = joint;
        this.Filling();
      };
      BendsPipe.prototype.Output = function() {
        var _this = this;
        this.Joint.forEach(function(e) {
          e != _this._From && e.Output(_this.Liquid);
        });
      };
      BendsPipe.prototype.Filling = function() {
        var _this = this;
        var _a, _b, _c;
        this._fill = false;
        this.HighLight.forEach(function(e) {
          return e.node.active = false;
        });
        if (this.Liquid == BasePipe_1.Liquid.Empty) {
          this.Water.forEach(function(e) {
            return e.active = false;
          });
          this.Ani.stop();
          this.Ani.off(cc.Animation.EventType.FINISHED);
          this.Output();
          return;
        }
        this.Water.forEach(function(e) {
          return e.color = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
        });
        var index = this.Joint.indexOf(this._From);
        var clips = this.Ani.getClips();
        0 == index ? null === (_a = this.Ani) || void 0 === _a ? void 0 : _a.play(clips[0].name, 0) : null === (_b = this.Ani) || void 0 === _b ? void 0 : _b.play(clips[2].name, 0);
        null === (_c = this.Ani) || void 0 === _c ? void 0 : _c.once(cc.Animation.EventType.FINISHED, function() {
          _this._fill = true;
          _this.Output();
          _this.HighLight.forEach(function(e, x) {
            e.node.active = x == index;
            e.startColor = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
            e.endColor = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
          });
        });
      };
      BendsPipe.prototype.RemoveJoint = function(joint) {
        if (joint == this._From) {
          this.Liquid = BasePipe_1.Liquid.Empty;
          this._From = null;
          this.Filling();
        }
      };
      __decorate([ property([ cc.Node ]) ], BendsPipe.prototype, "Water", void 0);
      __decorate([ property([ cc.ParticleSystem ]) ], BendsPipe.prototype, "HighLight", void 0);
      __decorate([ property(cc.Animation) ], BendsPipe.prototype, "Ani", null);
      BendsPipe = __decorate([ ccclass ], BendsPipe);
      return BendsPipe;
    }(BasePipe_1.default);
    exports.default = BendsPipe;
    cc._RF.pop();
  }, {
    "../data/GameData": "GameData",
    "./BasePipe": "BasePipe"
  } ],
  ComicView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "10d2aVMQvFNxrptRygVfr+v", "ComicView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ComicView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var ComicView = function(_super) {
      __extends(ComicView, _super);
      function ComicView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.aniComic = void 0;
        return _this;
      }
      ComicView.prototype.start = function() {
        var _this = this;
        var canUse = false;
        var nowComic = 0;
        var all = this.aniComic.getClips();
        console.log(all[nowComic]);
        this.aniComic.play(all[nowComic].name);
        this.aniComic.on(cc.Animation.EventType.FINISHED, function() {
          canUse = true;
        });
        this.node.on(cc.Node.EventType.TOUCH_END, function() {
          if (canUse) {
            canUse = false;
            nowComic++;
            if (nowComic >= all.length) {
              SDKManager_1.SDKManager.ReportAnalytics("\u5f00\u573a\u52a8\u753b", {
                status: "\u5173\u95ed"
              });
              UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.ComicView);
              UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.GameView);
            } else _this.aniComic.play(all[nowComic].name);
          }
        });
      };
      __decorate([ property(cc.Animation) ], ComicView.prototype, "aniComic", void 0);
      ComicView = __decorate([ ccclass ], ComicView);
      return ComicView;
    }(cc.Component);
    exports.ComicView = ComicView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef"
  } ],
  Config: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "8b0d9YTKsJMuZg9+g5mIihL", "Config");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.config = exports.ConfigType = void 0;
    var ConfigType;
    (function(ConfigType) {
      ConfigType["CheckpointConfig"] = "Checkpoint";
      ConfigType["Model1Config"] = "Model1";
      ConfigType["Model2Config"] = "Model2";
      ConfigType["Model3Config"] = "Model3";
    })(ConfigType = exports.ConfigType || (exports.ConfigType = {}));
    var Config = function() {
      function Config() {
        this._isInit = false;
        this._configList = [];
      }
      Object.defineProperty(Config.prototype, "checkpointConfig", {
        get: function() {
          return this._checkpointConfig;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(Config.prototype, "model1Config", {
        get: function() {
          return this._model1Config;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(Config.prototype, "model2Config", {
        get: function() {
          return this._model2Config;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(Config.prototype, "model3Config", {
        get: function() {
          return this._model3Config;
        },
        enumerable: false,
        configurable: true
      });
      Config.prototype.init = function() {
        var _this = this;
        if (this._isInit) return Promise.resolve();
        this._isInit = true;
        return new Promise(function(resolve, reject) {
          return __awaiter(_this, void 0, void 0, function() {
            return __generator(this, function(_a) {
              switch (_a.label) {
               case 0:
                return [ 4, this.initConfig() ];

               case 1:
                _a.sent();
                resolve();
                return [ 2 ];
              }
            });
          });
        });
      };
      Config.prototype.initConfig = function() {
        var _this = this;
        this._configList.forEach(function(element) {
          _this._loadSingle("config/" + element, cc.JsonAsset, function(res) {
            _this.ParseData(element, res);
          });
        });
      };
      Config.prototype._loadSingle = function(path, asset, callBack) {
        cc.loader.loadRes(path, asset, function(err, res) {
          if (null != err) {
            cc.error(err);
            return;
          }
          console.log("\u5df2\u52a0\u8f7d " + path);
          callBack(res);
        });
      };
      Config.prototype.ParseData = function(dataName, Data) {
        var jsonData = Data.json;
        switch (dataName) {
         case ConfigType.CheckpointConfig:
          this._checkpointConfig = jsonData;
          break;

         case ConfigType.Model1Config:
          this._model1Config = jsonData;
          break;

         case ConfigType.Model2Config:
          this._model2Config = jsonData;
          break;

         case ConfigType.Model3Config:
          this._model3Config = jsonData;
        }
      };
      return Config;
    }();
    exports.config = new Config();
    cc._RF.pop();
  }, {} ],
  Databus: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f75abJoV55LQaWXiWt7i5pL", "Databus");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.databus = void 0;
    var Databus = function() {
      function Databus() {
        this.channel = "WX";
        this.isSubLoad = false;
        this.romEnemyCar = null;
        this.romEnemyCarStyle = null;
        this.isFirstGame = true;
      }
      return Databus;
    }();
    exports.databus = new Databus();
    cc._RF.pop();
  }, {} ],
  EmptyTank: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1104cn9cshLfqDIwpYjTa4m", "EmptyTank");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var EmptyTank = function(_super) {
      __extends(EmptyTank, _super);
      function EmptyTank() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      EmptyTank.prototype.start = function() {
        this.Joint[1].node.groupIndex = 1;
      };
      EmptyTank.prototype.Input = function(liquid, from) {
        this.Liquid = liquid;
        this._From = from;
        this.Filling();
      };
      EmptyTank.prototype.Output = function() {
        var _this = this;
        this.Joint.forEach(function(e) {
          e != _this._From && e.Output(_this.Liquid);
        });
      };
      EmptyTank.prototype.Filling = function() {
        this.Output();
      };
      EmptyTank.prototype.AddJoint = function(selfJoint) {};
      EmptyTank.prototype.RemoveJoint = function(joint) {
        if (joint == this._From) {
          this.Liquid = BasePipe_1.Liquid.Empty;
          this._From = null;
          this.Filling();
        }
      };
      EmptyTank = __decorate([ ccclass ], EmptyTank);
      return EmptyTank;
    }(BasePipe_1.default);
    exports.default = EmptyTank;
    cc._RF.pop();
  }, {
    "./BasePipe": "BasePipe"
  } ],
  EnumDef: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2e351hBaTdEh7XVz19UlzY9", "EnumDef");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.AnalyticsEnum = exports.UIInfo = exports.UIName = exports.ModelType = exports.EffectType = exports.AudioType = exports.AssetBundleEnum = exports.ConfigType = exports.GameErrLan = exports.ShareType = exports.AdRewardType = exports.AdType = void 0;
    var AdType;
    (function(AdType) {
      AdType[AdType["TT"] = 0] = "TT";
      AdType[AdType["WeChat"] = 1] = "WeChat";
      AdType[AdType["QQ"] = 2] = "QQ";
      AdType[AdType["None"] = 3] = "None";
      AdType[AdType["UMWeChat"] = 4] = "UMWeChat";
    })(AdType = exports.AdType || (exports.AdType = {}));
    var AdRewardType;
    (function(AdRewardType) {
      AdRewardType["ResultView"] = "ResultView";
      AdRewardType["GameViewShowTip"] = "GameViewShowTip";
      AdRewardType["GameViewNextMission"] = "GameViewNextMission";
      AdRewardType["CheckoutView"] = "CheckoutView";
      AdRewardType["SuitView"] = "SuitView";
      AdRewardType["SkiilChooseView"] = "SkiilChooseView";
      AdRewardType["SkiilChooseViewItem"] = "SkiilChooseViewItem";
      AdRewardType["CardReward"] = "CardReward";
      AdRewardType["CardSlot"] = "CardSlot";
      AdRewardType["RuneView"] = "RuneView";
      AdRewardType["SaveTip"] = "SaveTip";
      AdRewardType["Revive"] = "Revive";
      AdRewardType["ShootWin"] = "ShootWin";
      AdRewardType["FollowerRevive"] = "FollowerRevive";
      AdRewardType["TokenTip"] = "TokenTip";
      AdRewardType["AddLock"] = "AddLock";
      AdRewardType["CatapultRepair"] = "CatapultRepair";
      AdRewardType["TryOut"] = "TryOut";
      AdRewardType["NewSkinTip"] = "NewSkinTip";
      AdRewardType["QuickUlt"] = "QuickUlt";
      AdRewardType["RedBag"] = "RedBag";
      AdRewardType["GoldCow"] = "GoldCow";
      AdRewardType["SecretLight"] = "SecretLight";
      AdRewardType["FollowerReviveView"] = "FollowerReviveView";
      AdRewardType["LimitRewardView"] = "LimitRewardView";
      AdRewardType["BoxRewardOpen"] = "BoxRewardOpen";
      AdRewardType["BoxRewardTipView"] = "BoxRewardTipView";
      AdRewardType["NewArrowTipView"] = "NewArrowTipView";
      AdRewardType["Bubble"] = "Bubble";
    })(AdRewardType = exports.AdRewardType || (exports.AdRewardType = {}));
    var ShareType;
    (function(ShareType) {
      ShareType["VideoShareView"] = "VideoShareView";
    })(ShareType = exports.ShareType || (exports.ShareType = {}));
    var GameErrLan;
    (function(GameErrLan) {
      GameErrLan["LAN_ADNoReady"] = "\u5e7f\u544a\u672a\u51c6\u5907\u597d\uff0c\u8bf7\u7a0d\u540e\u91cd\u8bd5";
      GameErrLan["LAN_NoKey"] = "\u6ca1\u6709\u8db3\u591f\u7684\u94a5\u5319";
      GameErrLan["LAN_RecordShareSuccess"] = "\u89c6\u9891\u53d1\u5e03\u6210\u529f";
      GameErrLan["LAN_InviteError1"] = "\u8be5\u7528\u6237\u5df2\u5206\u4eab\u8fc7\uff0c\u8bf7\u91cd\u65b0\u5206\u4eab";
      GameErrLan["LAN_NoEnoughtMoney"] = "\u94b1\u4e0d\u591f";
      GameErrLan["LAN_SystemNoOpen"] = "\u529f\u80fd\u5c1a\u672a\u5f00\u653e";
      GameErrLan["LAN_NoMana"] = "\u9b54\u6cd5\u503c\u4e0d\u591f";
      GameErrLan["LAN_NoDiamond"] = "\u94bb\u77f3\u4e0d\u591f";
      GameErrLan["LAN_NoGold"] = "\u91d1\u5e01\u4e0d\u591f";
      GameErrLan["LAN_NoCard"] = "\u5347\u7ea7\u5361\u4e0d\u591f";
      GameErrLan["LAN_AlreadySeeAllAD"] = "\u6b21\u6570\u7528\u5b8c\u4e86\uff0c\u660e\u5929\u518d\u6765\u5427";
      GameErrLan["LAN_NoRune"] = "\u7b26\u6587\u6570\u91cf\u4e0d\u8db3\uff0c\u65e0\u6cd5\u8d2d\u4e70";
      GameErrLan["LAN_NoTimeReward"] = "\u65f6\u95f4\u8fd8\u6ca1\u5230\uff0c\u6682\u65f6\u65e0\u6cd5\u9886\u53d6";
      GameErrLan["LAN_BoxRewardMax"] = "\u5b9d\u7bb1\u69fd\u4f4d\u5df2\u6ee1\uff0c\u65e0\u6cd5\u7ee7\u7eed\u83b7\u5f97\u5b9d\u7bb1";
      GameErrLan["LAN_BoxOnce"] = "\u540c\u4e00\u65f6\u95f4\u53ea\u80fd\u5f00\u542f\u4e00\u4e2a\u5b9d\u7bb1";
    })(GameErrLan = exports.GameErrLan || (exports.GameErrLan = {}));
    var ConfigType;
    (function(ConfigType) {})(ConfigType = exports.ConfigType || (exports.ConfigType = {}));
    var AssetBundleEnum;
    (function(AssetBundleEnum) {
      AssetBundleEnum["mission"] = "mission";
      AssetBundleEnum["audio"] = "audio";
      AssetBundleEnum["effect"] = "effect";
      AssetBundleEnum["model"] = "model";
      AssetBundleEnum["ui"] = "ui";
      AssetBundleEnum["config"] = "config";
      AssetBundleEnum["atlas"] = "atlas";
    })(AssetBundleEnum = exports.AssetBundleEnum || (exports.AssetBundleEnum = {}));
    var AudioType;
    (function(AudioType) {
      AudioType["None"] = "";
      AudioType["audio_bgm"] = "\u80cc\u666f";
      AudioType["audio_Win"] = "\u901a\u5173\u97f3\u4e50";
      AudioType["audio_Fail"] = "\u5931\u8d25";
      AudioType["audio_Click"] = "\u6309\u94ae\u70b9\u51fb";
    })(AudioType = exports.AudioType || (exports.AudioType = {}));
    var EffectType;
    (function(EffectType) {
      EffectType["effect"] = "effect";
    })(EffectType = exports.EffectType || (exports.EffectType = {}));
    var ModelType;
    (function(ModelType) {
      ModelType["Goat"] = "Goat";
    })(ModelType = exports.ModelType || (exports.ModelType = {}));
    var UIName;
    (function(UIName) {
      UIName[UIName["BeginView"] = 0] = "BeginView";
      UIName[UIName["GameView"] = 1] = "GameView";
      UIName[UIName["FailView"] = 2] = "FailView";
      UIName[UIName["GameLoading"] = 3] = "GameLoading";
      UIName[UIName["SetView"] = 4] = "SetView";
      UIName[UIName["SignView"] = 5] = "SignView";
      UIName[UIName["BuildingView"] = 6] = "BuildingView";
      UIName[UIName["RoomView"] = 7] = "RoomView";
      UIName[UIName["RankView"] = 8] = "RankView";
      UIName[UIName["AddMoneyView"] = 9] = "AddMoneyView";
      UIName[UIName["AddPowerView"] = 10] = "AddPowerView";
      UIName[UIName["LackPowerView"] = 11] = "LackPowerView";
      UIName[UIName["ComicView"] = 12] = "ComicView";
      UIName[UIName["WinView"] = 13] = "WinView";
      UIName[UIName["RoomDetailView"] = 14] = "RoomDetailView";
      UIName[UIName["FashionView"] = 15] = "FashionView";
      UIName[UIName["WifeView"] = 16] = "WifeView";
      UIName[UIName["TipView"] = 17] = "TipView";
      UIName[UIName["Win2View"] = 18] = "Win2View";
      UIName[UIName["Fail2View"] = 19] = "Fail2View";
      UIName[UIName["FashionRewardView"] = 20] = "FashionRewardView";
      UIName[UIName["TipsView"] = 21] = "TipsView";
    })(UIName = exports.UIName || (exports.UIName = {}));
    var UIInfo;
    (function(UIInfo) {
      UIInfo["UIEvent"] = "UIevent";
      UIInfo["ShowView"] = "ShowView";
      UIInfo["CloseView"] = "CloseView";
      UIInfo["RefreshView"] = "RefreshView";
      UIInfo["GameShow"] = "GameShow";
      UIInfo["GameHide"] = "GameHide";
      UIInfo["SetProgress"] = "SetProgress";
      UIInfo["CheckWin"] = "CheckWin";
      UIInfo["CheckFail"] = "CheckFail";
      UIInfo["RefreshToken"] = "RefreshToken";
      UIInfo["BoomTank"] = "BoomTank";
    })(UIInfo = exports.UIInfo || (exports.UIInfo = {}));
    var AnalyticsEnum;
    (function(AnalyticsEnum) {
      AnalyticsEnum["VideoType"] = "VideoType";
      AnalyticsEnum["ShowMission"] = "ShowMission";
      AnalyticsEnum["MissionWin"] = "MissionWin";
      AnalyticsEnum["MissionFail"] = "MissionFail";
    })(AnalyticsEnum = exports.AnalyticsEnum || (exports.AnalyticsEnum = {}));
    cc._RF.pop();
  }, {} ],
  Event: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e0e39UBk+hHnaN/XwtvFytl", "Event");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.events = void 0;
    var Events = function() {
      function Events() {
        this.events = [];
      }
      Events.prototype.on = function(compName, compUUID, eventName, callback, that) {
        this.hasListener(compName, eventName) && this.removeListener(compName, eventName);
        this.events.push({
          compName: compName,
          compUUID: compUUID,
          eventName: eventName,
          callback: callback,
          that: that
        });
      };
      Events.prototype.once = function(compName, compUUID, eventName, callback, that) {
        var _this = this;
        this.hasListener(compName, eventName) && this.removeListener(compName, eventName);
        callback = function() {
          callback();
          _this.removeListener(compName, eventName);
        };
        this.events.push({
          compName: compName,
          compUUID: compUUID,
          eventName: eventName,
          callback: callback,
          that: that
        });
      };
      Events.prototype.emit = function(eventName) {
        var restOfData = [];
        for (var _i = 1; _i < arguments.length; _i++) restOfData[_i - 1] = arguments[_i];
        for (var _a = 0, _b = this.events; _a < _b.length; _a++) {
          var listener = _b[_a];
          listener.eventName === eventName && listener.callback.apply(listener.that, restOfData);
        }
      };
      Events.prototype.hasListener = function(compName, eventName) {
        for (var _i = 0, _a = this.events; _i < _a.length; _i++) {
          var listener = _a[_i];
          if (listener.compName === compName && listener.eventName === eventName) return true;
        }
        return false;
      };
      Events.prototype.removeListener = function(compName, eventName) {
        for (var i = 0; i < this.events.length; i++) {
          var listener = this.events[i];
          if (listener.compName === compName && listener.eventName === eventName) {
            listener.compName = null;
            listener.compUUID = null;
            listener.eventName = null;
            listener.callback = null;
            listener.that = null;
            this.events.splice(i, 1);
            break;
          }
        }
      };
      return Events;
    }();
    exports.events = new Events();
    cc._RF.pop();
  }, {} ],
  ExpansionPipe: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "acd5byzr/FOdLgF2xDQSDkj", "ExpansionPipe");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameData_1 = require("../data/GameData");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ExpansionPipe = function(_super) {
      __extends(ExpansionPipe, _super);
      function ExpansionPipe() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.sprite = null;
        _this.graphics = null;
        _this.bezierColor = new cc.Color(255, 0, 0);
        _this.lineColor = new cc.Color(0, 255, 255);
        _this.nodeRed = null;
        _this._vertices = {
          x: [],
          y: [],
          nu: [],
          nv: [],
          triangles: []
        };
        return _this;
      }
      ExpansionPipe.prototype.onLoad = function() {
        this.sprite.type = cc.Sprite.Type.MESH;
        this.sprite.node.anchorY = 1;
        this.sprite.node.anchorX = 0;
        this.sprite.node.scaleY = -1;
        this.sprite.spriteFrame = new cc.SpriteFrame(this.sprite.spriteFrame.getTexture());
        this.sprite.spriteFrame["vertices"] = this._vertices;
        this.sprite.node.opacity = 80;
        this.bezierColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Water);
      };
      ExpansionPipe.prototype.start = function() {
        var _this = this;
        this.drawBezier();
        this.stroke();
        var beginPos = this.Joint[0].node.position;
        var beginAngle = this.Joint[0].node.angle;
        var beginRed = this.nodeRed.position;
        var circleCenter = this.Joint[0].node.parent.convertToWorldSpaceAR(this.Joint[0].node.position.add(cc.v3(60, 0)));
        this.Joint[0].node.on(cc.Node.EventType.TOUCH_START, function(e) {
          _this.Joint[0].Reset();
          _this.Liquid == BasePipe_1.Liquid.Empty;
          _this.Joint[0].node.getComponent(cc.BoxCollider).enabled = false;
        });
        this.Joint[0].node.on(cc.Node.EventType.TOUCH_MOVE, function(e) {
          var worldPos = cc.v3(e.getLocation());
          var dis = cc.Vec3.distance(circleCenter, worldPos);
          var local = _this.Joint[0].node.parent.convertToNodeSpaceAR(worldPos);
          if (dis > 100) {
            var dir = worldPos.sub(circleCenter).normalize().mul(100);
            local = _this.Joint[0].node.parent.convertToNodeSpaceAR(circleCenter.add(dir));
          }
          _this.Joint[0].node.setPosition(local);
          _this.nodeRed.setPosition(local);
          _this.drawBezier();
        });
        this.Joint[0].node.on(cc.Node.EventType.TOUCH_END, function(e) {
          _this.TouchEnd(beginPos, beginAngle, beginRed);
        });
        this.Joint[0].node.on(cc.Node.EventType.TOUCH_CANCEL, function(e) {
          _this.TouchEnd(beginPos, beginAngle, beginRed);
        });
      };
      ExpansionPipe.prototype.TouchEnd = function(beginPos, beginAngle, beginRed) {
        var _this = this;
        this.Joint[0].node.getComponent(cc.BoxCollider).enabled = true;
        cc.tween(this.Joint[0].node).delay(.05).call(function() {
          if (null == _this.Joint[0].TargetJoint) {
            _this.Joint[0].node.setPosition(beginPos);
            _this.nodeRed.setPosition(beginRed);
            _this.Joint[0].node.angle = beginAngle;
            _this.nodeRed.angle = _this.Joint[0].node.angle - 90;
            _this.drawBezier();
          } else {
            var worldPos = _this.Joint[0].TargetJoint.node.convertToWorldSpaceAR(cc.Vec3.ZERO);
            var local = _this.Joint[0].node.parent.convertToNodeSpaceAR(worldPos);
            _this.Joint[0].node.setPosition(local);
            _this.nodeRed.setPosition(local);
            var selfV2 = _this.GetWorldV2(_this.Joint[0]);
            var otherV2 = _this.GetWorldV2(_this.Joint[0].TargetJoint);
            var radian = selfV2.signAngle(otherV2);
            var degree = cc.misc.radiansToDegrees(radian);
            _this.Joint[0].node.angle = _this.Joint[0].node.angle + degree;
            _this.nodeRed.angle = _this.Joint[0].node.angle - 90;
            _this.drawBezier();
          }
        }).start();
      };
      ExpansionPipe.prototype.GetWorldV2 = function(arg0) {
        var points = arg0.getComponent(cc.BoxCollider).world.points;
        var v0 = points[1].sub(points[0]);
        var v1 = points[2].sub(points[1]);
        var v2 = points[3].sub(points[2]);
        var v3 = points[0].sub(points[3]);
        var d0 = v0.mag();
        var d1 = v1.mag();
        if (d0 > d1) {
          var comVec = cc.v2(0, 1);
          var radian = v0.signAngle(comVec);
          var degree = cc.misc.radiansToDegrees(radian);
          return v0.angle(cc.v2(1, 0)) < 180 ? v0 : v2;
        }
        var comVec = cc.v2(0, 1);
        var radian = v1.signAngle(comVec);
        var degree = cc.misc.radiansToDegrees(radian);
        return v1.angle(cc.v2(1, 0)) < 180, v1;
      };
      ExpansionPipe.prototype.AddJoint = function(selfJoint) {
        this.Liquid != BasePipe_1.Liquid.Empty && this.Output();
      };
      ExpansionPipe.prototype.RemoveJoint = function(joint) {
        if (joint == this._From) {
          this.Liquid = BasePipe_1.Liquid.Empty;
          this._From = null;
        }
        this.Filling();
        this.drawBezier();
      };
      ExpansionPipe.prototype.Input = function(liquid, from) {
        this.Liquid = liquid;
        this._From = from;
        this.Filling();
        this.drawBezier();
      };
      ExpansionPipe.prototype.Output = function() {
        var _this = this;
        this.Joint.forEach(function(e) {
          e != _this._From && e.Output(_this.Liquid);
        });
      };
      ExpansionPipe.prototype.Filling = function() {
        this.Output();
      };
      ExpansionPipe.prototype.drawBezier = function() {
        this.graphics.clear();
        var startPos = this.Joint[1].node.convertToWorldSpaceAR(cc.Vec3.ZERO);
        var endPos = this.Joint[0].node.convertToWorldSpaceAR(cc.Vec3.ZERO);
        var controlPos = this.Joint[1].node.parent.convertToWorldSpaceAR(this.Joint[1].node.position.add(cc.v3(30, 0)));
        var one = this.Joint[0].node.parent.convertToWorldSpaceAR(this.Joint[0].node.position.add(cc.v3(-30, 0)));
        var two = this.Joint[0].node.parent.convertToWorldSpaceAR(this.Joint[0].node.position.add(cc.v3(30, 0)));
        if (-90 == this.nodeRed.angle || 90 == this.nodeRed.angle) {
          one = this.Joint[0].node.parent.convertToWorldSpaceAR(this.Joint[0].node.position.add(cc.v3(0, -30)));
          two = this.Joint[0].node.parent.convertToWorldSpaceAR(this.Joint[0].node.position.add(cc.v3(0, 30)));
        }
        var dis1 = cc.Vec2.distance(startPos, one);
        var dis2 = cc.Vec2.distance(startPos, two);
        var controlPos2 = dis1 < dis2 ? one : two;
        startPos = this.graphics.node.parent.convertToNodeSpaceAR(startPos);
        endPos = this.graphics.node.parent.convertToNodeSpaceAR(endPos);
        controlPos = this.graphics.node.parent.convertToNodeSpaceAR(controlPos);
        controlPos2 = this.graphics.node.parent.convertToNodeSpaceAR(controlPos2);
        this.graphics.moveTo(startPos.x, startPos.y);
        this.graphics.strokeColor = GameData_1.GameData.GetColorByLiquid(this.Liquid);
        this.graphics.bezierCurveTo(controlPos.x, controlPos.y, controlPos2.x, controlPos2.y, endPos.x, endPos.y);
        this.graphics.stroke();
        this.stroke();
      };
      ExpansionPipe.prototype.stroke = function() {
        var startPos = this.Joint[1].node.position;
        var controlPos = cc.v3(30, 0);
        var endPos = this.Joint[0].node.position;
        var len = Math.abs(this.GetCurveLenght(startPos, controlPos, endPos));
        len > 150 && (len = 150);
        if (cc.game.renderType === cc.game.RENDER_TYPE_WEBGL) {
          var vertices_1 = this._vertices;
          vertices_1.x.length = vertices_1.y.length = vertices_1.nu.length = vertices_1.nv.length = vertices_1.triangles.length = 0;
          var _impl = this.graphics["_impl"];
          var w = this.graphics.lineWidth;
          var uv_mul = 1 / len;
          var trianglesCache = [];
          var offsetX = cc.v2();
          for (var index = 0; index < _impl._paths.length; index++) {
            var path = _impl._paths[index];
            var pathPoints = path.points;
            for (var index2 = pathPoints.length - 1; index2 > 0; index2--) {
              var triangles = [];
              var p = cc.v2(pathPoints[index2].x, pathPoints[index2].y);
              var p_pre = cc.v2(pathPoints[index2 - 1].x, pathPoints[index2 - 1].y);
              var dir = p.sub(p_pre);
              var cross_dir = (0 == dir.y ? cc.v2(0, 1) : cc.v2(1, -dir.x / dir.y).normalize()).mulSelf(w / 2);
              var p_r_t = p.add(cross_dir);
              var p_r_b = p.sub(cross_dir);
              var p_l_t = p_pre.add(cross_dir);
              var p_l_b = p_pre.sub(cross_dir);
              var dirLen = dir.len();
              var i_offset = vertices_1.x.length;
              vertices_1.x.push(p_r_t.x, p_r_b.x, p_l_t.x, p_l_b.x);
              vertices_1.y.push(p_r_t.y, p_r_b.y, p_l_t.y, p_l_b.y);
              vertices_1.nu.push(offsetX.x * uv_mul, offsetX.x * uv_mul, (offsetX.x + dirLen) * uv_mul, (offsetX.x + dirLen) * uv_mul);
              vertices_1.nv.push(1, 0, 1, 0);
              triangles.push(i_offset + 0);
              triangles.push(i_offset + 1);
              triangles.push(i_offset + 2);
              triangles.push(i_offset + 1);
              triangles.push(i_offset + 2);
              triangles.push(i_offset + 3);
              var dir_angle = dir.signAngle(cc.v2(-1, 0));
              var count = 12;
              vertices_1.x.push(p.x);
              vertices_1.y.push(p.y);
              vertices_1.nu.push(offsetX.x * uv_mul);
              vertices_1.nv.push(.5);
              for (var index3 = 0; index3 < count; index3++) {
                var r = 2 * Math.PI * index3 / count;
                var pos_circle = cc.v2(w / 2 * Math.cos(r), w / 2 * Math.sin(r));
                vertices_1.x.push(pos_circle.add(p).x);
                vertices_1.y.push(pos_circle.add(p).y);
                vertices_1.nu.push((pos_circle.rotate(dir_angle).x + offsetX.x) * uv_mul);
                vertices_1.nv.push(pos_circle.rotate(dir_angle).y / w + .5);
                0 === index3 ? triangles.push(i_offset, i_offset + 1 + index3, i_offset + count) : triangles.push(i_offset, i_offset + 1 + index3, i_offset + index3);
              }
              trianglesCache.push(triangles);
              offsetX.addSelf(cc.v2(dirLen, 0));
            }
          }
          trianglesCache.forEach(function(v) {
            var _a;
            (_a = vertices_1.triangles).push.apply(_a, v);
          });
          if (this.Joint[0].node.y > 0) {
            for (var x = 0; x < vertices_1.nu.length; x++) vertices_1.nu[x] = -vertices_1.nu[x];
            for (var x = 0; x < vertices_1.nv.length; x++) vertices_1.nv[x] = -vertices_1.nv[x];
            vertices_1.triangles.reverse();
          }
          this.sprite["setVertsDirty"]();
        }
      };
      ExpansionPipe.prototype.CalculateCalculusValue = function(t, vecA, vecB, vecC) {
        var va = vecA.add(vecC).sub(vecB.mul(2));
        var vb = vecB.sub(vecA).mul(2);
        var A = 4 * (va.x * va.x + va.y * va.y);
        var B = 4 * (va.x * vb.x + va.y * vb.y);
        var C = vb.x * vb.x + vb.y * vb.y;
        var sa = Math.sqrt(A);
        var satb = 2 * A * t + B;
        var satbtc = 2 * sa * Math.sqrt(A * t * t + B * t + C);
        var sa23 = 8 * A * sa;
        var result = (satbtc * satb - (B * B - 4 * A * C) * Math.log(satbtc + satb)) / sa23;
        return result;
      };
      ExpansionPipe.prototype.GetCurveLenght = function(vecA, vecB, vecC) {
        return this.CalculateCalculusValue(0, vecA, vecB, vecC) - this.CalculateCalculusValue(1, vecA, vecB, vecC);
      };
      __decorate([ property(cc.Sprite) ], ExpansionPipe.prototype, "sprite", void 0);
      __decorate([ property(cc.Graphics) ], ExpansionPipe.prototype, "graphics", void 0);
      __decorate([ property(cc.Color) ], ExpansionPipe.prototype, "bezierColor", void 0);
      __decorate([ property(cc.Color) ], ExpansionPipe.prototype, "lineColor", void 0);
      __decorate([ property(cc.Node) ], ExpansionPipe.prototype, "nodeRed", void 0);
      ExpansionPipe = __decorate([ ccclass ], ExpansionPipe);
      return ExpansionPipe;
    }(BasePipe_1.default);
    exports.default = ExpansionPipe;
    cc._RF.pop();
  }, {
    "../data/GameData": "GameData",
    "./BasePipe": "BasePipe"
  } ],
  ExportAction: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "12797gQakBLxrhetZDO2eqn", "ExportAction");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var NewClass = function(_super) {
      __extends(NewClass, _super);
      function NewClass() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.moveNode = null;
        _this.control_node1 = null;
        _this.control_node2 = null;
        _this.isMove = false;
        return _this;
      }
      NewClass.prototype.start = function() {};
      NewClass.prototype.moveRight = function() {
        var _this = this;
        if (this.isMove) return;
        cc.tween(this.moveNode).call(function() {
          _this.isMove = true;
        }).to(.5, {
          x: 525
        }, {
          easing: "quadIn"
        }).call(function() {
          _this.control_node1.active = true;
          _this.control_node2.active = false;
          _this.isMove = false;
        }).start();
      };
      NewClass.prototype.moveLeft = function() {
        var _this = this;
        if (this.isMove) return;
        cc.tween(this.moveNode).call(function() {
          _this.isMove = true;
        }).to(.5, {
          x: 0
        }).call(function() {
          _this.control_node1.active = false;
          _this.control_node2.active = true;
          _this.isMove = false;
        }).start();
      };
      __decorate([ property(cc.Node) ], NewClass.prototype, "moveNode", void 0);
      __decorate([ property(cc.Node) ], NewClass.prototype, "control_node1", void 0);
      __decorate([ property(cc.Node) ], NewClass.prototype, "control_node2", void 0);
      NewClass = __decorate([ ccclass ], NewClass);
      return NewClass;
    }(cc.Component);
    exports.default = NewClass;
    cc._RF.pop();
  }, {} ],
  ExportContent: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ac81702MqVPraf3bA8nFEQZ", "ExportContent");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ExportLayout_1 = require("./ExportLayout");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ExportContent = function(_super) {
      __extends(ExportContent, _super);
      function ExportContent() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.exportLayouts = [];
        return _this;
      }
      ExportContent.prototype.Show = function(exportHand, whatBox) {
        void 0 === whatBox && (whatBox = 0);
        this.node.active = true;
        for (var x = 0; x < this.exportLayouts.length; x++) {
          var element = this.exportLayouts[x];
          exportHand && "ExportHand" == exportHand.name && whatBox == x ? element.ShowIcon(exportHand) : element.ShowIcon();
        }
      };
      ExportContent.prototype.UnShow = function() {
        this.node.active = false;
      };
      ExportContent.prototype.SetPage = function(platform) {
        this.Platform = platform;
        for (var x = 0; x < this.exportLayouts.length; x++) {
          var element = this.exportLayouts[x];
          element.GetIcon(this.Platform);
        }
        this.node.active = false;
      };
      __decorate([ property([ ExportLayout_1.default ]) ], ExportContent.prototype, "exportLayouts", void 0);
      ExportContent = __decorate([ ccclass ], ExportContent);
      return ExportContent;
    }(cc.Component);
    exports.default = ExportContent;
    cc._RF.pop();
  }, {
    "./ExportLayout": "ExportLayout"
  } ],
  ExportHand: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "52a1d/a4eROoYMaH08lnFzX", "ExportHand");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var NewClass = function(_super) {
      __extends(NewClass, _super);
      function NewClass() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeHand1 = null;
        _this.nodeHand2 = null;
        return _this;
      }
      NewClass.prototype.start = function() {
        this.actionManager();
      };
      NewClass.prototype.actionManager = function(Bool) {
        var _this = this;
        void 0 === Bool && (Bool = true);
        if (!Bool) {
          this.nodeHand1.stopAllActions();
          this.nodeHand2.stopAllActions();
        }
        cc.tween(this.nodeHand2).repeatForever(cc.tween().to(1, {
          scale: 1.8,
          opacity: 50
        }).call(function() {
          _this.nodeHand2.scale = 1;
          _this.nodeHand2.opacity = 255;
        })).start();
        cc.tween(this.nodeHand1).repeatForever(cc.tween().to(.5, {
          scale: 1.5
        }).to(.5, {
          scale: 1
        })).start();
      };
      __decorate([ property(cc.Node) ], NewClass.prototype, "nodeHand1", void 0);
      __decorate([ property(cc.Node) ], NewClass.prototype, "nodeHand2", void 0);
      NewClass = __decorate([ ccclass ], NewClass);
      return NewClass;
    }(cc.Component);
    exports.default = NewClass;
    cc._RF.pop();
  }, {} ],
  ExportIcon: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7933blBqrFPM7mi86ZXumZ8", "ExportIcon");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ExportIcon = function(_super) {
      __extends(ExportIcon, _super);
      function ExportIcon() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.RedPoint = null;
        _this.Pic = null;
        _this.Name = null;
        _this.DownPicContent = null;
        _this.streamer = null;
        _this.MoveTime = .3;
        _this.GapTime = 1;
        _this.Shake = false;
        return _this;
      }
      ExportIcon.prototype.SetIcon = function(url, name, boolShowRedPoint, index) {
        var _this = this;
        this.node.active = false;
        cc.loader.load(url, function(err, texture) {
          if (err) {
            console.log("icon err", err);
            return;
          }
          var sprite = new cc.SpriteFrame(texture);
          _this.Pic.spriteFrame = sprite;
          _this.node.active = true;
        });
        this.RedPoint && (this.RedPoint.active = boolShowRedPoint);
        this.Name && (this.Name.string = name);
        if (this.DownPicContent) {
          for (var i = 0; i < this.DownPicContent.childrenCount; i++) {
            var ele = this.DownPicContent.children[i];
            ele.active = false;
          }
          this.DownPicContent.children[index % this.DownPicContent.childrenCount].active = true;
        }
        this.streamer && this.RunStreamer();
      };
      ExportIcon.prototype.RunStreamer = function() {
        var _this = this;
        this.streamer.stopAllActions();
        var Move = cc.callFunc(function() {
          _this.streamer.x = -_this.streamer.parent.getContentSize().width / 2;
          var action = cc.moveTo(_this.MoveTime, -_this.streamer.x);
          var delay = cc.delayTime(_this.GapTime);
          _this.streamer.runAction(cc.sequence(action, delay, Move));
        });
        this.streamer.runAction(Move);
      };
      ExportIcon.prototype.BeginShake = function() {
        if (this.Shake) {
          this.node.stopAllActions();
          var shake = cc.sequence(cc.rotateTo(.2, 15), cc.rotateTo(.2, -15), cc.rotateTo(.2, 5), cc.rotateTo(.2, -5), cc.rotateTo(.2, 0), cc.delayTime(2));
          this.node.runAction(shake.repeatForever());
        }
      };
      __decorate([ property({
        type: cc.Node,
        tooltip: "\u7ea2\u70b9"
      }) ], ExportIcon.prototype, "RedPoint", void 0);
      __decorate([ property({
        type: cc.Sprite,
        displayName: "\u56fe\u7247"
      }) ], ExportIcon.prototype, "Pic", void 0);
      __decorate([ property({
        type: cc.Label,
        displayName: "\u540d\u5b57"
      }) ], ExportIcon.prototype, "Name", void 0);
      __decorate([ property({
        type: cc.Node,
        displayName: "\u989c\u8272\u6846\u7236\u8282\u70b9"
      }) ], ExportIcon.prototype, "DownPicContent", void 0);
      __decorate([ property({
        type: cc.Node,
        displayName: "\u95ea\u5149"
      }) ], ExportIcon.prototype, "streamer", void 0);
      __decorate([ property({
        type: cc.Float,
        visible: function() {
          return null != this.streamer;
        },
        displayName: "\u95ea\u5149\u65f6\u95f4"
      }) ], ExportIcon.prototype, "MoveTime", void 0);
      __decorate([ property({
        type: cc.Float,
        visible: function() {
          return null != this.streamer;
        },
        displayName: "\u95ea\u5149\u95f4\u9694"
      }) ], ExportIcon.prototype, "GapTime", void 0);
      __decorate([ property({
        displayName: "\u662f\u5426\u6296\u52a8"
      }) ], ExportIcon.prototype, "Shake", void 0);
      ExportIcon = __decorate([ ccclass ], ExportIcon);
      return ExportIcon;
    }(cc.Component);
    exports.default = ExportIcon;
    cc._RF.pop();
  }, {} ],
  ExportLayout: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6ff60DTYTFOapf9WmzkD7N6", "ExportLayout");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Utils_1 = require("../../global/libs/Utils");
    var ExportIcon_1 = require("./ExportIcon");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ExportLayOutType;
    (function(ExportLayOutType) {
      ExportLayOutType[ExportLayOutType["None"] = 0] = "None";
      ExportLayOutType[ExportLayOutType["DownPage"] = 1] = "DownPage";
      ExportLayOutType[ExportLayOutType["CoupletLeft"] = 2] = "CoupletLeft";
      ExportLayOutType[ExportLayOutType["CoupletRight"] = 3] = "CoupletRight";
      ExportLayOutType[ExportLayOutType["MoreGameTop"] = 4] = "MoreGameTop";
      ExportLayOutType[ExportLayOutType["MoreGameBottom"] = 5] = "MoreGameBottom";
      ExportLayOutType[ExportLayOutType["ReturnPageTop"] = 6] = "ReturnPageTop";
      ExportLayOutType[ExportLayOutType["ReturnPageBottom"] = 7] = "ReturnPageBottom";
      ExportLayOutType[ExportLayOutType["LeftDrawerPage"] = 8] = "LeftDrawerPage";
      ExportLayOutType[ExportLayOutType["SettlementPage"] = 9] = "SettlementPage";
      ExportLayOutType[ExportLayOutType["FailPage"] = 10] = "FailPage";
      ExportLayOutType[ExportLayOutType["OldUsersPage"] = 11] = "OldUsersPage";
    })(ExportLayOutType || (ExportLayOutType = {}));
    var ExportLayout = function(_super) {
      __extends(ExportLayout, _super);
      function ExportLayout() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.AdId = ExportLayOutType.None;
        _this.ExportIcon = null;
        _this.showNumber = 10;
        _this.MoveVertical = false;
        _this.MoveHorizontal = false;
        _this.onClickRefresh = false;
        _this.handRelationNode = null;
        _this.exportHand = null;
        return _this;
      }
      ExportLayout.prototype.GetSlotId = function(AdId) {
        switch (AdId) {
         case ExportLayOutType.DownPage:
         case ExportLayOutType.CoupletRight:
         case ExportLayOutType.CoupletLeft:
         case ExportLayOutType.MoreGameTop:
         case ExportLayOutType.JiaoChaTop:
         case ExportLayOutType.JiaoChaCenter:
         case ExportLayOutType.JiaoChaDown:
         case ExportLayOutType.MoreGameBottom:
          return "a84d9cb1104d301e";

         case ExportLayOutType.ReturnPageTop:
          return "d4e0fef07fbbdb02";

         case ExportLayOutType.ReturnPageBottom:
          return "658ee68321caa631";

         case ExportLayOutType.LeftDrawerPage:
         case ExportLayOutType.SettlementPage:
         case ExportLayOutType.FailPage:
         case ExportLayOutType.OldUsersPage:
          return "100dfa7ba23886b5";
        }
      };
      ExportLayout.prototype.GetIcon = function(platform) {
        return __awaiter(this, void 0, void 0, function() {
          var x, NewIcon, result, error_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              this.Platform = platform;
              if (this.AdId == ExportLayOutType.None) return [ 2 ];
              if (null == this.ExportIcon) return [ 2 ];
              if (null == this.Platform) return [ 2 ];
              for (x = 0; x < this.showNumber; x++) if (this.ExportIcon.node.parent.childrenCount <= x) {
                NewIcon = cc.instantiate(this.ExportIcon.node);
                NewIcon.parent = this.ExportIcon.node.parent;
              }
              this.IconAd = this.Platform.createIconAd({
                slotId: this.GetSlotId(this.AdId),
                limit: this.showNumber
              });
              _a.label = 1;

             case 1:
              _a.trys.push([ 1, 3, , 4 ]);
              return [ 4, this.IconAd.load() ];

             case 2:
              result = _a.sent();
              this.BoxList = result;
              this.Refresh();
              return [ 3, 4 ];

             case 3:
              error_1 = _a.sent();
              return [ 3, 4 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      ExportLayout.prototype.Refresh = function() {
        if (this.BoxList && this.BoxList.length > 0) for (var x = 0; x < this.ExportIcon.node.parent.childrenCount; x++) {
          var nowNode = this.ExportIcon.node.parent.children[x];
          var nowExportIcon = nowNode.getComponent(ExportIcon_1.default);
          if (this.BoxList[x]) {
            nowNode.active = true;
            var data = this.BoxList[x].data;
            nowExportIcon.SetIcon(data.icon, data.title, this.BoxList[x].dot(), x);
            nowExportIcon.node.on(cc.Node.EventType.TOUCH_END, this.onIconTouch, this);
          } else nowNode.active = false;
        }
      };
      ExportLayout.prototype.onIconTouch = function(event) {
        var Target = event.currentTarget;
        this.JumpToGame(Target.getSiblingIndex());
      };
      ExportLayout.prototype.JumpToGame = function(index) {
        var _this = this;
        this.BoxList && this.BoxList[index] && this.BoxList[index].click({
          complete: function() {
            if (_this.onClickRefresh) {
              _this.BoxList = Utils_1.utils.randomArr(_this.BoxList);
              _this.Refresh();
            }
          }
        });
      };
      ExportLayout.prototype.randomHand = function(ranMax) {
        return Math.floor(Math.random() * ranMax);
      };
      ExportLayout.prototype.update = function(dt) {
        if (!this.exportHand || !this.handRelationNode || !this.ExportIcon) return;
        var pox = this.handRelationNode.getPosition().add(this.ExportIcon.node.parent.getPosition());
        this.exportHand.setPosition(pox);
      };
      ExportLayout.prototype.ShowIcon = function(exportHand) {
        var _this = this;
        var MoveContent = this.ExportIcon.node.parent;
        MoveContent.stopAllActions();
        var Move = cc.callFunc(function() {
          var size = MoveContent.getContentSize();
          var parentSize = MoveContent.parent.getContentSize();
          if (_this.MoveHorizontal) if (size.width > parentSize.width) {
            var Gap = (MoveContent.x > 0 ? -1 : 1) * (size.width - parentSize.width) / 2;
            var action = cc.moveTo(Math.abs(MoveContent.x - Gap) / (_this.ExportIcon.node.getContentSize().width / 4), cc.v2(Gap, MoveContent.y));
            MoveContent.runAction(cc.sequence(action, Move));
          } else {
            var delayAction = cc.delayTime(.3);
            MoveContent.runAction(cc.sequence(delayAction, Move));
          } else if (_this.MoveVertical) if (size.height > parentSize.height) {
            var Gap = (MoveContent.y > 0 ? -1 : 1) * (size.height - parentSize.height) / 2;
            var action = cc.moveTo(Math.abs(MoveContent.y - Gap) / (_this.ExportIcon.node.getContentSize().height / 4), cc.v2(MoveContent.x, Gap));
            MoveContent.runAction(cc.sequence(action, Move));
          } else {
            var delayAction = cc.delayTime(.3);
            MoveContent.runAction(cc.sequence(delayAction, Move));
          }
        });
        MoveContent.runAction(Move);
        var exportIconArr = this.node.getComponentsInChildren(ExportIcon_1.default);
        exportIconArr.forEach(function(item) {
          return item.BeginShake();
        });
        var ranHand = this.randomHand(this.ExportIcon.node.parent.childrenCount);
        if (exportHand) {
          this.exportHand = exportHand;
          this.handRelationNode = this.ExportIcon.node.parent.children[ranHand];
          exportHand.parent = this.ExportIcon.node.parent.parent;
          exportHand.active = true;
        }
        if (this.BoxList) for (var x = 0; x < this.ExportIcon.node.parent.childrenCount; x++) this.ExportIcon.node.parent.children[x].active && null != this.BoxList[x] && this.BoxList[x].show();
      };
      __decorate([ property({
        type: cc.Enum(ExportLayOutType),
        displayName: "\u540e\u53f0\u5e7f\u544a"
      }) ], ExportLayout.prototype, "AdId", void 0);
      __decorate([ property({
        type: ExportIcon_1.default,
        displayName: "\u56fe\u6807"
      }) ], ExportLayout.prototype, "ExportIcon", void 0);
      __decorate([ property({
        type: cc.Integer,
        displayName: "\u663e\u793a\u6570\u76ee"
      }) ], ExportLayout.prototype, "showNumber", void 0);
      __decorate([ property({
        displayName: "\u5782\u76f4\u6eda\u52a8"
      }) ], ExportLayout.prototype, "MoveVertical", void 0);
      __decorate([ property({
        displayName: "\u6c34\u5e73\u6eda\u52a8"
      }) ], ExportLayout.prototype, "MoveHorizontal", void 0);
      __decorate([ property({
        displayName: "\u70b9\u51fbicon\u5237\u65b0"
      }) ], ExportLayout.prototype, "onClickRefresh", void 0);
      ExportLayout = __decorate([ ccclass ], ExportLayout);
      return ExportLayout;
    }(cc.Component);
    exports.default = ExportLayout;
    cc._RF.pop();
  }, {
    "../../global/libs/Utils": "Utils",
    "./ExportIcon": "ExportIcon"
  } ],
  ExportPageController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3cfd7vodfhMX5TVtM3DXBFG", "ExportPageController");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.exportPageController = void 0;
    var UmSdk_1 = require("./UmSdk");
    var ExportPageController = function() {
      function ExportPageController() {
        this.nodeExport = null;
        this._Platform = null;
      }
      ExportPageController.prototype.init = function(nodeExport) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this._initNode(nodeExport) ];

             case 1:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      ExportPageController.prototype.refreshAllPage = function(Platform) {
        this.nodeExport ? this.nodeExport.getComponent("ExportPage").RefreshAllPage(Platform) : this._Platform = Platform;
      };
      ExportPageController.prototype._initNode = function(nodeExport) {
        return __awaiter(this, void 0, Promise, function() {
          var node, parent, widget;
          return __generator(this, function(_a) {
            node = new cc.Node("Export");
            parent = cc.find("Canvas").parent;
            parent.addChild(node, 9999);
            cc.game.addPersistRootNode(node);
            widget = node.addComponent(cc.Widget);
            widget.isAlignVerticalCenter = true;
            widget.isAlignHorizontalCenter = true;
            node.setContentSize(this.getScreenSize().x, this.getScreenSize().y);
            this.nodeExport = cc.instantiate(nodeExport);
            node.addChild(this.nodeExport);
            this._Platform && this.nodeExport.getComponent("ExportPage").RefreshAllPage(this._Platform);
            return [ 2 ];
          });
        });
      };
      ExportPageController.prototype.getScreenSize = function() {
        return new cc.Vec2(cc.winSize.width, cc.winSize.height);
      };
      ExportPageController.prototype.createNode = function(name) {
        console.log("\u53ea\u4f1a\u521b\u5efa\u4e00\u6b21");
        return new Promise(function(resolve, reject) {
          cc.loader.loadRes("prefab/popup/" + name, cc.Prefab, function(err, res) {
            if (err) {
              reject(null);
              return;
            }
            var nodeExport = cc.instantiate(res);
            resolve(nodeExport);
          });
        });
      };
      ExportPageController.prototype.showExport = function(pageType, parent, callback) {
        var exportStatus = UmSdk_1.umSdk.getOnLineGameValue(15);
        if (this.nodeExport && exportStatus) this.nodeExport.getComponent("ExportPage").ShowPage(pageType, parent, callback); else {
          console.log("\u8fd8\u6ca1\u521d\u59cb\u5316\u51fa\u6765\u8fd9\u4e2a\u5bfc\u51fa\u5f39\u7a97");
          callback && callback();
        }
      };
      ExportPageController.prototype.closeExport = function(pageType) {
        this.nodeExport && this.nodeExport.getComponent("ExportPage").ClosePage(pageType);
      };
      return ExportPageController;
    }();
    exports.exportPageController = new ExportPageController();
    cc._RF.pop();
  }, {
    "./UmSdk": "UmSdk"
  } ],
  ExportPage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4b915KuP6ZHg5f/RcdOskcL", "ExportPage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ExportContent_1 = require("./ExportContent");
    var MoreGamePage_1 = require("./MoreGamePage");
    var ReturnPage_1 = require("./ReturnPage");
    var SDKEnum_1 = require("./SDKEnum");
    var OldUsersPage_1 = require("./OldUsersPage");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var PopUp_1 = require("../../global/libs/PopUp");
    var Util_1 = require("../../scripts/Core/Util");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ExportPage = function(_super) {
      __extends(ExportPage, _super);
      function ExportPage() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.MoreGamePage = null;
        _this.MoreGamePage_Old = null;
        _this.ReturnPage = null;
        _this.AppletPage = null;
        _this.InsidePage = null;
        _this.CoupletPage = null;
        _this.DownPage = null;
        _this.LeftDrawerPage = null;
        _this.RightDrawerPage = null;
        _this.SettlementPage = null;
        _this.FailPage = null;
        _this.OldUsersPage = null;
        _this.OldMoreGamePage = null;
        _this.ExportHand = null;
        _this.callback = null;
        return _this;
      }
      ExportPage.prototype.RefreshAllPage = function(platform) {
        this.Platform = platform;
        this.RefreshPage(SDKEnum_1.PageType.MoreGamePage);
        this.RefreshPage(SDKEnum_1.PageType.RightDrawerPage);
      };
      ExportPage.prototype.RefreshPage = function(pageType) {
        switch (pageType) {
         case SDKEnum_1.PageType.MoreGamePage:
          this.MoreGamePage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.ReturnPage:
          this.ReturnPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.AppletPage:
          this.AppletPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.InsidePage:
          this.InsidePage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.CoupletPage:
          this.CoupletPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.DownPage:
          this.DownPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.LeftDrawerPage:
          this.LeftDrawerPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.RightDrawerPage:
          this.RightDrawerPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.SettlementPage:
          this.SettlementPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.FailPage:
          this.FailPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.OldUsersPage:
          this.OldUsersPage.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.MoreGamePage_Old:
          this.MoreGamePage_Old.SetPage(this.Platform);
          break;

         case SDKEnum_1.PageType.OldMoreGame:
          this.OldMoreGamePage.SetPage(this.Platform);
        }
      };
      ExportPage.prototype.ShowPage = function(pageType, parent, callback) {
        void 0 === parent && (parent = null);
        callback && (this.callback = callback);
        switch (pageType) {
         case SDKEnum_1.PageType.MoreGamePage:
          this.MoreGamePage.Show(this.ExportHand);
          this.MoreGamePage.node.getComponent(MoreGamePage_1.default).OnPageShow();
          break;

         case SDKEnum_1.PageType.ReturnPage:
          this.ReturnPage.Show(this.ExportHand);
          this.ReturnPage.getComponent(ReturnPage_1.default).OnPageShow();
          break;

         case SDKEnum_1.PageType.AppletPage:
          this.AppletPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.InsidePage:
          this.InsidePage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.CoupletPage:
          UmSdk_1.umSdk.getOnLineGameValue(20) && this.CoupletPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.DownPage:
          this.DownPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.LeftDrawerPage:
          this.LeftDrawerPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.RightDrawerPage:
          this.RightDrawerPage.Show();
          break;

         case SDKEnum_1.PageType.SettlementPage:
          this.SettlementPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.FailPage:
          this.FailPage.Show(this.ExportHand);
          break;

         case SDKEnum_1.PageType.OldUsersPage:
          console.log("\u8001\u7528\u6237\u5bfc\u51fa");
          if (UmSdk_1.umSdk.getOnLineGameValue(20)) {
            this.OldUsersPage.Show();
            this.OldUsersPage.node.getComponent(OldUsersPage_1.default).OnPageShow();
          }
          break;

         case SDKEnum_1.PageType.OldMoreGame:
          this.OldMoreGamePage.Show(this.ExportHand);
          this.OldMoreGamePage.node.getComponent("OldMoreGamePage").OnPageShow();
          break;

         case SDKEnum_1.PageType.MoreGamePage_Old:
          this.MoreGamePage_Old.Show();
          this.MoreGamePage_Old.node.getComponent(MoreGamePage_1.default).OnPageShow();
        }
        return true;
      };
      ExportPage.prototype.ClosePage = function(pageType) {
        switch (pageType) {
         case SDKEnum_1.PageType.MoreGamePage:
          this.MoreGamePage.UnShow();
          this.MoreGamePage.node.getComponent(MoreGamePage_1.default).OnPageHide();
          var chance = Util_1.Util.Random.getRandom(0, 100);
          console.log("\u5b9d\u7bb1\u6982\u7387", chance, UmSdk_1.umSdk.BoxChance);
          if (chance < UmSdk_1.umSdk.BoxChance) {
            console.log("????");
            PopUp_1.popup.show(PopUp_1.PopUp.MysticBoxView, null, function() {}, function() {});
          }
          break;

         case SDKEnum_1.PageType.MoreGamePage_Old:
          this.MoreGamePage_Old.UnShow();
          this.MoreGamePage_Old.node.getComponent(MoreGamePage_1.default).OnPageHide();
          this.ShowPage(SDKEnum_1.PageType.ReturnPage);
          this.RefreshPage(SDKEnum_1.PageType.MoreGamePage);
          this.RefreshPage(SDKEnum_1.PageType.InsidePage);
          break;

         case SDKEnum_1.PageType.ReturnPage:
          this.ReturnPage.UnShow();
          this.ReturnPage.getComponent(ReturnPage_1.default).OnPageHide();
          if (this.callback) {
            this.callback();
            this.callback = null;
          }
          break;

         case SDKEnum_1.PageType.AppletPage:
          this.AppletPage.UnShow();
          break;

         case SDKEnum_1.PageType.InsidePage:
          this.InsidePage.UnShow();
          break;

         case SDKEnum_1.PageType.CoupletPage:
          this.CoupletPage.UnShow();
          break;

         case SDKEnum_1.PageType.DownPage:
          this.DownPage.UnShow();
          break;

         case SDKEnum_1.PageType.LeftDrawerPage:
          this.LeftDrawerPage.UnShow();
          break;

         case SDKEnum_1.PageType.RightDrawerPage:
          this.RightDrawerPage.UnShow();
          break;

         case SDKEnum_1.PageType.SettlementPage:
          this.SettlementPage.UnShow();
          break;

         case SDKEnum_1.PageType.FailPage:
          this.FailPage.UnShow();
          break;

         case SDKEnum_1.PageType.OldUsersPage:
          this.OldUsersPage.UnShow();
          this.OldUsersPage.getComponent(OldUsersPage_1.default).OnPageHide();
          break;

         case SDKEnum_1.PageType.OldMoreGame:
          this.OldMoreGamePage.UnShow();
          this.OldMoreGamePage.node.getComponent("OldMoreGamePage").OnPageHide();
        }
      };
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u66f4\u591a\u6e38\u620f\u9875"
      }) ], ExportPage.prototype, "MoreGamePage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u66f4\u591a\u6e38\u620f\u9875\uff08\u65e7\uff09"
      }) ], ExportPage.prototype, "MoreGamePage_Old", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u9000\u51fa\u9875"
      }) ], ExportPage.prototype, "ReturnPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u5c0f\u7a0b\u5e8f\u5bfc\u51fa\u9875"
      }) ], ExportPage.prototype, "AppletPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u9875\u9762\u5185\u5bfc\u51fa"
      }) ], ExportPage.prototype, "InsidePage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u5bf9\u8054"
      }) ], ExportPage.prototype, "CoupletPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u4e0b\u65b9\u5bfc\u51fa"
      }) ], ExportPage.prototype, "DownPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u5de6\u8fb9\u62bd\u5c49"
      }) ], ExportPage.prototype, "LeftDrawerPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u53f3\u8fb9\u62bd\u5c49"
      }) ], ExportPage.prototype, "RightDrawerPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u7ed3\u7b97\u9875"
      }) ], ExportPage.prototype, "SettlementPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u5931\u8d25\u9875"
      }) ], ExportPage.prototype, "FailPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u8001\u7528\u6237\u9875"
      }) ], ExportPage.prototype, "OldUsersPage", void 0);
      __decorate([ property({
        type: ExportContent_1.default,
        displayName: "\u8001\u7528\u6237\u98752"
      }) ], ExportPage.prototype, "OldMoreGamePage", void 0);
      __decorate([ property({
        type: cc.Node,
        displayName: "shou"
      }) ], ExportPage.prototype, "ExportHand", void 0);
      ExportPage = __decorate([ ccclass ], ExportPage);
      return ExportPage;
    }(cc.Component);
    exports.default = ExportPage;
    cc._RF.pop();
  }, {
    "../../global/libs/PopUp": "PopUp",
    "../../global/libs/UmSdk": "UmSdk",
    "../../scripts/Core/Util": "Util",
    "./ExportContent": "ExportContent",
    "./MoreGamePage": "MoreGamePage",
    "./OldUsersPage": "OldUsersPage",
    "./ReturnPage": "ReturnPage",
    "./SDKEnum": "SDKEnum"
  } ],
  Fail2View: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "81ec1uyPaVLAoqgY2dXNO5D", "Fail2View");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.Fail2View = void 0;
    var SDKEnum_1 = require("../../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../../global/libs/ExportPageController");
    var Platform_1 = require("../../../global/libs/Platform");
    var UmSdk_1 = require("../../../global/libs/UmSdk");
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var Fail2View = function(_super) {
      __extends(Fail2View, _super);
      function Fail2View() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnReturn = void 0;
        _this.btnContinue = void 0;
        return _this;
      }
      Fail2View.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Fail2View);
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.GameView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      Fail2View.prototype.click_btn_continue = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Fail2View);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.FailView);
      };
      Fail2View.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          SDKManager_1.SDKManager.ShowCenterCustomAd();
          1 == UmSdk_1.umSdk.CustomCloseBanner && Platform_1.platform.hideBanner();
          break;

         case EnumDef_1.UIInfo.CloseView:
          ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.MoreGamePage, null);
          SDKManager_1.SDKManager.CloseCenterCustomAd();
          1 == UmSdk_1.umSdk.CustomCloseBanner && Platform_1.platform.showBanner();
        }
      };
      __decorate([ property(cc.Button) ], Fail2View.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Button) ], Fail2View.prototype, "btnContinue", void 0);
      Fail2View = __decorate([ ccclass ], Fail2View);
      return Fail2View;
    }(cc.Component);
    exports.Fail2View = Fail2View;
    cc._RF.pop();
  }, {
    "../../../export/scripts/SDKEnum": "SDKEnum",
    "../../../global/libs/ExportPageController": "ExportPageController",
    "../../../global/libs/Platform": "Platform",
    "../../../global/libs/UmSdk": "UmSdk",
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef"
  } ],
  FailView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d0bf5+PaBdH0rmfUpZZEqvN", "FailView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FailView = void 0;
    var SDKEnum_1 = require("../../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../../global/libs/ExportPageController");
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var FailView = function(_super) {
      __extends(FailView, _super);
      function FailView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeReplayAd = void 0;
        _this.btnReplay = void 0;
        _this.nodeChoose = void 0;
        _this.btnChoose = void 0;
        return _this;
      }
      FailView.prototype.click_btn_jump = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u5931\u8d25\u8df3\u8fc7", function() {
          GameData_1.GameData.UserData.Mission++;
          GameData_1.GameData.UserData.SaveData();
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FailView);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.ShowView);
        });
      };
      FailView.prototype.click_btn_replay = function(e) {
        if (this.nodeReplayAd.active) SDKManager_1.SDKManager.PlayVideo("\u5931\u8d25\u91cd\u73a9", function() {
          GameData_1.GameData.UserData.Power++;
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FailView);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        }); else {
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FailView);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        }
      };
      FailView.prototype.click_btn_choose = function(e) {
        this.nodeChoose.active = !this.nodeChoose.active;
        this.nodeReplayAd.active = !this.nodeChoose.active;
      };
      FailView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          Util_1.Util.Audio.PlayEffect(EnumDef_1.AudioType.audio_Fail);
          ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.RightDrawerPage, null);
          this.nodeChoose.active = false;
          this.nodeReplayAd.active = true;
          break;

         case EnumDef_1.UIInfo.CloseView:
          ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.RightDrawerPage);
        }
      };
      __decorate([ property(cc.Node) ], FailView.prototype, "nodeReplayAd", void 0);
      __decorate([ property(cc.Button) ], FailView.prototype, "btnReplay", void 0);
      __decorate([ property(cc.Node) ], FailView.prototype, "nodeChoose", void 0);
      __decorate([ property(cc.Button) ], FailView.prototype, "btnChoose", void 0);
      FailView = __decorate([ ccclass ], FailView);
      return FailView;
    }(cc.Component);
    exports.FailView = FailView;
    cc._RF.pop();
  }, {
    "../../../export/scripts/SDKEnum": "SDKEnum",
    "../../../global/libs/ExportPageController": "ExportPageController",
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  FashionItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "7a1cfi44tZMXawkO0ABMx/L", "FashionItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FashionItem = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var FashionItem = function(_super) {
      __extends(FashionItem, _super);
      function FashionItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeOnUse_arr = [];
        _this.nodeAd_arr = [];
        _this.labelMoney_arr = [];
        _this.nodeMoney_arr = [];
        _this.nodeUse_arr = [];
        _this.nodeRole_arr = [];
        _this.display_arr = [];
        return _this;
      }
      FashionItem.prototype.start = function() {
        var _this = this;
        this.nodeAd_arr.forEach(function(e, x) {
          e.on(cc.Node.EventType.TOUCH_END, function() {
            SDKManager_1.SDKManager.PlayVideo("\u7537\u65f6\u88c5", function() {
              GameData_1.GameData.AddFashion(_this._num + x);
              GameData_1.GameData.UseFashion(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.FashionView, EnumDef_1.UIInfo.RefreshView);
            });
          });
        });
        this.nodeMoney_arr.forEach(function(e, x) {
          e.on(cc.Node.EventType.TOUCH_END, function() {
            var money = GameData_1.GameData.FashionMoney[_this._num + x];
            if (GameData_1.GameData.UserData.Money >= money) {
              GameData_1.GameData.UserData.Money -= money;
              GameData_1.GameData.AddFashion(_this._num + x);
              GameData_1.GameData.UseFashion(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.FashionView, EnumDef_1.UIInfo.RefreshView);
            }
          });
          _this.nodeUse_arr.forEach(function(e, x) {
            e.on(cc.Node.EventType.TOUCH_END, function() {
              GameData_1.GameData.UseFashion(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.FashionView, EnumDef_1.UIInfo.RefreshView);
            });
          });
        });
      };
      FashionItem.prototype.SetData = function(num) {
        this._num = num;
        for (var x = 0; x < 3; x++) this.SetOne(num, x);
      };
      FashionItem.prototype.SetOne = function(num, x) {
        var fashion = GameData_1.GameData.FashionData[num + x];
        if (null != fashion) {
          this.nodeRole_arr[x].active = true;
          this.display_arr[x].playAnimation(fashion + "\u5f85\u673a", 0);
          this.nodeOnUse_arr[x].active = GameData_1.GameData.UserData.NowFashion == fashion;
          this.nodeAd_arr[x].active = !GameData_1.GameData.HadFashion(fashion) && GameData_1.GameData.FashionMoney[num + x] < 10;
          this.nodeMoney_arr[x].active = !GameData_1.GameData.HadFashion(fashion) && GameData_1.GameData.FashionMoney[num + x] > 10;
          this.labelMoney_arr[x].string = "" + GameData_1.GameData.FashionMoney[num + x];
          this.nodeUse_arr[x].active = GameData_1.GameData.HadFashion(fashion) && GameData_1.GameData.UserData.NowFashion != fashion;
        } else this.nodeRole_arr[x].active = false;
      };
      __decorate([ property(cc.Node) ], FashionItem.prototype, "nodeOnUse_arr", void 0);
      __decorate([ property(cc.Node) ], FashionItem.prototype, "nodeAd_arr", void 0);
      __decorate([ property(cc.Label) ], FashionItem.prototype, "labelMoney_arr", void 0);
      __decorate([ property(cc.Node) ], FashionItem.prototype, "nodeMoney_arr", void 0);
      __decorate([ property(cc.Node) ], FashionItem.prototype, "nodeUse_arr", void 0);
      __decorate([ property(cc.Node) ], FashionItem.prototype, "nodeRole_arr", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], FashionItem.prototype, "display_arr", void 0);
      FashionItem = __decorate([ ccclass ], FashionItem);
      return FashionItem;
    }(cc.Component);
    exports.FashionItem = FashionItem;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  FashionRewardView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6fefcuCbGBJz7QzSdIFPOjg", "FashionRewardView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FashionRewardView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var FashionRewardView = function(_super) {
      __extends(FashionRewardView, _super);
      function FashionRewardView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeRole = void 0;
        _this.btnSure = void 0;
        _this.btnReturn = void 0;
        _this.roleAni = void 0;
        _this._DanceName = [];
        return _this;
      }
      FashionRewardView.prototype.click_btn_sure = function(e) {
        var _this = this;
        SDKManager_1.SDKManager.PlayVideo("\u65f6\u88c5\u9886\u53d6", function() {
          GameData_1.GameData.AddFashion(_this._nowFashion);
          GameData_1.GameData.UseFashion(_this._nowFashion);
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FashionRewardView);
        });
      };
      FashionRewardView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FashionRewardView);
      };
      FashionRewardView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this._nowFashion = data;
          this.PlayRoleAni();
        }
      };
      FashionRewardView.prototype.PlayRoleAni = function() {
        var _this = this;
        var fashionName = GameData_1.GameData.FashionData[this._nowFashion];
        if (0 == this._DanceName.length) {
          this._DanceName = [ "\u8df3\u821e1", "\u8df3\u821e2", "\u8d70\u8def" ];
          Util_1.Util.Random.ArrayBreakOrder(this._DanceName);
        }
        cc.Tween.stopAllByTarget(this.nodeRole);
        this.nodeRole.x = 0;
        var aniName = this._DanceName.shift();
        var timer = 2;
        var time = this.roleAni.armature().armatureData.getAnimation(fashionName + aniName).duration * timer;
        if ("\u8df3\u821e2" == aniName) {
          this._DanceName.unshift("\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3", "\u8df3\u821e3");
          console.log(this._DanceName);
          cc.tween(this.nodeRole).call(function() {
            _this.roleAni.playAnimation(fashionName + aniName, 2 * timer);
          }).delay(2 * time).call(function() {
            _this.PlayRoleAni();
          }).start();
        } else "\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3" == aniName ? cc.tween(this.nodeRole).call(function() {
          _this.roleAni.playAnimation(fashionName + aniName, timer / timer);
        }).delay(time / timer).call(function() {
          _this.PlayRoleAni();
        }).start() : cc.tween(this.nodeRole).call(function() {
          _this.roleAni.playAnimation(fashionName + aniName, timer);
        }).by(time, {
          x: 200
        }).to(0, {
          scaleX: -1
        }).call(function() {
          _this.roleAni.playAnimation(fashionName + aniName, 2 * timer);
        }).by(2 * time, {
          x: -400
        }).to(0, {
          scaleX: 1
        }).call(function() {
          _this.roleAni.playAnimation(fashionName + aniName, timer);
        }).by(time, {
          x: 200
        }).call(function() {
          _this.PlayRoleAni();
        }).start();
      };
      __decorate([ property(cc.Node) ], FashionRewardView.prototype, "nodeRole", void 0);
      __decorate([ property(cc.Button) ], FashionRewardView.prototype, "btnSure", void 0);
      __decorate([ property(cc.Button) ], FashionRewardView.prototype, "btnReturn", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], FashionRewardView.prototype, "roleAni", void 0);
      FashionRewardView = __decorate([ ccclass ], FashionRewardView);
      return FashionRewardView;
    }(cc.Component);
    exports.FashionRewardView = FashionRewardView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  FashionView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "727742Nb5dNF6tYrqgLSkaV", "FashionView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.FashionView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var FashionItem_1 = require("./FashionItem");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var FashionView = function(_super) {
      __extends(FashionView, _super);
      function FashionView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnReturn = void 0;
        _this.btnWife = void 0;
        _this.nodeLayout = void 0;
        _this.fashionItem = void 0;
        _this.role = void 0;
        return _this;
      }
      FashionView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.FashionView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      FashionView.prototype.click_btn_wife = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.WifeView);
      };
      FashionView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
         case EnumDef_1.UIInfo.RefreshView:
          this.ShowAllFashion();
        }
      };
      FashionView.prototype.ShowAllFashion = function() {
        var num = 0;
        this.nodeLayout.children.forEach(function(e) {
          return e.active = false;
        });
        for (var x = 0; x < GameData_1.GameData.FashionData.length; x += 3) {
          var node = this.nodeLayout.children[num];
          if (null == node) {
            node = cc.instantiate(this.fashionItem);
            node.setParent(this.nodeLayout);
          }
          node.getComponent(FashionItem_1.FashionItem).SetData(num);
          num++;
          node.active = true;
        }
        this.role.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 0);
      };
      __decorate([ property(cc.Button) ], FashionView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Button) ], FashionView.prototype, "btnWife", void 0);
      __decorate([ property(cc.Node) ], FashionView.prototype, "nodeLayout", void 0);
      __decorate([ property(cc.Prefab) ], FashionView.prototype, "fashionItem", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], FashionView.prototype, "role", void 0);
      FashionView = __decorate([ ccclass ], FashionView);
      return FashionView;
    }(cc.Component);
    exports.FashionView = FashionView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "./FashionItem": "FashionItem"
  } ],
  GameData: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "94111YdnipALp7Ajd+DlG6b", "GameData");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.GameData = exports.MissionData = void 0;
    var UmSdk_1 = require("../../../global/libs/UmSdk");
    var Util_1 = require("../../Core/Util");
    var BasePipe_1 = require("../mission/BasePipe");
    var SaveData = function() {
      function SaveData() {}
      SaveData.prototype.GetData = function() {
        var data = Util_1.Util.Save.GetData(this.name);
        for (var key in data) Object.prototype.hasOwnProperty.call(data, key) && (this[key] = data[key]);
      };
      SaveData.prototype.SaveData = function() {
        Util_1.Util.Save.SaveData(this.name, this);
      };
      return SaveData;
    }();
    var UserData = function(_super) {
      __extends(UserData, _super);
      function UserData() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.name = "user";
        _this.userName = "";
        _this.uuid = Util_1.Util.getUUid();
        _this.Mission = 1;
        _this.ShowComic = 0;
        _this.Money = 0;
        _this.Power = 5;
        _this.NowFashion = "\u521d\u59cb_";
        _this.AllFashion = [ "\u521d\u59cb_" ];
        _this.NowWife = "\u521d\u59cb_";
        _this.AllWife = [ "\u521d\u59cb_" ];
        _this.Sign = 0;
        _this.RoomDetail = "\u5ba2\u5385";
        _this.AllRoomDetail = {};
        _this.WinPercent = 0;
        _this.SdkChange = 0;
        return _this;
      }
      return UserData;
    }(SaveData);
    var SettingData = function(_super) {
      __extends(SettingData, _super);
      function SettingData() {
        var _this = _super.call(this) || this;
        _this.name = "setting";
        _this.sound = 1;
        _this.vibration = 1;
        _this.effect = 1;
        cc.audioEngine.setMusicVolume(_this.sound ? 1 : 0);
        cc.audioEngine.setEffectsVolume(_this.effect ? 1 : 0);
        return _this;
      }
      return SettingData;
    }(SaveData);
    var MissionData = function() {
      function MissionData() {}
      return MissionData;
    }();
    exports.MissionData = MissionData;
    var _GameData = function() {
      function _GameData() {
        this.UserData = new UserData();
        this.SettingData = new SettingData();
        this.MissionGold = 100;
        this.AdMoney = 500;
        this.Color = {
          Water: cc.Color.BLACK.fromHEX("0d8bf1"),
          Poison: cc.Color.BLACK.fromHEX("00cb11"),
          Health: cc.Color.BLACK.fromHEX("f0fb29"),
          Fire: cc.Color.BLACK.fromHEX("f1730d"),
          Ice: cc.Color.BLACK.fromHEX("acdaff"),
          None: new cc.Color(0, 0, 0, 0)
        };
        this.RoomData = [ "\u5ba2\u5385", "\u53a8\u623f", "\u9910\u5385", "\u5367\u5ba4", "\u6d74\u5ba4", "\u4e66\u623f", "\u5a31\u4e50", "\u5730\u4e0b\u5ba4", "\u513f\u7ae5\u623f", "\u8f66\u5e93", "\u5065\u8eab\u623f", "\u5c55\u5385" ];
        this.FashionData = [ "\u521d\u59cb_", "\u897f\u88c5_", "\u563b\u54c8_" ];
        this.FashionMoney = [ 0, 1e4, 1e4 ];
        this.WifeMoney = [ 0, 1e4, 1e4 ];
        this.WifeData = [ "\u521d\u59cb_", "\u897f\u88c5_", "\u563b\u54c8_" ];
        this.SignRewardMoney = [ 100, 200, 300, 400, 500, 600 ];
        this.RoomDetail = [ [ "\u5ba2\u5385", "\u6c99\u53d1", 1 ], [ "\u5ba2\u5385", "\u67dc\u5b50", 1 ], [ "\u5ba2\u5385", "\u6302\u753b", 1 ], [ "\u5ba2\u5385", "\u5730\u6bef", 1 ], [ "\u5ba2\u5385", "\u706f", 1 ], [ "\u5ba2\u5385", "\u7a97\u6237", 1 ], [ "\u5ba2\u5385", "\u5730\u6bef", 0 ], [ "\u5ba2\u5385", "\u5899", 0 ], [ "\u53a8\u623f", "\u67dc\u5b50", 1 ], [ "\u53a8\u623f", "\u6cb9\u70df\u673a", 1 ], [ "\u53a8\u623f", "\u6302\u753b", 1 ], [ "\u53a8\u623f", "\u67dc\u5b502", 1 ], [ "\u53a8\u623f", "\u5200", 1 ], [ "\u53a8\u623f", "\u67dc\u5b503", 1 ], [ "\u53a8\u623f", "\u5730\u9762", 0 ], [ "\u53a8\u623f", "\u5899", 0 ] ];
        this._MissionData = {};
        this._LiquidPower = [ BasePipe_1.Liquid.Ice, BasePipe_1.Liquid.Water, BasePipe_1.Liquid.Fire, BasePipe_1.Liquid.Poison, BasePipe_1.Liquid.Health ];
        this.allicon = [ "\u7f6a\u72af", "\u7f6a\u72af", "\u7f6a\u72af", "\u6076\u9b541", "\u8d27", "\u6076\u9b542", "\u7f6a\u72af", "\u7f6a\u72af", "\u6076\u9b542", "\u76ae\u80a4", "\u6076\u9b541", "\u6076\u9b542", "\u5973\u4eba", "\u5973\u4eba", "\u8d27", "\u6bd2\u86c71", "\u5973\u4eba", "\u5973\u4eba", "\u6bd2\u86c72", "\u76ae\u80a4", "\u52ab\u532a", "\u6076\u9b541", "\u5973\u4eba", "\u52ab\u532a", "\u8d27", "\u52ab\u532a", "\u5973\u4eba", "\u52ab\u532a", "\u6076\u9b542", "\u8d27" ];
        this.UserData.GetData();
        this.SettingData.GetData();
      }
      Object.defineProperty(_GameData.prototype, "MaxPower", {
        get: function() {
          return UmSdk_1.umSdk.MaxPower;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(_GameData.prototype, "AdPower", {
        get: function() {
          return UmSdk_1.umSdk.ADAddPower;
        },
        enumerable: false,
        configurable: true
      });
      _GameData.prototype.GetColorByLiquid = function(liquid) {
        switch (liquid) {
         case BasePipe_1.Liquid.Fire:
          return this.Color.Fire;

         case BasePipe_1.Liquid.Health:
          return this.Color.Health;

         case BasePipe_1.Liquid.Ice:
          return this.Color.Ice;

         case BasePipe_1.Liquid.Poison:
          return this.Color.Poison;

         case BasePipe_1.Liquid.Water:
          return this.Color.Water;

         default:
          return this.Color.None;
        }
      };
      _GameData.prototype.ParseData = function(type, json) {};
      _GameData.prototype.RefreshTimer = function() {};
      _GameData.prototype.getSuitName = function() {
        return this.UserData.NowFashion;
      };
      _GameData.prototype.HadFashion = function(fashion) {
        return this.UserData.AllFashion.includes(fashion);
      };
      _GameData.prototype.UseFashion = function(arg0) {
        this.UserData.NowFashion = this.FashionData[arg0];
        this.UserData.SaveData();
      };
      _GameData.prototype.AddFashion = function(arg0) {
        this.UserData.AllFashion.push(this.FashionData[arg0]);
        this.UserData.SaveData();
      };
      _GameData.prototype.HadWife = function(Wife) {
        return this.UserData.AllWife.includes(Wife);
      };
      _GameData.prototype.UseWife = function(arg0) {
        this.UserData.NowWife = this.WifeData[arg0];
        this.UserData.SaveData();
      };
      _GameData.prototype.AddWife = function(arg0) {
        this.UserData.AllWife.push(this.WifeData[arg0]);
        this.UserData.SaveData();
      };
      _GameData.prototype.GetRoomDetail = function(str) {
        return this.UserData.AllRoomDetail[str] || [];
      };
      _GameData.prototype.AddRoomDetail = function(str, name) {
        var detail = this.GetRoomDetail(str);
        detail.push(name);
        this.UserData.AllRoomDetail[str] = detail;
        this.UserData.SaveData();
      };
      _GameData.prototype.GetRoomType = function(nowRoom, Reward) {
        for (var x = 0; x < this.RoomDetail.length; x++) {
          var element = this.RoomDetail[x];
          if (element[0] == nowRoom && element[1] == Reward) return element[2];
        }
        return 0;
      };
      _GameData.prototype.GetNowRoomDetailReward = function() {
        var now = Math.floor(this.UserData.Mission / 5);
        return this.RoomDetail[now][1];
      };
      _GameData.prototype.GetLiquidPower = function(first, second) {
        var num1 = this._LiquidPower.indexOf(first);
        var num2 = this._LiquidPower.indexOf(second);
        return num1 < num2 ? first : second;
      };
      _GameData.prototype.GetMissionSmallIcon = function(arg0) {
        return this.allicon[arg0];
      };
      return _GameData;
    }();
    exports.GameData = new _GameData();
    cc._RF.pop();
  }, {
    "../../../global/libs/UmSdk": "UmSdk",
    "../../Core/Util": "Util",
    "../mission/BasePipe": "BasePipe"
  } ],
  GameEvent: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "33d0dF5ixlC6L8n2a3I/2bv", "GameEvent");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var GameEvent = function(_super) {
      __extends(GameEvent, _super);
      function GameEvent() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      GameEvent.prototype.CheckWin = function() {
        return true;
      };
      GameEvent = __decorate([ ccclass ], GameEvent);
      return GameEvent;
    }(cc.Component);
    exports.default = GameEvent;
    cc._RF.pop();
  }, {} ],
  GameLoading: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "dd013C9SsNNRJ/b2IHtu+PX", "GameLoading");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.GameLoading = void 0;
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var GameLoading = function(_super) {
      __extends(GameLoading, _super);
      function GameLoading() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.spriteProgressBg = void 0;
        _this.spriteProgress = void 0;
        return _this;
      }
      GameLoading.prototype.notif = function(uiinfo, body) {
        void 0 === body && (body = null);
      };
      __decorate([ property(cc.Sprite) ], GameLoading.prototype, "spriteProgressBg", void 0);
      __decorate([ property(cc.Sprite) ], GameLoading.prototype, "spriteProgress", void 0);
      GameLoading = __decorate([ ccclass ], GameLoading);
      return GameLoading;
    }(cc.Component);
    exports.GameLoading = GameLoading;
    cc._RF.pop();
  }, {} ],
  GameViewOld: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bbb0aGg0iZPLqN2eJqTe3Yd", "GameViewOld");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.GameView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var Main_1 = require("../../Main");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var Mission_1 = require("../mission/Mission");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var GameView = function(_super) {
      __extends(GameView, _super);
      function GameView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeMission = void 0;
        _this.btnReturn = void 0;
        return _this;
      }
      GameView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.GameView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      GameView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.ShowMission();
          break;

         case EnumDef_1.UIInfo.CheckWin:
          this._mission.CheckWin();
        }
      };
      GameView.prototype.ShowMission = function() {
        return __awaiter(this, void 0, void 0, function() {
          var main, prefab, node, waterShow, sprite;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              main = cc.find("Canvas").getComponent(Main_1.default);
              main.waterShow.node.active = false;
              this.nodeMission.destroyAllChildren();
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.mission, "" + GameData_1.GameData.UserData.Mission) ];

             case 1:
              prefab = _a.sent();
              node = cc.instantiate(prefab);
              node.setParent(this.nodeMission);
              node.setPosition(0, 0);
              this._mission = node.getComponent(Mission_1.default);
              waterShow = new cc.Node();
              waterShow.groupIndex = 0;
              waterShow.width = main.waterShow.node.width;
              waterShow.height = main.waterShow.node.height;
              waterShow.scaleY = -1;
              sprite = waterShow.addComponent(cc.Sprite);
              sprite.spriteFrame = main.waterShow.spriteFrame;
              sprite.trim = true;
              sprite.sizeMode = cc.Sprite.SizeMode.TRIMMED;
              sprite.setMaterial(0, main.waterShow.getMaterial(0));
              waterShow.setParent(node);
              waterShow.setPosition(0, 0, 0);
              waterShow.setSiblingIndex(1);
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeMission", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnReturn", void 0);
      GameView = __decorate([ ccclass ], GameView);
      return GameView;
    }(cc.Component);
    exports.GameView = GameView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../Main": "Main",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "../mission/Mission": "Mission"
  } ],
  GameView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6c134kv9e9EcLHSmcvZbL6n", "GameView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.GameView = void 0;
    var Platform_1 = require("../../../global/libs/Platform");
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var Main_1 = require("../../Main");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var Mission_1 = require("../mission/Mission");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var GameView = function(_super) {
      __extends(GameView, _super);
      function GameView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeMission = void 0;
        _this.btnReturn = void 0;
        _this.btnRefresh = void 0;
        _this.spritePowerPercent = void 0;
        _this.labelPowerNum = void 0;
        _this.btnPower = void 0;
        _this.labelMoneyNum = void 0;
        _this.btnMoney = void 0;
        _this.btnBuilding = void 0;
        _this.btnJump = void 0;
        _this.nodeProgressBg_arr = [];
        _this.nodeProgressGoldBg_arr = [];
        _this.spriteProgress_arr = [];
        _this.spriteProgressBlack_arr = [];
        _this.nodeFinish_arr = [];
        _this.nodeProgress_arr = [];
        _this.labelMission = void 0;
        return _this;
      }
      GameView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.GameView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      GameView.prototype.click_btn_refresh = function(e) {
        UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
      };
      GameView.prototype.click_btn_money = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.AddMoneyView);
      };
      GameView.prototype.click_btn_power = function(e) {
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.AddPowerView);
      };
      GameView.prototype.click_btn_building = function(e) {};
      GameView.prototype.click_btn_jump = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u6e38\u620f\u9875\u9762\u4e0b\u4e00\u5173", function() {
          UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.WinView);
        });
      };
      GameView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          Platform_1.platform.showBanner();

         case EnumDef_1.UIInfo.RefreshView:
          this.ShowMission();
          this.RefreshToken();
          break;

         case EnumDef_1.UIInfo.CheckWin:
          this._mission.CheckWin();
          break;

         case EnumDef_1.UIInfo.CheckFail:
          this._mission.CheckFail(msg);
          break;

         case EnumDef_1.UIInfo.BoomTank:
          this._mission.BoomTank(msg);
          break;

         case EnumDef_1.UIInfo.RefreshToken:
          this.RefreshToken();
          break;

         case EnumDef_1.UIInfo.CloseView:
        }
      };
      GameView.prototype.RefreshToken = function() {
        this.labelPowerNum.string = GameData_1.GameData.UserData.Power + "/" + GameData_1.GameData.MaxPower;
        this.spritePowerPercent.fillRange = GameData_1.GameData.UserData.Power / GameData_1.GameData.MaxPower;
        this.labelMoneyNum.string = "" + GameData_1.GameData.UserData.Money;
      };
      GameView.prototype.ShowMission = function() {
        return __awaiter(this, void 0, void 0, function() {
          var num, main, prefab, node, waterShow, sprite;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (GameData_1.GameData.UserData.Power <= 0) {
                UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.GameView);
                UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
                UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.LackPowerView);
                return [ 2 ];
              }
              num = GameData_1.GameData.UserData.Mission % 5;
              if (0 == num) {
                this.spriteProgressBlack_arr.forEach(function(e, x) {
                  return e.node.active = x < 4;
                });
                this.nodeFinish_arr.forEach(function(e, x) {
                  return e.active = x < 4;
                });
                this.nodeProgressGoldBg_arr.forEach(function(e, x) {
                  return e.active = 4 == x;
                });
              } else {
                this.spriteProgressBlack_arr.forEach(function(e, x) {
                  return e.node.active = x < num - 1;
                });
                this.nodeFinish_arr.forEach(function(e, x) {
                  return e.active = x < num - 1;
                });
                this.nodeProgressGoldBg_arr.forEach(function(e, x) {
                  return e.active = x == num - 1;
                });
              }
              this.spriteProgress_arr.forEach(function(e, x) {
                return Util_1.Util.Res.LoadAtlas(e, GameData_1.GameData.GetMissionSmallIcon(GameData_1.GameData.UserData.Mission - num + x));
              });
              this.spriteProgressBlack_arr.forEach(function(e, x) {
                return Util_1.Util.Res.LoadAtlas(e, GameData_1.GameData.GetMissionSmallIcon(GameData_1.GameData.UserData.Mission - num + x));
              });
              this.labelMission.string = "\u5173\u5361" + GameData_1.GameData.UserData.Mission;
              main = cc.find("Canvas").getComponent(Main_1.default);
              main.waterCamera.enabled = true;
              main.waterShow.node.active = false;
              this.nodeMission.destroyAllChildren();
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.mission, "" + GameData_1.GameData.UserData.Mission) ];

             case 1:
              prefab = _a.sent();
              if (null == prefab) {
                GameData_1.GameData.UserData.Mission = 1;
                this.ShowMission();
                return [ 2 ];
              }
              GameData_1.GameData.UserData.Power--;
              UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
              GameData_1.GameData.UserData.SaveData();
              cc.director.getCollisionManager().enabled = true;
              cc.director.getPhysicsManager().enabled = true;
              node = cc.instantiate(prefab);
              node.setParent(this.nodeMission);
              node.setPosition(0, 0);
              this._mission = node.getComponent(Mission_1.default);
              waterShow = new cc.Node();
              waterShow.groupIndex = 0;
              waterShow.width = main.waterShow.node.width;
              waterShow.height = main.waterShow.node.height;
              sprite = waterShow.addComponent(cc.Sprite);
              sprite.spriteFrame = main.waterShow.spriteFrame;
              sprite.trim = true;
              sprite.sizeMode = cc.Sprite.SizeMode.TRIMMED;
              sprite.setMaterial(0, main.waterShow.getMaterial(0));
              waterShow.setParent(node);
              waterShow.scaleY = -1 / node.scaleY;
              waterShow.scaleX = 1 / node.scaleX;
              waterShow.setPosition(0, 0, 0);
              waterShow.setSiblingIndex(1);
              SDKManager_1.SDKManager.ReportAnalytics(EnumDef_1.AnalyticsEnum.ShowMission, {
                mission: GameData_1.GameData.UserData.Mission
              });
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeMission", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnRefresh", void 0);
      __decorate([ property(cc.Sprite) ], GameView.prototype, "spritePowerPercent", void 0);
      __decorate([ property(cc.Label) ], GameView.prototype, "labelPowerNum", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnPower", void 0);
      __decorate([ property(cc.Label) ], GameView.prototype, "labelMoneyNum", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnMoney", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnBuilding", void 0);
      __decorate([ property(cc.Button) ], GameView.prototype, "btnJump", void 0);
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeProgressBg_arr", void 0);
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeProgressGoldBg_arr", void 0);
      __decorate([ property(cc.Sprite) ], GameView.prototype, "spriteProgress_arr", void 0);
      __decorate([ property(cc.Sprite) ], GameView.prototype, "spriteProgressBlack_arr", void 0);
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeFinish_arr", void 0);
      __decorate([ property(cc.Node) ], GameView.prototype, "nodeProgress_arr", void 0);
      __decorate([ property(cc.Label) ], GameView.prototype, "labelMission", void 0);
      GameView = __decorate([ ccclass ], GameView);
      return GameView;
    }(cc.Component);
    exports.GameView = GameView;
    cc._RF.pop();
  }, {
    "../../../global/libs/Platform": "Platform",
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../Main": "Main",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "../mission/Mission": "Mission"
  } ],
  Joint: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c108bfRnopIOa9Ie4Nxzp3g", "Joint");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Util_1 = require("../../Core/Util");
    var EnumDef_1 = require("../EnumDef");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var jointType;
    (function(jointType) {
      jointType[jointType["effect"] = 0] = "effect";
      jointType[jointType["create"] = 1] = "create";
    })(jointType || (jointType = {}));
    var Joint = function(_super) {
      __extends(Joint, _super);
      function Joint() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.jointType = jointType.effect;
        _this._allChild = [];
        _this._canCreate = false;
        return _this;
      }
      Joint_1 = Joint;
      Object.defineProperty(Joint.prototype, "TargetJoint", {
        get: function() {
          return this._target;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(Joint.prototype, "pipe", {
        get: function() {
          null == this._pipe && null != this.node.parent && (this._pipe = this.node.parent.getComponent(BasePipe_1.default));
          return this._pipe;
        },
        set: function(v) {
          this._pipe = v;
        },
        enumerable: false,
        configurable: true
      });
      Joint.prototype.onCollisionEnter = function(other, self) {
        var joint = other.node.getComponent(Joint_1);
        if ("\u66f2\u7ba1" == joint.node.parent.name && "\u66f2\u7ba1" == this.node.parent.name) return;
        if (joint) {
          if (null != joint._target) return;
          if (null != this._target) return;
          this._target = joint;
          this.pipe.AddJoint(this);
          this._target._target = this;
          this._target.pipe.AddJoint(this._target);
        }
      };
      Joint.prototype.Input = function(Liquid) {
        this.DestroyAllChild();
        this._liquid = Liquid;
        this.pipe.Input(Liquid, this);
      };
      Joint.prototype.DestroyAllChild = function() {
        for (var x = 0; x < this.node.childrenCount; x++) {
          var element = this.node.children[x];
          element.name.includes("New Sprite") || element.name.includes("\u63a5\u53e3") || element.destroy();
        }
        this._canCreate = false;
      };
      Joint.prototype.CreateObj = function(liquid) {
        return __awaiter(this, void 0, void 0, function() {
          var prefab, _a;
          var _this = this;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              cc.Tween.stopAllByTarget(this.node);
              liquid != BasePipe_1.Liquid.Empty && this.DestroyAllChild();
              this._canCreate = true;
              _a = liquid;
              switch (_a) {
               case BasePipe_1.Liquid.Water:
                return [ 3, 1 ];

               case BasePipe_1.Liquid.Health:
                return [ 3, 3 ];

               case BasePipe_1.Liquid.Poison:
                return [ 3, 5 ];

               case BasePipe_1.Liquid.Fire:
                return [ 3, 7 ];

               case BasePipe_1.Liquid.Ice:
                return [ 3, 9 ];

               case BasePipe_1.Liquid.Empty:
                return [ 3, 11 ];
              }
              return [ 3, 12 ];

             case 1:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u6c34") ];

             case 2:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 3:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u751f\u547d") ];

             case 4:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 5:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u6bd2\u6db2") ];

             case 6:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 7:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u5ca9\u6d46") ];

             case 8:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 9:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u51b0") ];

             case 10:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 11:
              return [ 2 ];

             case 12:
              return [ 3, 13 ];

             case 13:
              cc.tween(this.node).then(cc.tween(this.node).delay(Util_1.Util.Random.getRandom(5, 15) / 100).call(function() {
                var node = cc.instantiate(prefab);
                node.setParent(_this.node);
                node.setPosition(Util_1.Util.Random.getRandom(-20, 20), -10, 0);
              })).repeat(45).start();
              return [ 2 ];
            }
          });
        });
      };
      Joint.prototype.Output = function(Liquid) {
        if (this.TargetJoint) {
          this.DestroyAllChild();
          this.TargetJoint.Input(Liquid);
        } else switch (this.jointType) {
         case jointType.effect:
          this.ShowEffect(Liquid);
          break;

         case jointType.create:
          this.CreateObj(Liquid);
        }
      };
      Joint.prototype.ShowEffect = function(liquid) {
        return __awaiter(this, void 0, void 0, function() {
          var prefab, _a, node;
          return __generator(this, function(_b) {
            switch (_b.label) {
             case 0:
              cc.Tween.stopAllByTarget(this.node);
              this.DestroyAllChild();
              this._canCreate = true;
              _a = liquid;
              switch (_a) {
               case BasePipe_1.Liquid.Water:
                return [ 3, 1 ];

               case BasePipe_1.Liquid.Health:
                return [ 3, 3 ];

               case BasePipe_1.Liquid.Poison:
                return [ 3, 5 ];

               case BasePipe_1.Liquid.Fire:
                return [ 3, 7 ];

               case BasePipe_1.Liquid.Ice:
                return [ 3, 9 ];

               case BasePipe_1.Liquid.Empty:
                return [ 3, 11 ];
              }
              return [ 3, 12 ];

             case 1:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "shui") ];

             case 2:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 3:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "shengming") ];

             case 4:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 5:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "du") ];

             case 6:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 7:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "huo") ];

             case 8:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 9:
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "bing") ];

             case 10:
              prefab = _b.sent();
              return [ 3, 13 ];

             case 11:
              return [ 2 ];

             case 12:
              return [ 3, 13 ];

             case 13:
              node = cc.instantiate(prefab);
              node.setParent(this.node);
              node.setPosition(0, -10, 0);
              this._canCreate || node.destroy();
              return [ 2 ];
            }
          });
        });
      };
      Joint.prototype.Reset = function() {
        if (null != this._target) {
          var now = this._target;
          this._target = null;
          now.Reset();
          this.pipe.RemoveJoint(this);
        }
      };
      var Joint_1;
      __decorate([ property({
        type: cc.Enum(jointType)
      }) ], Joint.prototype, "jointType", void 0);
      __decorate([ property(BasePipe_1.default) ], Joint.prototype, "pipe", null);
      Joint = Joint_1 = __decorate([ ccclass ], Joint);
      return Joint;
    }(cc.Component);
    exports.default = Joint;
    cc._RF.pop();
  }, {
    "../../Core/Util": "Util",
    "../EnumDef": "EnumDef",
    "./BasePipe": "BasePipe"
  } ],
  LackPowerView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a4aadx44wBIB4cAdpbwIS3R", "LackPowerView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.LackPowerView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var LackPowerView = function(_super) {
      __extends(LackPowerView, _super);
      function LackPowerView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnClose = void 0;
        _this.labelAdReward = void 0;
        _this.btnAdReward = void 0;
        return _this;
      }
      LackPowerView.prototype.click_btn_close = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.LackPowerView);
      };
      LackPowerView.prototype.click_btn_ad_reward = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u52a0\u4f53\u529b\u5f39\u7a97", function() {
          GameData_1.GameData.UserData.Power += GameData_1.GameData.AdPower;
          GameData_1.GameData.UserData.SaveData();
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.LackPowerView);
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
        });
      };
      LackPowerView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.labelAdReward.string = "" + GameData_1.GameData.AdPower;
        }
      };
      __decorate([ property(cc.Button) ], LackPowerView.prototype, "btnClose", void 0);
      __decorate([ property(cc.Label) ], LackPowerView.prototype, "labelAdReward", void 0);
      __decorate([ property(cc.Button) ], LackPowerView.prototype, "btnAdReward", void 0);
      LackPowerView = __decorate([ ccclass ], LackPowerView);
      return LackPowerView;
    }(cc.Component);
    exports.LackPowerView = LackPowerView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  Main: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9f4ccv6WsFFBaw0Gng948Fw", "Main");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ExportPageController_1 = require("../global/libs/ExportPageController");
    var Platform_1 = require("../global/libs/Platform");
    var PopUp_1 = require("../global/libs/PopUp");
    var UmSdk_1 = require("../global/libs/UmSdk");
    var ResManager_1 = require("./Core/ResManager");
    var UIManager_1 = require("./Core/UIManager");
    var Util_1 = require("./Core/Util");
    var GameData_1 = require("./modules/data/GameData");
    var EnumDef_1 = require("./modules/EnumDef");
    var SDKManager_1 = require("./SDK/SDKManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Main = function(_super) {
      __extends(Main, _super);
      function Main() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.UIRoot = null;
        _this.waterCamera = null;
        _this.waterShow = null;
        _this.percent = null;
        _this.loadingText = null;
        _this.nodeExport = null;
        _this._RefreshTime = 1;
        _this._load = false;
        _this._fillPercent = 0;
        _this._LoadingText = [];
        return _this;
      }
      Main.prototype.start = function() {
        var _this = this;
        setTimeout(function() {
          _this.Chengetext();
          cc.game.setFrameRate(60);
          cc.game.on(cc.game.EVENT_SHOW, function() {
            UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.GameShow);
          });
          cc.game.on(cc.game.EVENT_HIDE, function() {
            UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.GameHide);
          });
          _this._run();
        }, 100);
      };
      Main.prototype._run = function() {
        return __awaiter(this, void 0, void 0, function() {
          var texture, spriteFrame;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              SDKManager_1.SDKManager.Init();
              cc.game.addPersistRootNode(this.node);
              UIManager_1.UIManager.InitUILayer(this.UIRoot);
              return [ 4, ResManager_1.ResManager.BeginLoad() ];

             case 1:
              _a.sent();
              return [ 4, Util_1.Util.Scene.LoadScene("MainView") ];

             case 2:
              _a.sent();
              return [ 4, this.loadChiChengGameSDK() ];

             case 3:
              _a.sent();
              Util_1.Util.Audio.PlayBGM(EnumDef_1.AudioType.audio_bgm);
              SDKManager_1.SDKManager.CreateCustomAd();
              if (0 == GameData_1.GameData.UserData.ShowComic) {
                GameData_1.GameData.UserData.ShowComic = 1;
                GameData_1.GameData.UserData.SaveData();
                SDKManager_1.SDKManager.ReportAnalytics("\u7b2c\u4e00\u6b21\u8fdb\u5165\u6e38\u620f", {
                  status: "\u5c55\u793a"
                });
                UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.GameView);
              } else UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
              texture = new cc.RenderTexture();
              texture.initWithSize(this.waterShow.node.width, this.waterShow.node.height);
              spriteFrame = new cc.SpriteFrame();
              spriteFrame.setTexture(texture);
              this.waterCamera.targetTexture = texture;
              this.waterShow.spriteFrame = spriteFrame;
              this._load = true;
              this.percent.node.parent.destroyAllChildren();
              SDKManager_1.SDKManager.ReportAnalytics("\u52a0\u8f7d", {
                status: "finish"
              });
              return [ 2 ];
            }
          });
        });
      };
      Main.prototype.update = function(t) {
        if (!this._load) {
          this._fillPercent += Util_1.Util.Random.getRandom(1, 3);
          this.percent.fillRange = this._fillPercent / 100 > 1 ? 1 : this._fillPercent / 100;
          if (this._fillPercent > 100) {
            this._fillPercent = 0;
            this.Chengetext();
          }
        }
      };
      Main.prototype.Chengetext = function() {
        if (0 == this._LoadingText.length) {
          this._LoadingText = [ "\u6c34\u7ba1\u5de5\u6b63\u5728\u52aa\u529b\u901a\u6c34", "\u5c4e\u5751\u7206\u4e86\uff0c\u6c34\u7ba1\u5de5\u6b63\u5728\u8d76\u6765", "\u5395\u6240\u91cc\u9762\u53d1\u73b0\u4e86\u5965\u91cc\u7ed9", "\u6b63\u5728\u758f\u901a\u5965\u5229\u7ed9" ];
          Util_1.Util.Random.ArrayBreakOrder(this._LoadingText);
        }
        this.loadingText.string = this._LoadingText.shift();
      };
      Main.prototype.loadChiChengGameSDK = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, ExportPageController_1.exportPageController.init(this.nodeExport) ];

             case 1:
              _a.sent();
              return [ 4, UmSdk_1.umSdk.init() ];

             case 2:
              _a.sent();
              return [ 4, Platform_1.platform.init() ];

             case 3:
              _a.sent();
              SDKManager_1.SDKManager.ReportAnalytics("\u52a0\u8f7d", {
                status: "Begin"
              });
              return [ 4, PopUp_1.popup.init() ];

             case 4:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property(cc.Node) ], Main.prototype, "UIRoot", void 0);
      __decorate([ property(cc.Camera) ], Main.prototype, "waterCamera", void 0);
      __decorate([ property(cc.Sprite) ], Main.prototype, "waterShow", void 0);
      __decorate([ property(cc.Sprite) ], Main.prototype, "percent", void 0);
      __decorate([ property(cc.Label) ], Main.prototype, "loadingText", void 0);
      __decorate([ property(cc.Prefab) ], Main.prototype, "nodeExport", void 0);
      Main = __decorate([ ccclass ], Main);
      return Main;
    }(cc.Component);
    exports.default = Main;
    cc._RF.pop();
  }, {
    "../global/libs/ExportPageController": "ExportPageController",
    "../global/libs/Platform": "Platform",
    "../global/libs/PopUp": "PopUp",
    "../global/libs/UmSdk": "UmSdk",
    "./Core/ResManager": "ResManager",
    "./Core/UIManager": "UIManager",
    "./Core/Util": "Util",
    "./SDK/SDKManager": "SDKManager",
    "./modules/EnumDef": "EnumDef",
    "./modules/data/GameData": "GameData"
  } ],
  Mission: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "d929b3L4TRCH6HDrg0bZhlP", "Mission");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var Main_1 = require("../../Main");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var BaseRole_1 = require("./BaseRole");
    var EmptyTank_1 = require("./EmptyTank");
    var RoleHero_1 = require("./RoleHero");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var Mission = function(_super) {
      __extends(Mission, _super);
      function Mission() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._win = false;
        _this._fail = false;
        return _this;
      }
      Object.defineProperty(Mission.prototype, "AllRole", {
        get: function() {
          null == this._BaseRole && (this._BaseRole = this.node.getComponentsInChildren(BaseRole_1.default));
          return this._BaseRole;
        },
        set: function(v) {
          this._BaseRole = v;
        },
        enumerable: false,
        configurable: true
      });
      Mission.prototype.CheckWin = function() {
        var _this = this;
        if (this._win) return;
        for (var x = 0; x < this.AllRole.length; x++) {
          var element = this.AllRole[x];
          if (!element.CheckSuccess()) {
            console.log(element);
            return;
          }
        }
        SDKManager_1.SDKManager.ReportAnalytics(EnumDef_1.AnalyticsEnum.MissionWin, {
          mission: GameData_1.GameData.UserData.Mission
        });
        this._win = true;
        cc.director.getCollisionManager().enabled = false;
        cc.director.getPhysicsManager().enabled = false;
        cc.tween(this.node).delay(1).call(function() {
          UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.WinView);
          _this.AllRole.forEach(function(e) {
            if (e instanceof RoleHero_1.default) {
              e.playAni || e.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u80dc\u5229", 0);
              var scale = 2;
              cc.tween(_this.node).to(.8, {
                scale: scale,
                x: -e.node.position.x * scale,
                y: -(e.node.position.y + e.Ani.node.height / 3 / scale) * scale
              }).call(function() {
                console.log(e.node.position.x, e.node.position.y);
              }).start();
              cc.find("Canvas").getComponent(Main_1.default).waterCamera.enabled = false;
            }
          });
        }).start();
      };
      Mission.prototype.CheckFail = function(arg0) {
        var _this = this;
        if (this._fail) return;
        SDKManager_1.SDKManager.ReportAnalytics(EnumDef_1.AnalyticsEnum.MissionFail, {
          mission: GameData_1.GameData.UserData.Mission
        });
        this._fail = true;
        cc.director.getCollisionManager().enabled = false;
        cc.director.getPhysicsManager().enabled = false;
        cc.tween(this.node).delay(1).call(function() {
          UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.FailView);
          _this.AllRole.forEach(function(e) {
            if (e == arg0) {
              var scale = 2;
              cc.tween(_this.node).to(.8, {
                scale: scale,
                x: -e.node.position.x * scale,
                y: -(e.node.position.y + e.Ani.node.height / 3 / scale) * scale
              }).call(function() {
                console.log(e.node.position.x, e.node.position.y);
              }).start();
              cc.find("Canvas").getComponent(Main_1.default).waterCamera.enabled = false;
            }
          });
        }).start();
      };
      Mission.prototype.BoomTank = function(arg0) {
        return __awaiter(this, void 0, void 0, function() {
          var allTank, _loop_1, this_1, x, state_1;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              allTank = this.node.getComponentsInChildren(EmptyTank_1.default);
              _loop_1 = function(x) {
                var element, prefab, node_1;
                return __generator(this, function(_a) {
                  switch (_a.label) {
                   case 0:
                    element = allTank[x];
                    if (!(cc.Vec3.distance(arg0.node, element.node) < 110)) return [ 3, 2 ];
                    return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u7206\u5f00") ];

                   case 1:
                    prefab = _a.sent();
                    node_1 = cc.instantiate(prefab);
                    node_1.setParent(this_1.node);
                    node_1.setPosition(element.node.position);
                    cc.tween(element.node).to(.5, {
                      opacity: 0
                    }).call(function() {
                      node_1.destroy();
                    }).start();
                    return [ 2, "break" ];

                   case 2:
                    return [ 2 ];
                  }
                });
              };
              this_1 = this;
              x = 0;
              _a.label = 1;

             case 1:
              if (!(x < allTank.length)) return [ 3, 4 ];
              return [ 5, _loop_1(x) ];

             case 2:
              state_1 = _a.sent();
              if ("break" === state_1) return [ 3, 4 ];
              _a.label = 3;

             case 3:
              x++;
              return [ 3, 1 ];

             case 4:
              return [ 2 ];
            }
          });
        });
      };
      __decorate([ property([ BaseRole_1.default ]) ], Mission.prototype, "AllRole", null);
      Mission = __decorate([ ccclass ], Mission);
      return Mission;
    }(cc.Component);
    exports.default = Mission;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../Main": "Main",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "./BaseRole": "BaseRole",
    "./EmptyTank": "EmptyTank",
    "./RoleHero": "RoleHero"
  } ],
  MoreGamePage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5a206aQ3rZDWKKEUM4M1r/Z", "MoreGamePage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Platform_1 = require("../../global/libs/Platform");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var ExportLayout_1 = require("./ExportLayout");
    var ExportPage_1 = require("./ExportPage");
    var SDKEnum_1 = require("./SDKEnum");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MoreGamePage = function(_super) {
      __extends(MoreGamePage, _super);
      function MoreGamePage() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.exportLayout = null;
        _this.BackBtn = null;
        _this.type = SDKEnum_1.PageType.MoreGamePage;
        _this._gameIndex = 0;
        _this.returnPageNum = 0;
        return _this;
      }
      MoreGamePage.prototype.start = function() {
        this.BackBtn.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
      };
      MoreGamePage.prototype.OnPageShow = function() {
        this.unscheduleAllCallbacks();
        this.scheduleOnce(this._continueGame, 2.5);
        var returnControl = UmSdk_1.umSdk.getOnLineGameValue(9);
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        var isOpen = false;
        this.type == SDKEnum_1.PageType.MoreGamePage_Old && Platform_1.platform.showCustomAd([ 0 ]);
        isOpen = true;
        Platform_1.platform.hideBanner();
        if (this.type != SDKEnum_1.PageType.MoreGamePage) {
          if (this.type == SDKEnum_1.PageType.MoreGamePage_Old) return;
          cc.tween(this.node).delay(1).call(function() {
            isOpen && Platform_1.platform.showBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.hideBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.showBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.hideBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.showBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.hideBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.showBanner();
          }).delay(1.5).call(function() {
            isOpen && Platform_1.platform.hideBanner();
          }).start();
        } else isOpen && this.openAction();
      };
      MoreGamePage.prototype.OnPageHide = function() {
        this.node.stopAllActions();
        Platform_1.platform.hideBanner();
        Platform_1.platform.hideCustomAd([ 0 ]);
      };
      MoreGamePage.prototype._onTouchEnd = function() {
        this.node.parent.getComponent(ExportPage_1.default).ClosePage(this.type);
        Platform_1.platform.showBanner();
      };
      MoreGamePage.prototype._continueGame = function() {
        var num = Number(this.exportLayout.showNumber);
        if (!num) return;
        this._gameIndex = Math.floor(Math.random() * num);
        this.exportLayout.JumpToGame(this._gameIndex);
        this._gameIndex++;
      };
      MoreGamePage.prototype.openAction = function() {
        if (1 == UmSdk_1.umSdk.getOnLineGameValue(1)) {
          var bannerErrorDelay = UmSdk_1.umSdk.getOnLineGameValue(2);
          var bannerDelayTime = UmSdk_1.umSdk.getOnLineGameValue(3);
          console.log("PopUpGameEndPage \u8bef\u70b9", bannerErrorDelay, bannerDelayTime);
          this.BackBtn.getComponent(cc.Widget).enabled = true;
          this.BackBtn.getComponent(cc.Widget).bottom = 0;
          this.BackBtn.getComponent(cc.Widget).updateAlignment();
          cc.Tween.stopAllByTarget(this.BackBtn);
          cc.tween(this.BackBtn).delay(bannerDelayTime).call(function() {
            console.log("showBanner");
            Platform_1.platform.showBanner();
          }).delay(bannerErrorDelay).call(function() {
            console.log("hideBanner");
            Platform_1.platform.hideBanner();
          }).start();
        }
      };
      __decorate([ property({
        type: ExportLayout_1.default
      }) ], MoreGamePage.prototype, "exportLayout", void 0);
      __decorate([ property({
        tooltip: "\u8fd4\u56de\u6309\u94ae",
        type: cc.Node
      }) ], MoreGamePage.prototype, "BackBtn", void 0);
      __decorate([ property({
        type: cc.Enum(SDKEnum_1.PageType)
      }) ], MoreGamePage.prototype, "type", void 0);
      MoreGamePage = __decorate([ ccclass ], MoreGamePage);
      return MoreGamePage;
    }(cc.Component);
    exports.default = MoreGamePage;
    cc._RF.pop();
  }, {
    "../../global/libs/Platform": "Platform",
    "../../global/libs/UmSdk": "UmSdk",
    "./ExportLayout": "ExportLayout",
    "./ExportPage": "ExportPage",
    "./SDKEnum": "SDKEnum"
  } ],
  MusicUtil: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "f0d1bsbdB1Bh7XErGsaJ5MW", "MusicUtil");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.PhoneShakeType = exports.EffectContineMusic = exports.EffectMusic = exports.BgMusic = void 0;
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var BgDefaultValue = .2;
    var EffectDefaultValue = 1;
    var GameSettingKey = "GameSetting_PuzzelBox";
    exports.BgMusic = cc.Enum({
      start: 1,
      game: 2
    });
    exports.EffectMusic = cc.Enum({
      buttonEffect: 0,
      Escape_blockPass: 1,
      WinViewEffect: 2,
      LoseViewEffect: 3,
      BlockMove: 4,
      sortNumwave: 5,
      Revive: 6,
      Move_2048: 7,
      BigNum_2048: 8
    });
    exports.EffectContineMusic = cc.Enum({
      rollball: 1,
      Escape_touchBlock: 2
    });
    var PhoneShakeType;
    (function(PhoneShakeType) {
      PhoneShakeType[PhoneShakeType["Short"] = 1] = "Short";
      PhoneShakeType[PhoneShakeType["Long"] = 2] = "Long";
    })(PhoneShakeType = exports.PhoneShakeType || (exports.PhoneShakeType = {}));
    var BgMusicType = function() {
      function BgMusicType() {
        this.soundID = 0;
        this.soundClip = null;
      }
      __decorate([ property({
        type: exports.BgMusic,
        displayName: "\u80cc\u666f\u97f3\u4e50id"
      }) ], BgMusicType.prototype, "soundID", void 0);
      __decorate([ property({
        type: cc.AudioClip,
        displayName: "\u97f3\u6548\u8d44\u6e90"
      }) ], BgMusicType.prototype, "soundClip", void 0);
      BgMusicType = __decorate([ ccclass("BgMusicType") ], BgMusicType);
      return BgMusicType;
    }();
    var EffectMusicType = function() {
      function EffectMusicType() {
        this.effectID = 0;
        this.effectClip = null;
      }
      __decorate([ property({
        type: exports.EffectMusic,
        displayName: "\u97f3\u6548id"
      }) ], EffectMusicType.prototype, "effectID", void 0);
      __decorate([ property({
        type: cc.AudioClip,
        displayName: "\u97f3\u6548\u8d44\u6e90"
      }) ], EffectMusicType.prototype, "effectClip", void 0);
      EffectMusicType = __decorate([ ccclass("EffectMusicType") ], EffectMusicType);
      return EffectMusicType;
    }();
    var EffectContineType = function() {
      function EffectContineType() {
        this.effectContineID = 0;
        this.effectContine = [];
        this.index = 0;
      }
      __decorate([ property({
        type: exports.EffectContineMusic,
        displayName: "\u97f3\u6548id"
      }) ], EffectContineType.prototype, "effectContineID", void 0);
      __decorate([ property({
        type: [ cc.AudioClip ],
        displayName: "\u97f3\u6548\u8d44\u6e90\u7ec4"
      }) ], EffectContineType.prototype, "effectContine", void 0);
      EffectContineType = __decorate([ ccclass("EffectContineType") ], EffectContineType);
      return EffectContineType;
    }();
    var MusicUtil = function(_super) {
      __extends(MusicUtil, _super);
      function MusicUtil() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.bgMusicArrays = [];
        _this.x1 = null;
        _this.effectMusicArrays = [];
        _this.x2 = null;
        _this.effectContineArrays = [];
        return _this;
      }
      MusicUtil_1 = MusicUtil;
      MusicUtil.prototype.onLoad = function() {
        cc.game.addPersistRootNode(this.node);
        MusicUtil_1._instance = this;
        MusicUtil_1.GameSetting = MusicUtil_1.GetGameSetting();
        MusicUtil_1.CheckMusicMute();
        MusicUtil_1._initTypeMap();
      };
      MusicUtil.prototype.onDestroy = function() {
        MusicUtil_1._instance = null;
        MusicUtil_1._effectTypeMap = null;
      };
      MusicUtil.prototype.start = function() {
        MusicUtil_1.PlayBgMusic(exports.BgMusic.start);
      };
      MusicUtil.prototype.onEnable = function() {};
      MusicUtil._initTypeMap = function() {
        var keys = Object.keys(exports.EffectMusic);
        keys.push.apply(keys, Object.keys(exports.EffectContineMusic));
        for (var i = 0; i < keys.length; i++) MusicUtil_1._effectTypeMap[keys[i]] = [];
      };
      MusicUtil._addTypeMap = function(type, uuid) {
        var map = null;
        map = MusicUtil_1._effectTypeMap[type];
        if (!map) {
          console.error("\u6ca1\u6709\u97f3\u4e50type:", type);
          return;
        }
        map.push(uuid);
      };
      MusicUtil.CheckInit = function() {
        if (null == MusicUtil_1._instance) {
          console.error("\u97f3\u6548\u7ec4\u4ef6\u672a\u5b9e\u4f8b\u5316");
          return false;
        }
        return true;
      };
      MusicUtil.PlayBgMusic = function(bgID) {
        if (!this.CheckInit()) return;
        var instance = MusicUtil_1._instance;
        var bgArray = instance.bgMusicArrays;
        var clip = null;
        for (var i in bgArray) if (bgArray[i].soundID == bgID) {
          clip = bgArray[i].soundClip;
          break;
        }
        if (!clip) {
          console.error("\u68c0\u67e5\u80cc\u666f\u97f3\u4e50\u8d44\u6e90", "---bgID\uff1a" + bgID + " ---");
          return;
        }
        console.log("\u97f3\u4e50\u8d44\u6e90", "---bgID\uff1a" + bgID + " ---");
        cc.audioEngine.stopMusic();
        cc.audioEngine.playMusic(clip, true);
        cc.audioEngine.setMusicVolume(MusicUtil_1.GameSetting.bgValue);
      };
      MusicUtil.PlayEffectMusic = function(effectID, isCover, isCD) {
        var _this = this;
        void 0 === isCover && (isCover = false);
        void 0 === isCD && (isCD = false);
        if (!this.CheckInit()) return;
        var instance = MusicUtil_1._instance;
        var effectArray = instance.effectMusicArrays;
        var clip = null;
        for (var i in effectArray) if (effectArray[i].effectID == effectID) {
          clip = effectArray[i].effectClip;
          break;
        }
        if (!clip) {
          console.error("\u68c0\u67e5\u97f3\u6548\u8d44\u6e90", "---effectID\uff1a" + effectID + " ---");
          return;
        }
        if (isCD && this._effectCD) return;
        this._effectCD = true;
        this._instance.scheduleOnce(function() {
          _this._effectCD = false;
        }, this._effectCDTime);
        isCover && this.stopAllMusic();
        var uuid = cc.audioEngine.playEffect(clip, false);
        this._addTypeMap(exports.EffectMusic[effectID], uuid);
        return uuid;
      };
      MusicUtil.PlayEffectMusicByTime = function(effectID, time, isCover, isCD) {
        void 0 === isCover && (isCover = false);
        void 0 === isCD && (isCD = false);
        return __awaiter(this, void 0, void 0, function() {
          var instance, effectArray, clip, i, id;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (!this.CheckInit()) return [ 2 ];
              instance = MusicUtil_1._instance;
              effectArray = instance.effectMusicArrays;
              clip = null;
              for (i in effectArray) if (effectArray[i].effectID == effectID) {
                clip = effectArray[i].effectClip;
                break;
              }
              if (!clip) {
                console.error("\u68c0\u67e5\u97f3\u6548\u8d44\u6e90", "---effectID\uff1a" + effectID + " ---");
                return [ 2 ];
              }
              if (isCD && this._effectCD) return [ 2 ];
              this._effectCD = true;
              this._instance.scheduleOnce(function() {
                _this._effectCD = false;
              }, this._effectCDTime);
              isCover && this.stopAllMusic();
              id = cc.audioEngine.playEffect(clip, true);
              this._addTypeMap(exports.EffectMusic[effectID], id);
              return [ 4, function() {
                return new Promise(function(resolve, reject) {
                  setTimeout(resolve, 1e3 * time);
                });
              }() ];

             case 1:
              _a.sent();
              cc.audioEngine.stopEffect(id);
              return [ 2 ];
            }
          });
        });
      };
      MusicUtil.PlayerEffectContineMusic = function(id, isCover, isCD) {
        var _this = this;
        void 0 === isCover && (isCover = false);
        void 0 === isCD && (isCD = false);
        if (!this.CheckInit()) return;
        var instance = MusicUtil_1._instance;
        var effectArray = instance.effectContineArrays;
        var clip = [];
        var temp = null;
        for (var i in effectArray) if (effectArray[i].effectContineID == id) {
          clip = effectArray[i].effectContine;
          temp = effectArray[i];
          break;
        }
        if (0 == clip.length) {
          console.error("\u68c0\u67e5\u97f3\u6548\u8868\u8d44\u6e90", "---effectContineID:" + id + " ---");
          return;
        }
        if (isCD && this._effectCD) return;
        this._effectCD = true;
        this._instance.scheduleOnce(function() {
          _this._effectCD = false;
        }, this._effectCDTime);
        isCover && this.stopAllMusic();
        var m_id = cc.audioEngine.playEffect(clip[temp.index % clip.length], false);
        this._addTypeMap(exports.EffectContineMusic[id], m_id);
        temp.index++;
        return m_id;
      };
      MusicUtil.PlayerEffectContineMusicByIndex = function(id, index, isCover, isCD) {
        var _this = this;
        void 0 === isCover && (isCover = false);
        void 0 === isCD && (isCD = false);
        if (!this.CheckInit()) return;
        var instance = MusicUtil_1._instance;
        var effectArray = instance.effectContineArrays;
        var clip = [];
        var temp = null;
        for (var i in effectArray) if (effectArray[i].effectContineID == id) {
          clip = effectArray[i].effectContine;
          temp = effectArray[i];
          break;
        }
        if (0 == clip.length) {
          console.error("\u68c0\u67e5\u97f3\u6548\u8868\u8d44\u6e90", "---effectContineID:" + id + " ---index:" + index);
          return;
        }
        if (isCD && this._effectCD) return;
        this._effectCD = true;
        this._instance.scheduleOnce(function() {
          _this._effectCD = false;
        }, this._effectCDTime);
        isCover && this.stopAllMusic();
        var m_id = cc.audioEngine.playEffect(clip[index], false);
        this._addTypeMap(exports.EffectContineMusic[id], m_id);
        return m_id;
      };
      MusicUtil.StopEffectMusicByID = function(musicID) {
        "number" == typeof musicID ? cc.audioEngine.stopEffect(musicID) : console.error("\u65e0\u6548\u7684\u97f3\u6548id");
      };
      MusicUtil.StopAllMusicSameType = function(type) {
        var temp_typeIDs = this._effectTypeMap[type];
        if (temp_typeIDs) {
          for (var i = 0; i < temp_typeIDs.length; i++) cc.audioEngine.stopEffect(temp_typeIDs[i]);
          temp_typeIDs.length = 0;
        }
      };
      MusicUtil.CheckMusicMute = function() {
        if (!this.CheckInit()) return;
        MusicUtil_1.GameSetting.isBgMusic ? cc.audioEngine.setMusicVolume(MusicUtil_1.GameSetting.bgValue) : cc.audioEngine.setMusicVolume(0);
        MusicUtil_1.GameSetting.isEffectMusic ? cc.audioEngine.setEffectsVolume(MusicUtil_1.GameSetting.effectValue) : cc.audioEngine.setEffectsVolume(0);
      };
      MusicUtil.SetBgMusicSwitch = function(temp) {
        if (!this.CheckInit()) return;
        this.GameSetting.isBgMusic = temp;
        this.CheckMusicMute();
        this.SetGameSetting();
      };
      MusicUtil.SetEffectMusicSwitch = function(temp) {
        if (!this.CheckInit()) return;
        this.GameSetting.isEffectMusic = temp;
        this.CheckMusicMute();
        this.SetGameSetting();
      };
      MusicUtil.SetBgMusicValue = function(value, isLocalData) {
        void 0 === isLocalData && (isLocalData = true);
        if (!this.CheckInit()) return;
        cc.audioEngine.setMusicVolume(value);
        if (isLocalData) {
          this.GameSetting.bgValue = value;
          this.SetGameSetting();
        }
      };
      MusicUtil.SetEffectMusicValue = function(value, isLocalData) {
        void 0 === isLocalData && (isLocalData = true);
        if (!this.CheckInit()) return;
        cc.audioEngine.setEffectsVolume(value);
        if (isLocalData) {
          this.GameSetting.effectValue = value;
          this.SetGameSetting();
        }
      };
      MusicUtil.PauseAllMusic = function() {
        if (!this.CheckInit()) return;
        cc.audioEngine.setMusicVolume(0);
        cc.audioEngine.setEffectsVolume(0);
      };
      MusicUtil.ResumeAllMusic = function() {
        if (!this.CheckInit()) return;
        this.CheckMusicMute();
      };
      MusicUtil.PhoneShake = function(type) {
        if (false == this.GameSetting.isShake) return;
        switch (type) {
         case PhoneShakeType.Short:
          window["CAdGlobal"].vibrate(0);
          break;

         case PhoneShakeType.Long:
          window["CAdGlobal"].vibrate(1);
          break;

         default:
          console.error("------\u4e0d\u5b58\u5728\u6b64\u9707\u52a8\u7c7b\u578b--------");
        }
      };
      MusicUtil.stopAllMusic = function() {
        var count = 0;
        for (var i in this._effectTypeMap) {
          var temp = this._effectTypeMap[i];
          for (var t = 0; t < temp.length; t++) try {
            cc.audioEngine.stop(temp[t]);
          } catch (error) {}
          temp = [];
        }
      };
      MusicUtil.SetShakeSwitch = function(value) {
        this.GameSetting.isShake = value;
        this.SetGameSetting();
      };
      MusicUtil.SetGameSetting = function() {
        cc.sys.localStorage.setItem(GameSettingKey, JSON.stringify(this.GameSetting));
      };
      MusicUtil.GetGameSetting = function() {
        var Info = null;
        "" != cc.sys.localStorage.getItem(GameSettingKey) && cc.sys.localStorage.getItem(GameSettingKey) || this.SetGameSetting();
        Info = JSON.parse(cc.sys.localStorage.getItem(GameSettingKey));
        if (Info.bgValue) {
          Info.bgValue = BgDefaultValue;
          Info.effectValue = EffectDefaultValue;
        }
        return Info;
      };
      var MusicUtil_1;
      MusicUtil._instance = null;
      MusicUtil._effectTypeMap = {};
      MusicUtil.GameSetting = {
        isShake: true,
        isEffectMusic: true,
        isBgMusic: true,
        bgValue: .2,
        effectValue: 1
      };
      MusicUtil._effectCDTime = .1;
      MusicUtil._effectCD = false;
      __decorate([ property({
        type: [ BgMusicType ],
        displayName: "\u80cc\u666f\u97f3\u4e50\u6570\u7ec4"
      }) ], MusicUtil.prototype, "bgMusicArrays", void 0);
      __decorate([ property({
        displayName: "----------------------\u9694\u5f00----------------------------"
      }) ], MusicUtil.prototype, "x1", void 0);
      __decorate([ property({
        type: [ EffectMusicType ],
        displayName: "\u97f3\u6548\u6570\u7ec4"
      }) ], MusicUtil.prototype, "effectMusicArrays", void 0);
      __decorate([ property({
        displayName: "----------------------\u9694\u5f00----------------------------"
      }) ], MusicUtil.prototype, "x2", void 0);
      __decorate([ property({
        type: [ EffectContineType ],
        displayName: "\u97f3\u6548\u8868\u6570\u7ec4"
      }) ], MusicUtil.prototype, "effectContineArrays", void 0);
      MusicUtil = MusicUtil_1 = __decorate([ ccclass ], MusicUtil);
      return MusicUtil;
    }(cc.Component);
    exports.default = MusicUtil;
    window["MusicUtil"] = MusicUtil;
    cc._RF.pop();
  }, {} ],
  MysticBoxView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5ee89u923RFxImlOXx/+CBX", "MysticBoxView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SDKEnum_1 = require("../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../global/libs/ExportPageController");
    var Platform_1 = require("../../global/libs/Platform");
    var PopUp_1 = require("../../global/libs/PopUp");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var UIManager_1 = require("../../scripts/Core/UIManager");
    var GameData_1 = require("../../scripts/modules/data/GameData");
    var EnumDef_1 = require("../../scripts/modules/EnumDef");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MysticBoxView = function(_super) {
      __extends(MysticBoxView, _super);
      function MysticBoxView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.Progress = null;
        _this.GameTipsBtn = null;
        _this.MysticBoxBtn = null;
        _this.CloseVideoBtn = null;
        _this.closeBtn = null;
        _this.nodeGou = null;
        _this.BoxAni = null;
        _this._data = null;
        _this._stopProgress = false;
        _this._isBegin = false;
        _this._progressWidth = 528;
        _this._clickRate = .1;
        _this._autoRate = .006;
        _this.lookVideo = true;
        _this.luckState = 2;
        _this.isClick = null;
        return _this;
      }
      MysticBoxView.prototype.update = function(dt) {
        this.OnUpdate(dt);
      };
      MysticBoxView.prototype.init = function(data) {
        this.isClick = false;
        data && (this._data = data);
        this.OnOpen();
      };
      MysticBoxView.prototype.OnOpen = function() {
        this.scheduleOnce(function() {
          Platform_1.platform.hideBanner();
        });
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        var luckBagOpen = UmSdk_1.umSdk.getOnLineGameValue(7);
        missClickStatus && luckBagOpen;
        this.isClick = false;
        this._isBegin = true;
        this._stopProgress = false;
        this._switchBtnStatus(false);
        this.BoxAni.play("MysticBoxView_close");
        this.scheduleOnce(function() {
          ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.RightDrawerPage);
          ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.LeftDrawerPage);
        }, .5);
        this.luckState = UmSdk_1.umSdk.getOnLineGameValue(14);
        if (1 == this.luckState) {
          this.Progress.width = 528;
          this._onMysticBoxBtnClick();
        }
      };
      MysticBoxView.prototype.OnUpdate = function(deltaTime) {
        if (this._stopProgress || !this._isBegin) return;
        this.Progress.width -= this._progressWidth * this._autoRate;
        this.Progress.width <= 0 && (this.Progress.width = 0);
      };
      MysticBoxView.prototype.btn1Handle = function() {
        this._onMysticBoxBtnClick();
      };
      MysticBoxView.prototype.btn4Handle = function() {
        this._closeView();
      };
      MysticBoxView.prototype.setUi = function() {
        if (this.lookVideo) {
          this.closeBtn.active = false;
          this.nodeGou.active = true;
        } else {
          this.closeBtn.active = true;
          this.nodeGou.active = false;
        }
        1 == this.luckState && (this.closeBtn.active = false);
      };
      MysticBoxView.prototype.clickGouHandle = function() {
        this.lookVideo = !this.lookVideo;
        this.setUi();
      };
      MysticBoxView.prototype.clickOkHandle = function() {
        1 != this.luckState || this.lookVideo ? this.videoHandle() : this.btn4Handle();
      };
      MysticBoxView.prototype.videoHandle = function() {
        this._closeView();
      };
      MysticBoxView.prototype._onMysticBoxBtnClick = function() {
        var _this = this;
        if (this._stopProgress) return;
        if (!this.isClick) {
          this.isClick = true;
          this.toTimer();
        }
        this.unscheduleAllCallbacks();
        this.scheduleOnce(function() {
          _this.BoxAni.play();
          _this.BoxAni.on("finished", _this._onFinished, _this);
        }, 2.5);
        this.Progress.width += this._progressWidth * this._clickRate;
        if (this.Progress.width >= this._progressWidth) {
          this.Progress.width = this._progressWidth;
          this._stopProgress = true;
          this.unscheduleAllCallbacks();
          this.MysticBoxBtn.active = false;
          this.BoxAni.play();
          this.BoxAni.on("finished", this._onFinished, this);
        }
      };
      MysticBoxView.prototype._switchBtnStatus = function(status) {
        this.MysticBoxBtn.active = !status;
        this.GameTipsBtn.active = status;
      };
      MysticBoxView.prototype._onFinished = function() {
        var _this = this;
        Platform_1.platform.showBanner();
        GameData_1.GameData.UserData.Power++;
        GameData_1.GameData.UserData.SaveData();
        UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
        UIManager_1.UIManager.Notif(EnumDef_1.UIName.TipsView, EnumDef_1.UIInfo.ShowView, "\u4f53\u529b+1");
        this.BoxAni.off("finished", this._onFinished, this);
        this.node.stopAllActions();
        cc.tween(this.node).delay(1).call(function() {
          _this._closeView();
        }).start();
      };
      MysticBoxView.prototype._closeView = function() {
        PopUp_1.popup.hide();
        this.node.stopAllActions();
        this._isBegin = false;
        this._stopProgress = true;
        if (this._data) {
          this._data();
          this._data = null;
        }
      };
      MysticBoxView.prototype.toTimer = function() {
        var _this = this;
        var luckBagOpen = UmSdk_1.umSdk.getOnLineGameValue(7);
        console.log("\u6d4b\u8bd5\u8bef\u70b9", luckBagOpen);
        if (luckBagOpen) {
          console.log("\u5f00\u542f\u8bef\u70b9");
          cc.tween(this.node).delay(1).call(function() {
            Platform_1.platform.showBanner();
          }).delay(1).call(function() {
            Platform_1.platform.hideBanner();
            _this.MysticBoxBtn.active = false;
            _this.unscheduleAllCallbacks();
            _this.BoxAni.play();
            _this.BoxAni.on("finished", _this._onFinished, _this);
          }).start();
        }
      };
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "Progress", void 0);
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "GameTipsBtn", void 0);
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "MysticBoxBtn", void 0);
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "CloseVideoBtn", void 0);
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "closeBtn", void 0);
      __decorate([ property(cc.Node) ], MysticBoxView.prototype, "nodeGou", void 0);
      __decorate([ property(cc.Animation) ], MysticBoxView.prototype, "BoxAni", void 0);
      MysticBoxView = __decorate([ ccclass ], MysticBoxView);
      return MysticBoxView;
    }(cc.Component);
    exports.default = MysticBoxView;
    cc._RF.pop();
  }, {
    "../../export/scripts/SDKEnum": "SDKEnum",
    "../../global/libs/ExportPageController": "ExportPageController",
    "../../global/libs/Platform": "Platform",
    "../../global/libs/PopUp": "PopUp",
    "../../global/libs/UmSdk": "UmSdk",
    "../../scripts/Core/UIManager": "UIManager",
    "../../scripts/modules/EnumDef": "EnumDef",
    "../../scripts/modules/data/GameData": "GameData"
  } ],
  OldMoreGamePage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ce536NxnMFKZLsxCd8X2BVG", "OldMoreGamePage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Platform_1 = require("../../global/libs/Platform");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var ExportLayout_1 = require("./ExportLayout");
    var ExportPage_1 = require("./ExportPage");
    var SDKEnum_1 = require("./SDKEnum");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var MoreGamePage = function(_super) {
      __extends(MoreGamePage, _super);
      function MoreGamePage() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ExportPage = null;
        _this.exportLayout = null;
        _this.BackBtn = null;
        _this._gameIndex = 0;
        _this.returnPageNum = 0;
        return _this;
      }
      MoreGamePage.prototype.start = function() {
        this.BackBtn.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
      };
      MoreGamePage.prototype.OnPageShow = function() {
        var returnControl = UmSdk_1.umSdk.getOnLineGameValue(9);
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        var isOpen = false;
        if (missClickStatus && returnControl) {
          this.returnPageNum++;
          this.returnPageNum % returnControl == 0 && (isOpen = true);
        }
        1;
        cc.tween(this.node).delay(1).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).start();
      };
      MoreGamePage.prototype.OnPageHide = function() {
        this.node.stopAllActions();
        Platform_1.platform.hideBanner();
      };
      MoreGamePage.prototype._onTouchEnd = function() {
        this.ExportPage.getComponent(ExportPage_1.default).ClosePage(SDKEnum_1.PageType.OldMoreGame);
      };
      MoreGamePage.prototype._continueGame = function() {
        var num = Number(this.exportLayout.showNumber);
        if (!num) return;
        this._gameIndex = Math.floor(Math.random() * num);
        this.exportLayout.JumpToGame(this._gameIndex);
        this._gameIndex++;
      };
      __decorate([ property({
        type: cc.Node
      }) ], MoreGamePage.prototype, "ExportPage", void 0);
      __decorate([ property({
        type: ExportLayout_1.default
      }) ], MoreGamePage.prototype, "exportLayout", void 0);
      __decorate([ property({
        tooltip: "\u8fd4\u56de\u6309\u94ae",
        type: cc.Node
      }) ], MoreGamePage.prototype, "BackBtn", void 0);
      MoreGamePage = __decorate([ ccclass ], MoreGamePage);
      return MoreGamePage;
    }(cc.Component);
    exports.default = MoreGamePage;
    cc._RF.pop();
  }, {
    "../../global/libs/Platform": "Platform",
    "../../global/libs/UmSdk": "UmSdk",
    "./ExportLayout": "ExportLayout",
    "./ExportPage": "ExportPage",
    "./SDKEnum": "SDKEnum"
  } ],
  OldUsersPage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "10b50g/fsVEpIoqsdooqnGt", "OldUsersPage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var ExportPageController_1 = require("../../global/libs/ExportPageController");
    var Platform_1 = require("../../global/libs/Platform");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var ExportPage_1 = require("./ExportPage");
    var SDKEnum_1 = require("./SDKEnum");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var OldUsersPage = function(_super) {
      __extends(OldUsersPage, _super);
      function OldUsersPage() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.ExportPage = null;
        _this.BackBtn = null;
        _this.returnPageNum = 0;
        return _this;
      }
      OldUsersPage.prototype.start = function() {
        this.BackBtn.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
      };
      OldUsersPage.prototype.OnPageShow = function() {
        var returnControl = UmSdk_1.umSdk.getOnLineGameValue(9);
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        var isOpen = false;
        if (!missClickStatus || !returnControl) {
          Platform_1.platform.showBanner();
          this.BackBtn.getComponent(cc.Widget).bottom = -80;
          this.BackBtn.getComponent(cc.Widget).updateAlignment;
          return;
        }
        this.returnPageNum++;
        if (this.returnPageNum % returnControl == 0) {
          isOpen = true;
          this.scheduleOnce(function() {
            Platform_1.platform.hideBanner();
          });
        }
        cc.tween(this.node).delay(1).repeatForever(cc.tween().call(function() {
          Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.hideBanner();
        })).start();
      };
      OldUsersPage.prototype.OnPageHide = function() {
        ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.OldMoreGame, null, null);
        this.node.stopAllActions();
        Platform_1.platform.hideBanner();
      };
      OldUsersPage.prototype._onTouchEnd = function() {
        this.ExportPage.getComponent(ExportPage_1.default).ClosePage(SDKEnum_1.PageType.OldUsersPage);
      };
      __decorate([ property({
        type: cc.Node
      }) ], OldUsersPage.prototype, "ExportPage", void 0);
      __decorate([ property({
        tooltip: "\u8fd4\u56de\u6309\u94ae",
        type: cc.Node
      }) ], OldUsersPage.prototype, "BackBtn", void 0);
      OldUsersPage = __decorate([ ccclass ], OldUsersPage);
      return OldUsersPage;
    }(cc.Component);
    exports.default = OldUsersPage;
    cc._RF.pop();
  }, {
    "../../global/libs/ExportPageController": "ExportPageController",
    "../../global/libs/Platform": "Platform",
    "../../global/libs/UmSdk": "UmSdk",
    "./ExportPage": "ExportPage",
    "./SDKEnum": "SDKEnum"
  } ],
  PlatformConfig: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "96e43GzenZFwaih+NWEwHhe", "PlatformConfig");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.wxGameData = void 0;
    exports.wxGameData = {
      rewardVideoId: "adunit-f3d20e052f0263ab",
      bannerId: "adunit-2597ede0983ef465",
      insertVideoId: "",
      shareData: [ {
        title: "\u4f60\u597d",
        imageUrl: ""
      } ]
    };
    cc._RF.pop();
  }, {} ],
  Platform: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e44b9ouRUdO96rFxiva29eP", "Platform");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.platform = void 0;
    var WxPlatform_1 = require("./platform/WxPlatform");
    var PlatformConfig_1 = require("./platform/PlatformConfig");
    var WebPlatform_1 = require("./platform/WebPlatform");
    var UmSdk_1 = require("./UmSdk");
    var Platform = function() {
      function Platform() {
        this.isWxPlatform = false;
        this._tPlatform = null;
      }
      Platform.prototype.init = function() {
        if ("undefined" !== typeof window["wx"]) {
          this.isWxPlatform = true;
          this._tPlatform = new WxPlatform_1.default();
          this._tPlatform.initRewardVideo();
        } else this._tPlatform = new WebPlatform_1.default();
      };
      Platform.prototype.initWxPlatfrom = function() {
        this._tPlatform.init(PlatformConfig_1.wxGameData);
      };
      Platform.prototype.isWx = function() {
        return this.isWxPlatform;
      };
      Platform.prototype.vibrate = function(isLong) {
        void 0 === isLong && (isLong = false);
        this._tPlatform.vibrate ? this._tPlatform.vibrate(isLong) : console.warn("\u4e0d\u652f\u6301 vibrate");
      };
      Platform.prototype.showRewardVideo = function(cb) {
        this._tPlatform.showRewardVideo ? this._tPlatform.showRewardVideo(cb) : console.warn("\u4e0d\u652f\u6301 showRewardVideo()");
      };
      Platform.prototype.showInsertVideo = function(cb) {
        this._tPlatform.showInsertVideo ? this._tPlatform.showInsertVideo(cb) : console.warn("\u4e0d\u652f\u6301 showInsertVideo()");
      };
      Platform.prototype.showBanner = function() {
        this._tPlatform.showBanner ? this._tPlatform.showBanner() : console.warn("\u4e0d\u652f\u6301 showBanner()");
      };
      Platform.prototype.hideBanner = function() {
        this._tPlatform.hideBanner ? this._tPlatform.hideBanner() : console.warn("\u4e0d\u652f\u6301 hideBanner()");
      };
      Platform.prototype.shareVideo = function(cb) {
        this._tPlatform.shareVideo ? this._tPlatform.shareVideo(cb) : console.warn("\u4e0d\u652f\u6301 shareVideo()");
      };
      Platform.prototype.shareMessage = function(cb) {
        this._tPlatform.shareMessage ? this._tPlatform.shareMessage(cb) : console.warn("\u4e0d\u652f\u6301 shareMessage()");
      };
      Platform.prototype.startRecord = function(duration) {
        this._tPlatform.startRecord ? this._tPlatform.startRecord() : console.warn("\u4e0d\u652f\u6301 startRecord()");
      };
      Platform.prototype.showCustomAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this._tPlatform.showCustomAd) return;
        this._tPlatform.showCustomAd(ids);
      };
      Platform.prototype.hideCustomAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this._tPlatform.hideCustomAd) return;
        this._tPlatform.hideCustomAd(ids);
      };
      Platform.prototype.showGridAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this._tPlatform.showCustomAd) return;
        this._tPlatform.showGridAd(ids);
      };
      Platform.prototype.hideGridAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this._tPlatform.hideCustomAd) return;
        this._tPlatform.hideGridAd(ids);
      };
      Platform.prototype.stopRecord = function() {
        this._tPlatform.stopRecord ? this._tPlatform.stopRecord() : console.warn("\u4e0d\u652f\u6301 stopRecord()");
      };
      Platform.prototype.pauseRecord = function() {
        this._tPlatform.pauseRecord ? this._tPlatform.pauseRecord() : console.warn("\u4e0d\u652f\u6301 pauseRecord()");
      };
      Platform.prototype.resumeRecord = function() {
        this._tPlatform.resumeRecord ? this._tPlatform.resumeRecord() : console.warn("\u4e0d\u652f\u6301 resumeRecord()");
      };
      Platform.prototype.showMoreGame = function(style, appids, cb) {
        this._tPlatform.showMoreGame ? this._tPlatform.showMoreGame() : console.warn("\u4e0d\u652f\u6301 showMoreGame()");
      };
      Platform.prototype.hideMoreGame = function() {
        this._tPlatform.hideMoreGame ? this._tPlatform.hideMoreGame() : console.warn("\u4e0d\u652f\u6301 hideMoreGame()");
      };
      Platform.prototype.showLoading = function(data) {
        void 0 === data && (data = {
          title: " ",
          mask: true
        });
        this._tPlatform.showLoading ? this._tPlatform.showLoading() : console.warn("\u4e0d\u652f\u6301 showLoading()");
      };
      Platform.prototype.hideLoading = function() {
        this._tPlatform.hideLoading ? this._tPlatform.hideLoading() : console.warn("\u4e0d\u652f\u6301 hideLoading()");
      };
      Platform.prototype.onShow = function(cb) {
        this._tPlatform.onShow ? this._tPlatform.onShow(cb) : console.warn("\u4e0d\u652f\u6301 onShow()");
      };
      Platform.prototype.onHide = function(cb) {
        this._tPlatform.onHide ? this._tPlatform.onHide(cb) : console.warn("\u4e0d\u652f\u6301 onHide()");
      };
      Platform.prototype.startAccelerometer = function(cb) {
        this._tPlatform.startAccelerometer ? this._tPlatform.startAccelerometer(cb) : console.warn("\u4e0d\u652f\u6301 startAccelerometer()");
      };
      Platform.prototype.stopAccelerometer = function() {
        this._tPlatform.stopAccelerometer ? this._tPlatform.stopAccelerometer() : console.warn("\u4e0d\u652f\u6301 stopAccelerometer()");
      };
      Platform.prototype.report = function(key, data) {
        this._tPlatform.report ? this._tPlatform.report(key, data) : console.warn("\u4e0d\u652f\u6301 report()");
      };
      Platform.prototype.getSystemInfo = function() {
        if (this._tPlatform.getSystemInfo) return this._tPlatform.getSystemInfo();
        console.warn("\u4e0d\u652f\u6301 getSystemInfo()");
        return null;
      };
      Platform.prototype.umEvent = function(key) {
        if (!UmSdk_1.umSdk.UmSdk) return;
        console.log("\u4e8b\u4ef6\uff1a" + key);
        UmSdk_1.umSdk.UmSdk.event("event", key);
      };
      Platform.prototype.getLaunchOptions = function() {
        if (this._tPlatform.getLaunchOptions) return this._tPlatform.getLaunchOptions();
        console.warn("\u4e0d\u652f\u6301 getLaunchOptions()");
        return null;
      };
      return Platform;
    }();
    exports.platform = new Platform();
    cc._RF.pop();
  }, {
    "./UmSdk": "UmSdk",
    "./platform/PlatformConfig": "PlatformConfig",
    "./platform/WebPlatform": "WebPlatform",
    "./platform/WxPlatform": "WxPlatform"
  } ],
  PolygonMask: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fa5b14lkKZJF5OJv8l5AFbX", "PolygonMask");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var gfx = cc.gfx;
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property, executeInEditMode = _a.executeInEditMode, requireComponent = _a.requireComponent;
    var DEFAULT_VERTEXES = [ cc.v2(-50, -50), cc.v2(50, -50), cc.v2(50, 50), cc.v2(-50, 50) ];
    var PolygonMask = function(_super) {
      __extends(PolygonMask, _super);
      function PolygonMask() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this._editing = false;
        _this._offset = cc.v2(0, 0);
        _this._spriteFrame = null;
        _this._vertexes = DEFAULT_VERTEXES;
        _this._meshCache = null;
        _this.renderer = null;
        _this.mesh = null;
        _this.texture = null;
        return _this;
      }
      Object.defineProperty(PolygonMask.prototype, "editing", {
        get: function() {
          return this._editing;
        },
        set: function(value) {
          this._editing = value;
          this._applyVertexes();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(PolygonMask.prototype, "offset", {
        get: function() {
          return this._offset;
        },
        set: function(value) {
          this._offset = value;
          this.editing && this._applyVertexes();
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(PolygonMask.prototype, "spriteFrame", {
        get: function() {
          return this._spriteFrame;
        },
        set: function(value) {
          this._spriteFrame = value;
          false;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(PolygonMask.prototype, "vertexes", {
        get: function() {
          return this._vertexes;
        },
        set: function(value) {
          this._vertexes = value;
          this._updateMesh();
          this.editing && this._applyVertexes();
        },
        enumerable: false,
        configurable: true
      });
      PolygonMask.prototype.start = function() {
        this._meshCache = {};
        this._updateMesh();
        var renderer = this.node.getComponent(cc.MeshRenderer);
        renderer.mesh = null;
        this.renderer = renderer;
        var builtinMaterial = cc.MaterialVariant.createWithBuiltin("unlit", this).material;
        renderer.setMaterial(0, builtinMaterial);
        this._applySpriteFrame();
        this._applyVertexes();
      };
      PolygonMask.prototype._updateMesh = function() {
        var mesh = this._meshCache[this.vertexes.length];
        if (!mesh) {
          mesh = new cc.Mesh();
          mesh.init(new gfx.VertexFormat([ {
            name: gfx.ATTR_POSITION,
            type: gfx.ATTR_TYPE_FLOAT32,
            num: 2
          }, {
            name: gfx.ATTR_UV0,
            type: gfx.ATTR_TYPE_FLOAT32,
            num: 2
          } ]), this.vertexes.length, true);
          this._meshCache[this.vertexes.length] = mesh;
        }
        this.mesh = mesh;
      };
      PolygonMask.prototype._applyVertexes = function() {
        var _this = this;
        var mesh = this.mesh;
        var ves = this.vertexes.map(function(i) {
          return i.add(_this.offset);
        });
        mesh.setVertices(gfx.ATTR_POSITION, ves);
        if (this.texture) {
          var uvs = [];
          for (var _i = 0, ves_1 = ves; _i < ves_1.length; _i++) {
            var pt = ves_1[_i];
            var vx = (pt.x + this.texture.width / 2) / this.texture.width;
            var vy = 1 - (pt.y + this.texture.height / 2) / this.texture.height;
            uvs.push(cc.v2(vx, vy));
          }
          mesh.setVertices(gfx.ATTR_UV0, uvs);
        }
        if (this.vertexes.length >= 3) {
          var ids = [];
          var vertexes = [].concat(ves);
          var index = 0, rootIndex = -1;
          while (vertexes.length > 3) {
            var p1 = vertexes[index % vertexes.length];
            var p2 = vertexes[(index + 1) % vertexes.length];
            var p3 = vertexes[(index + 2) % vertexes.length];
            var v1 = p2.sub(p1);
            var v2 = p3.sub(p2);
            if (v1.cross(v2) >= 0) {
              var isIn = false;
              for (var _a = 0, vertexes_1 = vertexes; _a < vertexes_1.length; _a++) {
                var p_t = vertexes_1[_a];
                if (p_t !== p1 && p_t !== p2 && p_t !== p3 && this._testInTriangle(p_t, p1, p2, p3)) {
                  isIn = true;
                  break;
                }
              }
              if (isIn) {
                index = (index + 1) % vertexes.length;
                if (index === rootIndex) {
                  cc.log("\u5faa\u73af\u4e00\u5708\u672a\u53d1\u73b0");
                  break;
                }
              } else {
                ids = ids.concat([ ves.indexOf(p1), ves.indexOf(p2), ves.indexOf(p3) ]);
                vertexes.splice(vertexes.indexOf(p2), 1);
                rootIndex = index;
              }
            } else {
              index = (index + 1) % vertexes.length;
              if (index === rootIndex) {
                cc.log("\u5faa\u73af\u4e00\u5708\u672a\u53d1\u73b0");
                break;
              }
            }
          }
          ids = ids.concat(vertexes.map(function(v) {
            return ves.indexOf(v);
          }));
          mesh.setIndices(ids);
          this.renderer.mesh != mesh && (this.renderer.mesh = mesh);
        }
      };
      PolygonMask.prototype._testInTriangle = function(point, triA, triB, triC) {
        var AB = triB.sub(triA), AC = triC.sub(triA), BC = triC.sub(triB), AD = point.sub(triA), BD = point.sub(triB);
        return AB.cross(AC) >= 0 !== AB.cross(AD) < 0 && AB.cross(AC) >= 0 !== AC.cross(AD) >= 0 && BC.cross(AB) > 0 !== BC.cross(BD) >= 0;
      };
      PolygonMask.prototype._applySpriteFrame = function() {
        if (this.spriteFrame) {
          var renderer = this.renderer;
          var material = renderer.getMaterial(0);
          var texture = this.spriteFrame.getTexture();
          material.define("USE_DIFFUSE_TEXTURE", true);
          material.setProperty("diffuseTexture", texture);
          this.texture = texture;
        }
      };
      __decorate([ property({
        serializable: false,
        readonly: true
      }) ], PolygonMask.prototype, "_editing", void 0);
      __decorate([ property ], PolygonMask.prototype, "editing", null);
      __decorate([ property ], PolygonMask.prototype, "_offset", void 0);
      __decorate([ property ], PolygonMask.prototype, "offset", null);
      __decorate([ property({
        type: cc.SpriteFrame
      }) ], PolygonMask.prototype, "_spriteFrame", void 0);
      __decorate([ property({
        type: cc.SpriteFrame
      }) ], PolygonMask.prototype, "spriteFrame", null);
      __decorate([ property({
        type: cc.Vec2
      }) ], PolygonMask.prototype, "_vertexes", void 0);
      __decorate([ property({
        type: cc.Vec2
      }) ], PolygonMask.prototype, "vertexes", null);
      PolygonMask = __decorate([ ccclass, executeInEditMode, requireComponent(cc.MeshRenderer) ], PolygonMask);
      return PolygonMask;
    }(cc.Component);
    exports.default = PolygonMask;
    cc._RF.pop();
  }, {} ],
  PopUp: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "eded3gLB7dHeIZJG9FYUGzA", "PopUp");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.popup = exports.PopUp = void 0;
    var PopUp = function() {
      function PopUp() {
        this._isInit = false;
        this.containerNode = null;
        this.maskNode = null;
        this.nodes = [];
        this.names = [];
        this.showBoxCount = 0;
        this._completeCall = null;
        this._onHideCall = null;
      }
      PopUp.prototype.init = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            if (this._isInit) return [ 2 ];
            this._isInit = true;
            this._initNode();
            return [ 2 ];
          });
        });
      };
      PopUp.prototype.show = function(name, data, completeCall, onHideCall) {
        void 0 === completeCall && (completeCall = null);
        void 0 === onHideCall && (onHideCall = null);
        return __awaiter(this, void 0, Promise, function() {
          var node, comp;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (this.names.some(function(nName) {
                return nName === name;
              })) return [ 2 ];
              this.names.push(name);
              return [ 4, this.getNode(name) ];

             case 1:
              node = _a.sent();
              comp = node._components[0];
              comp && comp.init && comp.init(data);
              this.containerNode.addChild(node);
              this.checkMaskzIndex();
              this._completeCall = completeCall;
              this._onHideCall = onHideCall;
              this._completeCall && this._completeCall();
              return [ 2 ];
            }
          });
        });
      };
      PopUp.prototype.hide = function(cb) {
        if (!this.names.length) return;
        var name = this.names.pop();
        this.containerNode.children.forEach(function(node) {
          node.name === name && node.removeFromParent();
        });
        this.checkMaskzIndex();
        this.maskNode.active = this.names.length >= 1;
        this._onHideCall && this._onHideCall();
      };
      PopUp.prototype.hideAll = function(cb) {
        var _this = this;
        if (!this.names.length) return;
        this.names = [];
        this.containerNode.children.forEach(function(node) {
          if (_this.maskNode.name === node.name) return;
          node.removeFromParent();
        });
        this.maskNode.active = false;
        cb && cb();
      };
      PopUp.prototype.checkMaskzIndex = function() {
        var child = this.containerNode.children;
        this.names.forEach(function(name, index) {
          child.find(function(node) {
            return node.name === name;
          }).zIndex = 10 * (index + 1);
        });
        child.find(function(node) {
          return "Mask" === node.name;
        }).zIndex = 9 * this.names.length;
      };
      PopUp.prototype.getNode = function(name) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, this.nodes.find(function(node) {
                return node.name === name;
              }) ];

             case 1:
              return [ 2, _a.sent() || this.createNode(name) ];
            }
          });
        });
      };
      PopUp.prototype.createNode = function(name) {
        var _this = this;
        return new Promise(function(resolve, reject) {
          cc.loader.loadRes("prefab/popup/" + name, cc.Prefab, function(err, res) {
            if (err) {
              reject(null);
              return;
            }
            var node = cc.instantiate(res);
            _this.nodes.push(node);
            resolve(node);
          });
        });
      };
      PopUp.prototype._initNode = function() {
        var node = new cc.Node("PopUp");
        var parent = cc.find("Canvas").parent;
        parent.addChild(node);
        this.containerNode = node;
        cc.game.addPersistRootNode(node);
        node.setContentSize(this.getScreenSize().x, this.getScreenSize().y);
        var widget = node.addComponent(cc.Widget);
        widget.isAlignVerticalCenter = true;
        widget.isAlignHorizontalCenter = true;
        var maskNode = new cc.Node("Mask");
        maskNode.setContentSize(750, 1650);
        maskNode.color = new cc.Color(0, 0, 0);
        maskNode.opacity = 100;
        maskNode.active = false;
        node.addChild(maskNode);
        this.maskNode = maskNode;
        var sprite = maskNode.addComponent(cc.Sprite);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        cc.loader.loadRes("texture/common/singleColor", cc.SpriteFrame, function(err, res) {
          if (err) return;
          sprite.spriteFrame = res;
        });
        maskNode.addComponent(cc.Button);
      };
      PopUp.prototype.getScreenSize = function() {
        return new cc.Vec2(cc.winSize.width, cc.winSize.height);
      };
      PopUp.MysticBoxView = "MysticBoxView";
      PopUp.PopupGameEndPage = "popupGameEndPage";
      return PopUp;
    }();
    exports.PopUp = PopUp;
    exports.popup = new PopUp();
    cc._RF.pop();
  }, {} ],
  PopupGameEndPage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9812dZfXiNEUY8pSfLMcYfI", "PopupGameEndPage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SDKEnum_1 = require("../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../global/libs/ExportPageController");
    var Platform_1 = require("../../global/libs/Platform");
    var PopUp_1 = require("../../global/libs/PopUp");
    var Transition_1 = require("../../global/libs/Transition");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var User_1 = require("../../global/libs/User");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var NewClass = function(_super) {
      __extends(NewClass, _super);
      function NewClass() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeOption = null;
        _this.labelTitle = null;
        _this.nodeColorful = null;
        _this.liziList = [];
        _this.nodeBtn = null;
        _this.returnNum = 0;
        _this.selfData = {
          type: 1,
          levelIndex: 0,
          explainArray: [],
          achievableTargetArray: []
        };
        return _this;
      }
      NewClass.prototype.setData = function(data) {
        for (var key in data) data.hasOwnProperty(key) && this.selfData.hasOwnProperty(key) && (this.selfData[key] = data[key]);
        return this;
      };
      NewClass.prototype.init = function(data) {
        data && this.setData(data);
        if (1 === this.selfData.type) {
          this.nodeBtn = this.nodeOption.children[0];
          this.nodeColorful.active = true;
          this.intNext();
          this.initWinResult();
        } else {
          this.nodeBtn = this.nodeOption.children[1];
          this.nodeColorful.active = false;
          this.setUi();
          this.opAction(this.node.getChildByName("Node_Content"));
        }
        this.nodeBtn.active = true;
      };
      NewClass.prototype.intNext = function() {
        var _this = this;
        this.liziList.forEach(function(element) {
          element.resetSystem();
        });
        this.node.getChildByName("BG").active = true;
        cc.tween(this.node).call(function() {
          _this.node.getChildByName("Node_Content").active = false;
          _this.node.getChildByName("BG").opacity = 0;
        }).delay(1.5).call(function() {
          _this.node.getChildByName("BG").opacity = 180;
          _this.setUi();
          _this.opAction(_this.node.getChildByName("Node_Content"));
        }).start();
      };
      NewClass.prototype.opAction = function(actionNode) {
        actionNode.active = true;
        actionNode.scale = 0;
        cc.tween(actionNode).to(.3, {
          scale: 1
        }, {
          easing: "quadIn"
        }).delay(.1).call(function() {
          ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.FailPage, null, null);
        }).start();
      };
      NewClass.prototype.setUi = function() {
        if (!this.selfData.type) return;
        this.nodeOption.children[0].active = 1 == this.selfData.type;
        this.nodeOption.children[1].active = 2 == this.selfData.type;
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        missClickStatus ? this.judgeAction() : this.restBtn();
      };
      NewClass.prototype.restBtn = function() {
        var _this = this;
        this.nodeBtn.opacity = 1;
        this.scheduleOnce(function() {
          _this.nodeBtn.y = -377;
          _this.nodeBtn.opacity = 255;
        }, .5);
        Platform_1.platform.showBanner();
      };
      NewClass.prototype.judgeAction = function() {
        var MissClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        MissClickStatus ? this.openAction() : this.restBtn();
      };
      NewClass.prototype.openAction = function() {
        var _this = this;
        console.log("PopUpGameEndPage \u8bef\u70b9");
        var bannerErrorDelay = UmSdk_1.umSdk.getOnLineGameValue(2);
        var bannerDelayTime = UmSdk_1.umSdk.getOnLineGameValue(3);
        this.nodeBtn.active = true;
        cc.tween(this.nodeBtn).delay(.5).call(function() {
          _this.nodeBtn.getComponent(cc.Widget).enabled = true;
          _this.nodeBtn.getComponent(cc.Widget).bottom = 0;
          _this.nodeBtn.getComponent(cc.Widget).updateAlignment();
        }).delay(bannerDelayTime).call(function() {
          Platform_1.platform.showBanner();
          _this.scheduleOnce(function() {
            _this.nodeBtn.y = -377;
            _this.nodeBtn.opacity = 255;
          }, bannerErrorDelay + 2);
        }).start();
      };
      NewClass.prototype.initWinResult = function() {
        return;
        var levelData;
        var level;
      };
      NewClass.prototype.nextLevelButtonEvent = function(event, data) {
        ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.FailPage);
        Platform_1.platform.hideBanner();
        1;
        ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.MoreGamePage, null, function() {
          Transition_1.transition.loadScene("LoadingStart", function() {
            var luckBagOpen = UmSdk_1.umSdk.getOnLineGameValue(7);
            luckBagOpen && PopUp_1.popup.show(PopUp_1.PopUp.MysticBoxView, null, function() {
              ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.CoupletPage);
            }, function() {
              ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.CoupletPage, null, null);
            });
          });
        });
        PopUp_1.popup.hide();
      };
      NewClass.prototype.showMysticBoxView = function() {
        this.returnNum++;
        if (this.returnNum) {
          var luckBagOpen = UmSdk_1.umSdk.getOnLineGameValue(7);
          var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
          missClickStatus && luckBagOpen && this.returnNum % luckBagOpen == 0 && PopUp_1.popup.show(PopUp_1.PopUp.MysticBoxView, null, function() {
            ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.OldUsersPage);
            ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.CoupletPage);
          }, function() {
            ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.OldUsersPage, null, null);
            ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.CoupletPage, null, null);
          });
        }
      };
      __decorate([ property({
        type: cc.Node,
        tooltip: "\u9875\u9762\u4e0b\u65b9\u6309\u94ae\u7236\u8282\u70b9"
      }) ], NewClass.prototype, "nodeOption", void 0);
      __decorate([ property({
        type: cc.Label,
        tooltip: "\u6807\u9898"
      }) ], NewClass.prototype, "labelTitle", void 0);
      __decorate([ property({
        type: cc.Node,
        tooltip: "\u5f69\u82b1"
      }) ], NewClass.prototype, "nodeColorful", void 0);
      __decorate([ property({
        type: cc.ParticleSystem,
        tooltip: "\u5f69\u82b1"
      }) ], NewClass.prototype, "liziList", void 0);
      NewClass = __decorate([ ccclass ], NewClass);
      return NewClass;
    }(cc.Component);
    exports.default = NewClass;
    cc._RF.pop();
  }, {
    "../../export/scripts/SDKEnum": "SDKEnum",
    "../../global/libs/ExportPageController": "ExportPageController",
    "../../global/libs/Platform": "Platform",
    "../../global/libs/PopUp": "PopUp",
    "../../global/libs/Transition": "Transition",
    "../../global/libs/UmSdk": "UmSdk",
    "../../global/libs/User": "User"
  } ],
  ResManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "e8d9bYzzIlM65typzjty8UD", "ResManager");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.ResManager = void 0;
    var EnumDef_1 = require("../modules/EnumDef");
    var Util_1 = require("./Util");
    var UIManager_1 = require("./UIManager");
    var GameData_1 = require("../modules/data/GameData");
    var Pool = function() {
      function Pool(name) {
        this.Pool = [];
        this.Parent = new cc.Node(name);
        this.Parent.parent = cc.find("Canvas");
        this.Parent.active = false;
      }
      Pool.prototype.put = function(node) {
        node.parent = this.Parent;
        this.Pool.push(node);
      };
      Pool.prototype.get = function() {
        var node = this.Pool.shift();
        null == node && (node = cc.instantiate(this.BaseNode));
        return node;
      };
      return Pool;
    }();
    var _ResManager = function() {
      function _ResManager() {
        this._loadTotal = 0;
        this._curLoadCount = 0;
        this._AllPool = null;
        this._AllData = {};
        this._PreLoadEffect = [];
        this._PreLoadModel = [];
        this._PreLoadUI = [ EnumDef_1.UIName.GameView ];
        this._AllPool = [];
        this._AllPool[EnumDef_1.AssetBundleEnum.effect] = {};
        this._AllPool[EnumDef_1.AssetBundleEnum.model] = {};
        this._AllPool[EnumDef_1.AssetBundleEnum.mission] = [ "1" ];
        this._AllPool[EnumDef_1.AssetBundleEnum.ui] = [ EnumDef_1.UIName.GameView, EnumDef_1.UIName.BeginView, EnumDef_1.UIName.WinView, EnumDef_1.UIName.Win2View, EnumDef_1.UIName.FailView, EnumDef_1.UIName.Fail2View ];
      }
      Object.defineProperty(_ResManager.prototype, "AllData", {
        get: function() {
          return this._AllData;
        },
        enumerable: false,
        configurable: true
      });
      Object.defineProperty(_ResManager.prototype, "curLoadCount", {
        get: function() {
          return this._curLoadCount;
        },
        set: function(num) {
          this._curLoadCount = num;
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameLoading, EnumDef_1.UIInfo.SetProgress, this._curLoadCount / this._loadTotal);
        },
        enumerable: false,
        configurable: true
      });
      _ResManager.prototype.BeginLoad = function() {
        return __awaiter(this, void 0, Promise, function() {
          var AllConfigType, x, res, x, res, x, res, x, res;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              console.log("BeginLoad");
              AllConfigType = Object.keys(EnumDef_1.ConfigType);
              this._loadTotal = AllConfigType.length + this._PreLoadEffect.length + this._PreLoadModel.length + this._PreLoadUI.length;
              x = 0;
              _a.label = 1;

             case 1:
              if (!(x < AllConfigType.length)) return [ 3, 4 ];
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.config, EnumDef_1.ConfigType[AllConfigType[x]]) ];

             case 2:
              res = _a.sent();
              GameData_1.GameData.ParseData(EnumDef_1.ConfigType[AllConfigType[x]], res.json);
              this.curLoadCount++;
              _a.label = 3;

             case 3:
              x++;
              return [ 3, 1 ];

             case 4:
              console.log("Config PreLoad Complete");
              x = 0;
              _a.label = 5;

             case 5:
              if (!(x < this._PreLoadEffect.length)) return [ 3, 8 ];
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.effect, this._PreLoadEffect[x]) ];

             case 6:
              res = _a.sent();
              exports.ResManager.Put(EnumDef_1.AssetBundleEnum.effect, cc.instantiate(res));
              this.curLoadCount++;
              _a.label = 7;

             case 7:
              x++;
              return [ 3, 5 ];

             case 8:
              console.log("Effect PreLoad Complete");
              x = 0;
              _a.label = 9;

             case 9:
              if (!(x < this._PreLoadModel.length)) return [ 3, 12 ];
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, this._PreLoadModel[x]) ];

             case 10:
              res = _a.sent();
              exports.ResManager.Put(EnumDef_1.AssetBundleEnum.model, cc.instantiate(res));
              this.curLoadCount++;
              _a.label = 11;

             case 11:
              x++;
              return [ 3, 9 ];

             case 12:
              console.log("Model PreLoad Complete");
              x = 0;
              _a.label = 13;

             case 13:
              if (!(x < this._PreLoadUI.length)) return [ 3, 16 ];
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.ui, EnumDef_1.UIName[this._PreLoadUI[x]]) ];

             case 14:
              res = _a.sent();
              exports.ResManager.Put(EnumDef_1.AssetBundleEnum.ui, cc.instantiate(res));
              this.curLoadCount++;
              _a.label = 15;

             case 15:
              x++;
              return [ 3, 13 ];

             case 16:
              return [ 4, Util_1.Util.Res.LoadAssetBundle(EnumDef_1.AssetBundleEnum.mission) ];

             case 17:
              _a.sent();
              return [ 2 ];
            }
          });
        });
      };
      _ResManager.prototype.Put = function(cacheType, target) {
        null == this._AllPool[cacheType][target.name] && (this._AllPool[cacheType][target.name] = new Pool(target.name));
        var cacheItem = this._AllPool[cacheType][target.name];
        cacheItem.put(target);
      };
      _ResManager.prototype.GetCacheResource = function(cacheType, resName) {
        var cacheItem = this._AllPool[cacheType][resName];
        if (null != cacheItem) return cacheItem.get();
        return null;
      };
      return _ResManager;
    }();
    exports.ResManager = new _ResManager();
    cc._RF.pop();
  }, {
    "../modules/EnumDef": "EnumDef",
    "../modules/data/GameData": "GameData",
    "./UIManager": "UIManager",
    "./Util": "Util"
  } ],
  ReturnPage: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "cfc9588qDtHuZFSwwPBSwbo", "ReturnPage");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var Platform_1 = require("../../global/libs/Platform");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var ExportLayout_1 = require("./ExportLayout");
    var ExportPage_1 = require("./ExportPage");
    var SDKEnum_1 = require("./SDKEnum");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var ReturnPage = function(_super) {
      __extends(ReturnPage, _super);
      function ReturnPage() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.exportLayout = null;
        _this.BackBtn = null;
        _this.ContinueBtn = null;
        _this._gameIndex = 0;
        _this._timer = 0;
        _this.returnPageNum = 0;
        return _this;
      }
      ReturnPage.prototype.start = function() {
        this.BackBtn.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.ContinueBtn.on(cc.Node.EventType.TOUCH_END, this._continueGame, this);
      };
      ReturnPage.prototype.OnPageShow = function() {
        var _this = this;
        this.unscheduleAllCallbacks();
        this.scheduleOnce(this._continueGame, 2.5);
        this.BackBtn.active = true;
        var returnControl = UmSdk_1.umSdk.getOnLineGameValue(9);
        var missClickStatus = UmSdk_1.umSdk.getOnLineGameValue(11);
        var isOpen = false;
        if (missClickStatus && returnControl) {
          this.returnPageNum++;
          this.returnPageNum % returnControl == 0 && (isOpen = true);
        }
        cc.tween(this.node).call(function() {
          isOpen && (_this.BackBtn.active = true);
        }).delay(1).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          _this.BackBtn.active = true;
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          Platform_1.platform.hideBanner();
        }).delay(.5).call(function() {
          _this.scheduleOnce(function() {
            _this.BackBtn.active = true;
          }, 0);
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.showBanner();
        }).delay(1.5).call(function() {
          isOpen && Platform_1.platform.hideBanner();
        }).start();
      };
      ReturnPage.prototype.OnPageHide = function() {
        this.node.stopAllActions();
        Platform_1.platform.hideBanner();
      };
      ReturnPage.prototype._onTouchEnd = function() {
        this.node.parent.getComponent(ExportPage_1.default).ClosePage(SDKEnum_1.PageType.ReturnPage);
      };
      ReturnPage.prototype._continueGame = function() {
        this._gameIndex >= this.exportLayout.BoxList.length && (this._gameIndex = 0);
        this.exportLayout.JumpToGame(this._gameIndex);
        this._gameIndex++;
      };
      __decorate([ property({
        type: ExportLayout_1.default
      }) ], ReturnPage.prototype, "exportLayout", void 0);
      __decorate([ property({
        tooltip: "\u8fd4\u56de\u6309\u94ae",
        type: cc.Node
      }) ], ReturnPage.prototype, "BackBtn", void 0);
      __decorate([ property({
        tooltip: "\u7ee7\u7eed\u6e38\u620f\u6309\u94ae",
        type: cc.Node
      }) ], ReturnPage.prototype, "ContinueBtn", void 0);
      ReturnPage = __decorate([ ccclass ], ReturnPage);
      return ReturnPage;
    }(cc.Component);
    exports.default = ReturnPage;
    cc._RF.pop();
  }, {
    "../../global/libs/Platform": "Platform",
    "../../global/libs/UmSdk": "UmSdk",
    "./ExportLayout": "ExportLayout",
    "./ExportPage": "ExportPage",
    "./SDKEnum": "SDKEnum"
  } ],
  RoleBad: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "25fe75iq85ImYLYOYNEDlMk", "RoleBad");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleBad = function(_super) {
      __extends(RoleBad, _super);
      function RoleBad() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      RoleBad.prototype.start = function() {
        this.Ani.playAnimation("\u5f85\u673a", 0);
      };
      RoleBad.prototype.CheckSuccess = function() {
        return this.Liquid != BasePipe_1.Liquid.Health && this.Liquid != BasePipe_1.Liquid.Empty;
      };
      RoleBad.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.Liquid = BasePipe_1.Liquid.Health;
          this.ChangeAni();
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.DeadAni("\u6df9\u6b7b");
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.DeadAni("\u51bb\u6b7b");
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.DeadAni("\u6bd2\u6b7b");
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.DeadAni("\u70e7\u6b7b");
        }
      };
      RoleBad = __decorate([ ccclass ], RoleBad);
      return RoleBad;
    }(BaseRole_1.default);
    exports.default = RoleBad;
    cc._RF.pop();
  }, {
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoleDevil: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4bd9cn676hGyJ5JmKpOI0i9", "RoleDevil");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleDevil = function(_super) {
      __extends(RoleDevil, _super);
      function RoleDevil() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      RoleDevil.prototype.start = function() {
        this.Ani.playAnimation("\u5f85\u673a", 0);
      };
      RoleDevil.prototype.CheckSuccess = function() {
        return this.Liquid != BasePipe_1.Liquid.Health && this.Liquid != BasePipe_1.Liquid.Empty && this.Liquid != BasePipe_1.Liquid.Fire;
      };
      RoleDevil.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.Liquid = BasePipe_1.Liquid.Health;
          this.ChangeAni();
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.DeadAni("\u6df9\u6b7b");
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.DeadAni("\u51bb\u6b7b");
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.DeadAni("\u6bd2\u6b7b");
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.ChangeAni();
        }
      };
      RoleDevil = __decorate([ ccclass ], RoleDevil);
      return RoleDevil;
    }(BaseRole_1.default);
    exports.default = RoleDevil;
    cc._RF.pop();
  }, {
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoleFish: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "73f42udoJFOvYE8/uU51nLI", "RoleFish");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UIManager_1 = require("../../Core/UIManager");
    var EnumDef_1 = require("../EnumDef");
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleFish = function(_super) {
      __extends(RoleFish, _super);
      function RoleFish() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      RoleFish.prototype.start = function() {
        this.Ani.playAnimation("\u5f85\u673a", 0);
      };
      RoleFish.prototype.CheckSuccess = function() {
        return this.Liquid == BasePipe_1.Liquid.Health || this.Liquid == BasePipe_1.Liquid.Water;
      };
      RoleFish.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        var _this = this;
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.playAni = true;
          this.Liquid = BasePipe_1.Liquid.Health;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u8df3\u821e", 3);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              _this.Ani.playAnimation("\u8df3\u821e", 0);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.BoomTank, _this);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckWin);
            });
          });
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u8df3\u821e", 3);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              _this.Ani.playAnimation("\u8df3\u821e", 0);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.BoomTank, _this);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckWin);
            });
          });
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u5f85\u673a", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u5f85\u673a", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u5f85\u673a", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
        }
      };
      RoleFish = __decorate([ ccclass ], RoleFish);
      return RoleFish;
    }(BaseRole_1.default);
    exports.default = RoleFish;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoleHero: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c47123hVOZAy64n99AAkwN+", "RoleHero");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleHero = function(_super) {
      __extends(RoleHero, _super);
      function RoleHero() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.boolNormal = false;
        _this.heroLight = void 0;
        return _this;
      }
      RoleHero.prototype.start = function() {
        "\u7537\u4e3b" != this.node.name && (this.boolNormal = false);
        this.heroLight.active = false;
        this.boolNormal ? this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 0) : this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5bb3\u6015", 0);
      };
      RoleHero.prototype.CheckSuccess = function() {
        return this.boolNormal ? this.Liquid == BasePipe_1.Liquid.Health || this.Liquid == BasePipe_1.Liquid.Empty : this.Liquid == BasePipe_1.Liquid.Health;
      };
      RoleHero.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        var _this = this;
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.Liquid = BasePipe_1.Liquid.Health;
          this.playAni = true;
          this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.heroLight.active = true;
            _this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u8d85\u4eba\u53d8\u8eab", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.BoomTank, _this);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckWin);
            });
          });
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.playAni = true;
          this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u6df9\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.playAni = true;
          this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u51bb\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.playAni = true;
          this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u6bd2\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.playAni = true;
          this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation(GameData_1.GameData.getSuitName() + "\u70e7\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
        }
        otherCollider.tag == BasePipe_1.Liquid.Health;
      };
      __decorate([ property() ], RoleHero.prototype, "boolNormal", void 0);
      __decorate([ property(cc.Node) ], RoleHero.prototype, "heroLight", void 0);
      RoleHero = __decorate([ ccclass ], RoleHero);
      return RoleHero;
    }(BaseRole_1.default);
    exports.default = RoleHero;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoleSnake: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9a7564NrFFFFbLLNEr34XY+", "RoleSnake");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleSnake = function(_super) {
      __extends(RoleSnake, _super);
      function RoleSnake() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      RoleSnake.prototype.start = function() {
        this.Ani.playAnimation("\u5f85\u673a", 0);
      };
      RoleSnake.prototype.CheckSuccess = function() {
        return this.Liquid != BasePipe_1.Liquid.Health && this.Liquid != BasePipe_1.Liquid.Empty && this.Liquid != BasePipe_1.Liquid.Poison;
      };
      RoleSnake.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.Liquid = BasePipe_1.Liquid.Health;
          this.ChangeAni();
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.DeadAni("\u6df9\u6b7b");
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.DeadAni("\u51bb\u6b7b");
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.ChangeAni();
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.DeadAni("\u70e7\u6b7b");
        }
      };
      RoleSnake = __decorate([ ccclass ], RoleSnake);
      return RoleSnake;
    }(BaseRole_1.default);
    exports.default = RoleSnake;
    cc._RF.pop();
  }, {
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoleWife: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "56e45/0IxJFqKoiOBo3hgnz", "RoleWife");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UIManager_1 = require("../../Core/UIManager");
    var EnumDef_1 = require("../EnumDef");
    var BasePipe_1 = require("./BasePipe");
    var BaseRole_1 = require("./BaseRole");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var RoleWife = function(_super) {
      __extends(RoleWife, _super);
      function RoleWife() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      RoleWife.prototype.start = function() {
        this.Ani.playAnimation("\u4f24\u5fc3", 0);
      };
      RoleWife.prototype.CheckSuccess = function() {
        return this.Liquid == BasePipe_1.Liquid.Health;
      };
      RoleWife.prototype.onBeginContact = function(contact, selfCollider, otherCollider) {
        var _this = this;
        if (this.playAni) return;
        switch (otherCollider.tag) {
         case BasePipe_1.Liquid.Health:
          this.playAni = true;
          this.Liquid = BasePipe_1.Liquid.Health;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u8df3\u821e1", 3);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              _this.Ani.playAnimation("\u8df3\u821e1", 0);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.BoomTank, _this);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckWin);
            });
          });
          break;

         case BasePipe_1.Liquid.Water:
          this.Liquid = BasePipe_1.Liquid.Water;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u6df9\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Ice:
          this.Liquid = BasePipe_1.Liquid.Ice;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u51bb\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Poison:
          this.Liquid = BasePipe_1.Liquid.Poison;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u6bd2\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
          break;

         case BasePipe_1.Liquid.Fire:
          this.Liquid = BasePipe_1.Liquid.Fire;
          this.playAni = true;
          this.Ani.playAnimation("\u5f85\u673a", 3);
          this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
            _this.Ani.playAnimation("\u70e7\u6b7b", 0);
            _this.Ani.once(dragonBones.EventObject.LOOP_COMPLETE, function() {
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.CheckFail, _this);
            });
          });
        }
      };
      RoleWife = __decorate([ ccclass ], RoleWife);
      return RoleWife;
    }(BaseRole_1.default);
    exports.default = RoleWife;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "./BasePipe": "BasePipe",
    "./BaseRole": "BaseRole"
  } ],
  RoomDetailView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "4d4122MY/ZFfp1iH1JBtGj6", "RoomDetailView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.RoomDetailView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var RoomDetailView = function(_super) {
      __extends(RoomDetailView, _super);
      function RoomDetailView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeDetail = void 0;
        _this.nodeRole = void 0;
        _this.btnReturn = void 0;
        _this.roleAni = void 0;
        _this._DanceName = [];
        _this._boolNew = false;
        return _this;
      }
      RoomDetailView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.RoomDetailView);
        true != this._boolNew && UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.RoomView);
      };
      RoomDetailView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          var data = msg;
          this.PlayRoleAni();
          this.ShowRoom();
          this._boolNew = data;
          if (this._boolNew) {
            this.btnReturn.node.active = false;
            this.ShowReward();
          }
        }
      };
      RoomDetailView.prototype.ShowRoom = function() {
        return __awaiter(this, void 0, void 0, function() {
          var nowRoom, node, prefab, x, element, data, x, element, child;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              nowRoom = GameData_1.GameData.UserData.RoomDetail;
              node = this.nodeDetail.children[0];
              if (!(null == node || node.name != nowRoom)) return [ 3, 2 ];
              console.log(nowRoom);
              this.nodeDetail.destroyAllChildren();
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, nowRoom) ];

             case 1:
              prefab = _a.sent();
              node = cc.instantiate(prefab);
              node.setParent(this.nodeDetail);
              _a.label = 2;

             case 2:
              for (x = 0; x < node.childrenCount; x++) {
                element = node.children[x];
                element.children[0].active = false;
                element.getComponent(cc.Sprite).enabled = true;
              }
              data = GameData_1.GameData.GetRoomDetail(nowRoom);
              console.log(data);
              for (x = 0; x < data.length; x++) {
                element = data[x];
                child = node.getChildByName(nowRoom + "\u573a\u666f_" + element);
                if (child) {
                  child.children[0].active = true;
                  child.getComponent(cc.Sprite).enabled = false;
                } else console.log("can not find child " + nowRoom + "\u573a\u666f_" + element);
              }
              return [ 2 ];
            }
          });
        });
      };
      RoomDetailView.prototype.ShowReward = function() {
        return __awaiter(this, void 0, void 0, function() {
          var nowRoom, Reward, prefab, node;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              nowRoom = GameData_1.GameData.UserData.RoomDetail;
              Reward = GameData_1.GameData.GetNowRoomDetailReward();
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u65b0\u7269\u54c1") ];

             case 1:
              prefab = _a.sent();
              node = cc.instantiate(prefab);
              node.setParent(this.node);
              node.scale = 0;
              Util_1.Util.Res.LoadAtlas(node.getChildByName("\u56fe\u7247").getComponent(cc.Sprite), "\u65b0" + nowRoom + "\u573a\u666f_" + Reward);
              cc.tween(node).to(2, {
                scale: 1
              }).delay(3).call(function() {
                return __awaiter(_this, void 0, void 0, function() {
                  var nodParent, type, child, yan, yanNode_1;
                  var _this = this;
                  return __generator(this, function(_a) {
                    switch (_a.label) {
                     case 0:
                      nodParent = this.nodeDetail.children[0];
                      console.log(nodParent, nodParent.name);
                      type = GameData_1.GameData.GetRoomType(nowRoom, Reward);
                      child = nodParent.getChildByName(nowRoom + "\u573a\u666f_" + Reward);
                      if (!(1 == type)) return [ 3, 2 ];
                      if (child) {
                        child.children[0].active = true;
                        child.getComponent(cc.Sprite).enabled = false;
                      } else console.log("can not find child " + nowRoom + "\u573a\u666f_" + Reward);
                      this.btnReturn.node.active = true;
                      return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.model, "\u70df") ];

                     case 1:
                      yan = _a.sent();
                      yanNode_1 = cc.instantiate(yan);
                      yanNode_1.setParent(child);
                      setTimeout(function() {
                        yanNode_1.destroy();
                      }, 500);
                      return [ 3, 3 ];

                     case 2:
                      child.children[0].active = true;
                      cc.tween(child.children[0]).by(0, {
                        x: -300
                      }).by(3, {
                        x: 300
                      }).call(function() {
                        child.getComponent(cc.Sprite).enabled = false;
                        _this.btnReturn.node.active = true;
                      }).start();
                      _a.label = 3;

                     case 3:
                      this.PlayHappyAni();
                      node.destroy();
                      GameData_1.GameData.AddRoomDetail(nowRoom, Reward);
                      return [ 2 ];
                    }
                  });
                });
              }).start();
              return [ 2 ];
            }
          });
        });
      };
      RoomDetailView.prototype.PlayHappyAni = function() {
        cc.Tween.stopAllByTarget(this.nodeRole);
        this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + "\u80dc\u5229", 0);
      };
      RoomDetailView.prototype.PlayRoleAni = function() {
        var _this = this;
        if (0 == this._DanceName.length) {
          this._DanceName = [ "\u8df3\u821e1", "\u8df3\u821e2", "\u8d70\u8def" ];
          Util_1.Util.Random.ArrayBreakOrder(this._DanceName);
        }
        cc.Tween.stopAllByTarget(this.nodeRole);
        this.nodeRole.x = 0;
        var aniName = this._DanceName.shift();
        var timer = 2;
        var time = this.roleAni.armature().armatureData.getAnimation(GameData_1.GameData.getSuitName() + aniName).duration * timer;
        if ("\u8df3\u821e2" == aniName) {
          this._DanceName.unshift("\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3", "\u8df3\u821e3");
          console.log(this._DanceName);
          cc.tween(this.nodeRole).call(function() {
            _this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + aniName, 2 * timer);
          }).delay(2 * time).call(function() {
            _this.PlayRoleAni();
          }).start();
        } else "\u8df3\u821e2\u8fc7\u6e21\u8df3\u821e3" == aniName ? cc.tween(this.nodeRole).call(function() {
          _this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer / timer);
        }).delay(time / timer).call(function() {
          _this.PlayRoleAni();
        }).start() : cc.tween(this.nodeRole).call(function() {
          _this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: 200
        }).to(0, {
          scaleX: -1
        }).call(function() {
          _this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + aniName, 2 * timer);
        }).by(2 * time, {
          x: -400
        }).to(0, {
          scaleX: 1
        }).call(function() {
          _this.roleAni.playAnimation(GameData_1.GameData.getSuitName() + aniName, timer);
        }).by(time, {
          x: 200
        }).call(function() {
          _this.PlayRoleAni();
        }).start();
      };
      __decorate([ property(cc.Node) ], RoomDetailView.prototype, "nodeDetail", void 0);
      __decorate([ property(cc.Node) ], RoomDetailView.prototype, "nodeRole", void 0);
      __decorate([ property(cc.Button) ], RoomDetailView.prototype, "btnReturn", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], RoomDetailView.prototype, "roleAni", void 0);
      RoomDetailView = __decorate([ ccclass ], RoomDetailView);
      return RoomDetailView;
    }(cc.Component);
    exports.RoomDetailView = RoomDetailView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  RoomItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "b3fa8MDlAJOHIgsnwTXexl5", "RoomItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.RoomItem = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var RoomItem = function(_super) {
      __extends(RoomItem, _super);
      function RoomItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodePic_arr = [];
        _this.nodePicPoint = void 0;
        _this.labelLv = void 0;
        _this.nodeLock = void 0;
        _this.labelName = void 0;
        return _this;
      }
      RoomItem.prototype.start = function() {
        var _this = this;
        this.node.on(cc.Node.EventType.TOUCH_END, function() {
          _this.nodeLock.active || UIManager_1.UIManager.Notif(EnumDef_1.UIName.RoomDetailView, EnumDef_1.UIInfo.ShowView, false);
        });
      };
      RoomItem.prototype.SetData = function() {
        var index = this.node.getSiblingIndex();
        this.labelName.string = GameData_1.GameData.RoomData[index];
        var Mission = 40 * index;
        this.nodeLock.active = GameData_1.GameData.UserData.Mission < Mission;
        this.labelLv.string = "" + Mission;
        this.nodePic_arr.forEach(function(e, x) {
          e.active = x == index;
        });
      };
      __decorate([ property(cc.Node) ], RoomItem.prototype, "nodePic_arr", void 0);
      __decorate([ property(cc.Node) ], RoomItem.prototype, "nodePicPoint", void 0);
      __decorate([ property(cc.Label) ], RoomItem.prototype, "labelLv", void 0);
      __decorate([ property(cc.Node) ], RoomItem.prototype, "nodeLock", void 0);
      __decorate([ property(cc.Label) ], RoomItem.prototype, "labelName", void 0);
      RoomItem = __decorate([ ccclass ], RoomItem);
      return RoomItem;
    }(cc.Component);
    exports.RoomItem = RoomItem;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  RoomView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fe8ffx4Hg5AKIp5RdfqkhHv", "RoomView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.RoomView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var RoomItem_1 = require("./RoomItem");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var RoomView = function(_super) {
      __extends(RoomView, _super);
      function RoomView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnReturn = void 0;
        _this.nodeLayout = void 0;
        _this.roomItem = void 0;
        return _this;
      }
      RoomView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.RoomView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      RoomView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.ShowAllLevel();
        }
      };
      RoomView.prototype.ShowAllLevel = function() {
        this.nodeLayout.children.forEach(function(e) {
          return e.active = false;
        });
        for (var x = 0; x < GameData_1.GameData.RoomData.length; x++) {
          var node = this.nodeLayout.children[x];
          if (null == node) {
            node = cc.instantiate(this.roomItem);
            node.setParent(this.nodeLayout);
          }
          node.active = true;
          node.getComponent(RoomItem_1.RoomItem).SetData();
        }
      };
      __decorate([ property(cc.Button) ], RoomView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Node) ], RoomView.prototype, "nodeLayout", void 0);
      __decorate([ property(cc.Prefab) ], RoomView.prototype, "roomItem", void 0);
      RoomView = __decorate([ ccclass ], RoomView);
      return RoomView;
    }(cc.Component);
    exports.RoomView = RoomView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "./RoomItem": "RoomItem"
  } ],
  SDKEnum: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2602bRjnMVKkZt5JyGlEUtY", "SDKEnum");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.PageType = void 0;
    var PageType;
    (function(PageType) {
      PageType[PageType["ReturnPage"] = 0] = "ReturnPage";
      PageType[PageType["DownPage"] = 1] = "DownPage";
      PageType[PageType["CoupletPage"] = 2] = "CoupletPage";
      PageType[PageType["MoreGamePage"] = 3] = "MoreGamePage";
      PageType[PageType["AppletPage"] = 4] = "AppletPage";
      PageType[PageType["LeftDrawerPage"] = 5] = "LeftDrawerPage";
      PageType[PageType["RightDrawerPage"] = 6] = "RightDrawerPage";
      PageType[PageType["InsidePage"] = 7] = "InsidePage";
      PageType[PageType["SettlementPage"] = 8] = "SettlementPage";
      PageType[PageType["FailPage"] = 9] = "FailPage";
      PageType[PageType["OldUsersPage"] = 10] = "OldUsersPage";
      PageType[PageType["MoreGamePage_Old"] = 11] = "MoreGamePage_Old";
      PageType[PageType["OldMoreGame"] = 12] = "OldMoreGame";
    })(PageType = exports.PageType || (exports.PageType = {}));
    cc._RF.pop();
  }, {} ],
  SDKManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "167a0YM9ylBIalA5b7QEbmX", "SDKManager");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SDKManager = void 0;
    var UIManager_1 = require("../Core/UIManager");
    var EnumDef_1 = require("../modules/EnumDef");
    var AdBanner_1 = require("./modules/AdBanner");
    var AdInsert_1 = require("./modules/AdInsert");
    var AdRecord_1 = require("./modules/AdRecord");
    var Analytics_1 = require("./modules/Analytics");
    var Vibrate_1 = require("./modules/Vibrate");
    var AdVideo_1 = require("./modules/AdVideo");
    var Util_1 = require("../Core/Util");
    var GameData_1 = require("../modules/data/GameData");
    var Platform_1 = require("../../global/libs/Platform");
    var UmSdk_1 = require("../../global/libs/UmSdk");
    var _SDKManager = function() {
      function _SDKManager() {
        this._customShow = false;
        this._customCenterShow = false;
        this.adType = EnumDef_1.AdType.None;
        this.HadVideoAd = true;
        this.AdBanner = new AdBanner_1.NoneAdBanner();
        this.AdVideo = new AdVideo_1.NoneAdVideo();
        this.AdRecord = new AdRecord_1.NoneAdRecord();
        this.AdInsert = new AdInsert_1.NoneAdInsert();
        this.Physical = new Vibrate_1.NonePhysical();
        this.Analytics = new Analytics_1.NoneAnalytics();
        this.iconDataList = [];
        this._wxCustomAd = null;
        this._wxCustomAd1 = null;
        this._wxCustomAd2 = null;
        this._wxCustomBoolShow = {};
        this._customLeftId = [];
        this._customRightId = [];
        this._customLeftIndex = 0;
        this._customRightIndex = 0;
        this._customLeftShow = false;
        this._customRightShow = false;
        this._videoOnce = false;
      }
      _SDKManager.prototype.Init = function() {
        cc.sys.isBrowser ? this.adType = EnumDef_1.AdType.None : cc.sys.platform == cc.sys.WECHAT_GAME ? this.adType = EnumDef_1.AdType.UMWeChat : cc.sys.platform == cc.sys.BYTEDANCE_GAME && (this.adType = EnumDef_1.AdType.TT);
        console.log("Platform", cc.sys.platform, this.adType);
        switch (this.adType) {
         case EnumDef_1.AdType.TT:
          this.AdBanner = new AdBanner_1.TTAdBanner("1ae8e1a149i5518fj7");
          this.AdInsert = new AdInsert_1.TTAdInsert("12h03i1559j21nlhro");
          this.AdVideo = new AdVideo_1.TTAdVideo("8go2m1nlinr4chgg6h");
          this.AdRecord = new AdRecord_1.TTAdRecord({
            title: "\u8fd9\u4e2a\u6e38\u620f\u592a\u597d\u73a9\u4e86",
            desc: "\u5feb\u6765\u548c\u61d2\u7f8a\u7f8a\u4e00\u8d77\u5b88\u62a4\u7f8a\u6751\uff0c\u62ef\u6551\u5947\u732b\u56fd\uff01",
            topics: [ "\u559c\u7f8a\u7f8a\u4e0e\u7070\u592a\u72fc", "\u7f8a\u6751\u5b88\u62a4\u8005", "\u5f02\u56fd\u5927\u8425\u6551" ]
          });
          this.Physical = new Vibrate_1.TTPhysical();
          this.Analytics = new Analytics_1.TTAnalytics();
          this.FetchTTConfig();
          break;

         case EnumDef_1.AdType.UMWeChat:
          wx.showShareMenu({
            menus: [ "shareAppMessage", "shareTimeline" ]
          });
          break;

         case EnumDef_1.AdType.QQ:
         case EnumDef_1.AdType.None:
        }
      };
      Object.defineProperty(_SDKManager.prototype, "UMSDK", {
        get: function() {
          return this.UmSdk;
        },
        enumerable: false,
        configurable: true
      });
      _SDKManager.prototype.OnUpdate = function(deltaTime) {};
      _SDKManager.prototype.CheckAdType = function(adType) {
        return this.adType == adType;
      };
      _SDKManager.prototype.ShowBanner = function(style) {
        this.AdBanner.Show(style);
      };
      _SDKManager.prototype.CloseBanner = function() {
        this.AdBanner.Hide();
      };
      _SDKManager.prototype.GetIconAD = function(slotId, limit) {
        void 0 === limit && (limit = 10);
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve, reject) {
                return __awaiter(_this, void 0, void 0, function() {
                  var x, element;
                  var _this = this;
                  var _a;
                  return __generator(this, function(_b) {
                    for (x = 0; x < this.iconDataList.length; x++) {
                      element = this.iconDataList[x];
                      if (element.slotId == slotId) {
                        resolve(element.iconData);
                        return [ 2 ];
                      }
                    }
                    null === (_a = this.UmSdk) || void 0 === _a ? void 0 : _a.createIconAd({
                      slotId: slotId,
                      limit: limit
                    }).load().then(function(iconList) {
                      _this.iconDataList.push({
                        iconData: iconList,
                        slotId: slotId
                      });
                      console.log("already Load icon", slotId);
                      _this._LoadIcon(iconList);
                      resolve(iconList);
                    }).catch(function(err) {
                      console.log(err);
                      reject(null);
                    });
                    return [ 2 ];
                  });
                });
              }) ];

             case 1:
              return [ 2, _a.sent() ];
            }
          });
        });
      };
      _SDKManager.prototype.StartLoadIcon = function() {
        return __awaiter(this, void 0, void 0, function() {
          return __generator(this, function(_a) {
            exports.SDKManager.GetIconAD("308437f025ff0c3a", 16);
            exports.SDKManager.GetIconAD("23885756d5348739", 20);
            exports.SDKManager.GetIconAD("dca01081d1c14007", 16);
            exports.SDKManager.GetIconAD("943dba3cbb4750b1", 8);
            return [ 2 ];
          });
        });
      };
      _SDKManager.prototype._LoadIcon = function(IconData) {
        for (var x = 0; x < IconData.length; x++) {
          var element = IconData[x];
          Util_1.Util.Res.loadRemoteRes(element.data.icon);
        }
      };
      _SDKManager.prototype.CreateCustomAd = function() {
        if (!exports.SDKManager.CheckAdType(EnumDef_1.AdType.UMWeChat)) return;
        var res = wx.getSystemInfoSync();
        var width = 750;
        var height = 1334;
        var statusBarHeight = 150;
        try {
          var res_1 = wx.getSystemInfoSync();
          width = res_1.screenWidth;
          height = res_1.screenHeight;
          statusBarHeight = res_1.statusBarHeight;
        } catch (e) {
          console.log("\u65e0\u6cd5\u83b7\u5f97\u7cfb\u7edf\u4fe1\u606f", e);
        }
        var centerAd = wx.createCustomAd({
          adUnitId: "adunit-b824538968fe1de5",
          adIntervals: 30,
          style: {
            left: 0,
            top: height / 2 - 180
          }
        });
        centerAd.onClose(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u5173\u95ed");
        });
        centerAd.onHide(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u9690\u85cf");
        });
        centerAd.onError(function(res) {
          console.log("\u539f\u751f\u5e7f\u544a\u9519\u8bef", res.errMsg, res.errCode);
        });
        centerAd.onLoad(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u52a0\u8f7d");
        });
        this._wxCustomAd = centerAd;
        var customAd = wx.createCustomAd({
          adUnitId: "adunit-d4e81f3aa9dc6af7",
          adIntervals: 30,
          style: {
            left: 20,
            top: height / 2
          }
        });
        customAd.onClose(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u5173\u95ed");
        });
        customAd.onHide(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u9690\u85cf");
        });
        customAd.onError(function(res) {
          console.log("\u539f\u751f\u5e7f\u544a\u9519\u8bef", res.errMsg, res.errCode);
        });
        customAd.onLoad(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u52a0\u8f7d");
        });
        this._wxCustomAd1 = customAd;
        var customAd2 = wx.createCustomAd({
          adUnitId: "adunit-44f24e2e3e74c72d",
          adIntervals: 30,
          style: {
            left: width - 100,
            top: height / 2
          }
        });
        customAd2.onClose(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u5173\u95ed");
        });
        customAd2.onHide(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u9690\u85cf");
        });
        customAd2.onError(function(res) {
          console.log("\u539f\u751f\u5e7f\u544a\u9519\u8bef", res.errMsg, res.errCode);
        });
        customAd2.onLoad(function() {
          console.log("\u539f\u751f\u5e7f\u544a\u52a0\u8f7d");
        });
        this._wxCustomAd2 = customAd2;
      };
      _SDKManager.prototype.ShowCustomAd = function() {
        var _this = this;
        var _a, _b;
        this._customShow = true;
        null === (_a = this._wxCustomAd1) || void 0 === _a ? void 0 : _a.show().then(function() {
          console.log("\u539f\u751f\u5e7f\u544a", _this._wxCustomAd1.isShow());
          _this._customShow || _this._wxCustomAd1.isShow() && _this._wxCustomAd1.hide();
        });
        null === (_b = this._wxCustomAd2) || void 0 === _b ? void 0 : _b.show().then(function() {
          console.log("\u539f\u751f\u5e7f\u544a", _this._wxCustomAd2.isShow());
          _this._customShow || _this._wxCustomAd2.isShow() && _this._wxCustomAd2.hide();
        });
      };
      _SDKManager.prototype.CloseCustomAd = function() {
        var _a, _b;
        this._customShow = false;
        null === (_a = this._wxCustomAd1) || void 0 === _a ? void 0 : _a.hide();
        null === (_b = this._wxCustomAd2) || void 0 === _b ? void 0 : _b.hide();
      };
      _SDKManager.prototype.ShowCenterCustomAd = function() {
        var _this = this;
        var _a;
        this._customCenterShow = true;
        null === (_a = this._wxCustomAd) || void 0 === _a ? void 0 : _a.show().then(function() {
          console.log("\u539f\u751f\u5e7f\u544a", _this._wxCustomAd.isShow());
          _this._customCenterShow || _this._wxCustomAd.isShow() && _this._wxCustomAd.hide();
        });
      };
      _SDKManager.prototype.CloseCenterCustomAd = function() {
        var _a;
        this._customCenterShow = false;
        null === (_a = this._wxCustomAd) || void 0 === _a ? void 0 : _a.hide();
      };
      _SDKManager.prototype.ChangeWXCustomAd = function(adUnitId, bool) {
        var _this = this;
        if (!this.CheckAdType(EnumDef_1.AdType.UMWeChat)) return;
        this._wxCustomBoolShow[adUnitId] = bool;
        if (bool && 0 == this._wxCustomAd.length) {
          var width = 1334;
          var statusBarHeight = 150;
          try {
            var res = wx.getSystemInfoSync();
            width = res.screenWidth;
            statusBarHeight = res.statusBarHeight;
          } catch (e) {
            console.log("\u65e0\u6cd5\u83b7\u5f97\u7cfb\u7edf\u4fe1\u606f", e);
          }
          var start = 80;
          var style = [ {
            adUnitId: "adunit-5a884a4a935fad74",
            adIntervals: 30,
            style: {
              left: width - 120,
              top: statusBarHeight + 100
            }
          } ];
          for (var x = 0; x < style.length; x++) {
            var customAd_1 = wx.createCustomAd(style[x]);
            this._wxCustomAd[style[x].adUnitId] = customAd_1;
          }
        }
        var customAd = this._wxCustomAd[adUnitId];
        customAd && (this._wxCustomBoolShow[adUnitId] ? customAd.isShow() || customAd.show().then(function() {
          console.log(adUnitId, "ShowCustom", customAd.isShow());
          _this._wxCustomBoolShow[adUnitId] || customAd.isShow() && customAd.hide();
        }) : customAd.isShow() && customAd.hide());
      };
      _SDKManager.prototype.ShowInsert = function() {
        this.AdInsert.Show();
      };
      _SDKManager.prototype.PlayVideo = function(type, success, fail, stop) {
        var _this = this;
        void 0 === fail && (fail = null);
        void 0 === stop && (stop = null);
        Platform_1.platform.showRewardVideo(function(err, code) {
          console.log(err, code);
          if (err) {
            fail && fail();
            _this.ReportAnalytics(EnumDef_1.AnalyticsEnum.VideoType, {
              type: type,
              status: "fail"
            });
          } else {
            success && success();
            _this.ReportAnalytics(EnumDef_1.AnalyticsEnum.VideoType, {
              type: type,
              status: "successs"
            });
          }
        });
        return;
      };
      _SDKManager.prototype.isRVAvailable = function() {
        return this.AdVideo.isAvailable();
      };
      _SDKManager.prototype.RewardVideoFail = function() {};
      _SDKManager.prototype.GetRecordTweenTime = function() {
        return GameData_1.GameData.NowTime - this.BeginRecordTime;
      };
      _SDKManager.prototype.BeginRecord = function() {
        this.BeginRecordTime = GameData_1.GameData.NowTime;
        this.AdRecord.Start();
      };
      _SDKManager.prototype.EndRecord = function() {
        this.AdRecord.Stop();
      };
      _SDKManager.prototype.ShareRecord = function(ShareType, success, fail) {
        void 0 === fail && (fail = null);
        this.AdRecord.Share(function() {
          success();
        }, fail);
      };
      _SDKManager.prototype.Vibrate = function(boolLong) {
        void 0 === boolLong && (boolLong = false);
        boolLong ? this.Physical.VibrateLong() : this.Physical.VibrateShort();
      };
      _SDKManager.prototype.ReportAnalytics = function(eventName, data) {
        var _a;
        UmSdk_1.umSdk.ReportAnalytics(eventName, data);
        return;
        var str;
        var key;
        var element;
      };
      _SDKManager.prototype.FetchTTConfig = function() {};
      _SDKManager.prototype.FetchWxConfig = function() {};
      _SDKManager.prototype.ExportCloseCallBack = function(arg0) {
        this._ExportCloseCallBack = arg0;
      };
      _SDKManager.prototype.CallExportCloseCallBack = function() {
        if (null != this._ExportCloseCallBack) {
          var t = this._ExportCloseCallBack;
          this._ExportCloseCallBack = null;
          t();
        }
      };
      return _SDKManager;
    }();
    exports.SDKManager = new _SDKManager();
    cc._RF.pop();
  }, {
    "../../global/libs/Platform": "Platform",
    "../../global/libs/UmSdk": "UmSdk",
    "../Core/UIManager": "UIManager",
    "../Core/Util": "Util",
    "../modules/EnumDef": "EnumDef",
    "../modules/data/GameData": "GameData",
    "./modules/AdBanner": "AdBanner",
    "./modules/AdInsert": "AdInsert",
    "./modules/AdRecord": "AdRecord",
    "./modules/AdVideo": "AdVideo",
    "./modules/Analytics": "Analytics",
    "./modules/Vibrate": "Vibrate"
  } ],
  SetView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c6bb5BfrvdF+r9YoOASVySb", "SetView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SetView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var SetView = function(_super) {
      __extends(SetView, _super);
      function SetView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnClose = void 0;
        _this.nodeOffSound = void 0;
        _this.btnSound = void 0;
        _this.nodeOffEffect = void 0;
        _this.btnEffect = void 0;
        _this.nodeOffShake = void 0;
        _this.btnShake = void 0;
        return _this;
      }
      SetView.prototype.click_btn_close = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.SetView);
      };
      SetView.prototype.click_btn_sound = function(e) {
        GameData_1.GameData.SettingData.sound = 0 == GameData_1.GameData.SettingData.sound ? 1 : 0;
        GameData_1.GameData.SettingData.SaveData();
        cc.audioEngine.setMusicVolume(GameData_1.GameData.SettingData.sound ? 1 : 0);
        this.RefreshBtn();
      };
      SetView.prototype.click_btn_effect = function(e) {
        GameData_1.GameData.SettingData.effect = 0 == GameData_1.GameData.SettingData.effect ? 1 : 0;
        GameData_1.GameData.SettingData.SaveData();
        cc.audioEngine.setEffectsVolume(GameData_1.GameData.SettingData.effect ? 1 : 0);
        this.RefreshBtn();
      };
      SetView.prototype.click_btn_shake = function(e) {
        GameData_1.GameData.SettingData.vibration = 0 == GameData_1.GameData.SettingData.vibration ? 1 : 0;
        GameData_1.GameData.SettingData.SaveData();
        this.RefreshBtn();
      };
      SetView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.RefreshBtn();
        }
      };
      SetView.prototype.RefreshBtn = function() {
        this.nodeOffEffect.active = 0 == GameData_1.GameData.SettingData.effect;
        this.nodeOffSound.active = 0 == GameData_1.GameData.SettingData.sound;
        this.nodeOffShake.active = 0 == GameData_1.GameData.SettingData.vibration;
      };
      __decorate([ property(cc.Button) ], SetView.prototype, "btnClose", void 0);
      __decorate([ property(cc.Node) ], SetView.prototype, "nodeOffSound", void 0);
      __decorate([ property(cc.Button) ], SetView.prototype, "btnSound", void 0);
      __decorate([ property(cc.Node) ], SetView.prototype, "nodeOffEffect", void 0);
      __decorate([ property(cc.Button) ], SetView.prototype, "btnEffect", void 0);
      __decorate([ property(cc.Node) ], SetView.prototype, "nodeOffShake", void 0);
      __decorate([ property(cc.Button) ], SetView.prototype, "btnShake", void 0);
      SetView = __decorate([ ccclass ], SetView);
      return SetView;
    }(cc.Component);
    exports.SetView = SetView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  SignView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "62f60zV0JhLBL0Auc7Ps5D1", "SignView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.SignView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var SignView = function(_super) {
      __extends(SignView, _super);
      function SignView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeWater_arr = [];
        _this.nodeSignBg_arr = [];
        _this.nodeSignSuccess_arr = [];
        _this.labelSignMoney_arr = [];
        _this.btnReturn = void 0;
        _this.labelAdReward = void 0;
        _this.btnAdReward = void 0;
        return _this;
      }
      SignView.prototype.start = function() {
        var _this = this;
        this.nodeSignBg_arr.forEach(function(e, x) {
          e.parent.on(cc.Node.EventType.TOUCH_END, function() {
            console.log("????");
            var date = new Date().toDateString();
            if (GameData_1.GameData.UserData.LastSignDay == date) return;
            GameData_1.GameData.UserData.LastSignDay = date;
            GameData_1.GameData.UserData.Sign++;
            GameData_1.GameData.UserData.Money += GameData_1.GameData.SignRewardMoney[x];
            GameData_1.GameData.UserData.SaveData();
            _this.ShowSign();
            UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
          });
        });
      };
      SignView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.SignView);
      };
      SignView.prototype.click_btn_ad_reward = function(e) {
        SDKManager_1.SDKManager.PlayVideo("\u7b7e\u5230\u89c6\u9891", function() {
          var LastSignDay = GameData_1.GameData.UserData.Sign;
          GameData_1.GameData.UserData.Money += 5 * GameData_1.GameData.SignRewardMoney[LastSignDay - 1];
          GameData_1.GameData.UserData.SaveData();
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.SignView);
        });
      };
      SignView.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.ShowSign();
        }
      };
      SignView.prototype.ShowSign = function() {
        var date = new Date().toDateString();
        console.log(date);
        var LastSignDay = GameData_1.GameData.UserData.Sign;
        this.nodeWater_arr.forEach(function(e, x) {
          return e.active = x <= LastSignDay;
        });
        this.nodeSignBg_arr.forEach(function(e, x) {
          e.active = x == LastSignDay && date != GameData_1.GameData.UserData.LastSignDay;
        });
        this.nodeSignSuccess_arr.forEach(function(e, x) {
          e.active = x < LastSignDay;
        });
        this.labelSignMoney_arr.forEach(function(e, x) {
          e.string = "" + GameData_1.GameData.SignRewardMoney[x];
        });
        this.btnAdReward.node.active = date == GameData_1.GameData.UserData.LastSignDay;
        this.labelAdReward.string = "" + 5 * GameData_1.GameData.SignRewardMoney[LastSignDay - 1];
      };
      __decorate([ property(cc.Node) ], SignView.prototype, "nodeWater_arr", void 0);
      __decorate([ property(cc.Node) ], SignView.prototype, "nodeSignBg_arr", void 0);
      __decorate([ property(cc.Node) ], SignView.prototype, "nodeSignSuccess_arr", void 0);
      __decorate([ property(cc.Label) ], SignView.prototype, "labelSignMoney_arr", void 0);
      __decorate([ property(cc.Button) ], SignView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Label) ], SignView.prototype, "labelAdReward", void 0);
      __decorate([ property(cc.Button) ], SignView.prototype, "btnAdReward", void 0);
      SignView = __decorate([ ccclass ], SignView);
      return SignView;
    }(cc.Component);
    exports.SignView = SignView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  StraightPipe: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0188eVPIzpKuoRPV+PHbBv/", "StraightPipe");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameData_1 = require("../data/GameData");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var StraightPipe = function(_super) {
      __extends(StraightPipe, _super);
      function StraightPipe() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.Water = [];
        _this.HighLight = [];
        _this._fill = false;
        return _this;
      }
      Object.defineProperty(StraightPipe.prototype, "Ani", {
        get: function() {
          null == this._ani && (this._ani = this.node.getComponent(cc.Animation));
          return this._ani;
        },
        set: function(v) {
          this._ani = v;
        },
        enumerable: false,
        configurable: true
      });
      StraightPipe.prototype.AddJoint = function(selfJoint) {
        this._fill && this.Liquid != BasePipe_1.Liquid.Empty && this.Output();
      };
      StraightPipe.prototype.Input = function(liquid, joint) {
        this.Liquid = liquid;
        this._From = joint;
        this.Filling();
      };
      StraightPipe.prototype.Output = function() {
        var _this = this;
        this.Joint.forEach(function(e) {
          e != _this._From && e.Output(_this.Liquid);
        });
      };
      StraightPipe.prototype.Filling = function() {
        var _this = this;
        var _a, _b, _c;
        this._fill = false;
        this.HighLight.forEach(function(e) {
          return e.node.active = false;
        });
        if (this.Liquid == BasePipe_1.Liquid.Empty) {
          this.Water.forEach(function(e) {
            return e.active = false;
          });
          this.Ani.stop();
          this.Ani.off(cc.Animation.EventType.FINISHED);
          this.Output();
          return;
        }
        this.Water.forEach(function(e) {
          return e.color = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
        });
        var index = this.Joint.indexOf(this._From);
        var clips = this.Ani.getClips();
        if (0 == index) null === (_a = this.Ani) || void 0 === _a ? void 0 : _a.play(clips[0].name, 0); else {
          index = 1;
          null === (_b = this.Ani) || void 0 === _b ? void 0 : _b.play(clips[2].name, 0);
        }
        null === (_c = this.Ani) || void 0 === _c ? void 0 : _c.once(cc.Animation.EventType.FINISHED, function() {
          _this._fill = true;
          _this.Output();
          _this.HighLight.forEach(function(e, x) {
            e.node.active = x == index;
            e.startColor = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
            e.endColor = GameData_1.GameData.GetColorByLiquid(_this.Liquid);
          });
        });
      };
      StraightPipe.prototype.RemoveJoint = function(joint) {
        if (joint == this._From) {
          this.Liquid = BasePipe_1.Liquid.Empty;
          this._From = null;
          this.Filling();
        }
      };
      __decorate([ property([ cc.Node ]) ], StraightPipe.prototype, "Water", void 0);
      __decorate([ property([ cc.ParticleSystem ]) ], StraightPipe.prototype, "HighLight", void 0);
      __decorate([ property(cc.Animation) ], StraightPipe.prototype, "Ani", null);
      StraightPipe = __decorate([ ccclass ], StraightPipe);
      return StraightPipe;
    }(BasePipe_1.default);
    exports.default = StraightPipe;
    cc._RF.pop();
  }, {
    "../data/GameData": "GameData",
    "./BasePipe": "BasePipe"
  } ],
  TipsView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "db1cbAM4stNu7drHPqPVz30", "TipsView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.TipsView = void 0;
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var TipsView = function(_super) {
      __extends(TipsView, _super);
      function TipsView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.labelTip = void 0;
        return _this;
      }
      TipsView.prototype.start = function() {
        this.node.setSiblingIndex(this.node.parent.childrenCount - 1);
      };
      TipsView.prototype.notif = function(info, body) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.labelTip.string = body;
          cc.Tween.stopAllByTarget(this.node);
          this.node.opacity = 0;
          cc.tween(this.node).to(.5, {
            opacity: 255
          }).delay(2).to(.5, {
            opacity: 0
          }).start();
        }
      };
      __decorate([ property(cc.Label) ], TipsView.prototype, "labelTip", void 0);
      TipsView = __decorate([ ccclass ], TipsView);
      return TipsView;
    }(cc.Component);
    exports.TipsView = TipsView;
    cc._RF.pop();
  }, {
    "../EnumDef": "EnumDef"
  } ],
  Tips: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "2fef2ZfSA5PVqhBpH6jTMVK", "Tips");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.tips = void 0;
    var Tips = function() {
      function Tips() {
        this._isInit = false;
        this.containerNode = null;
        this.textNode = null;
      }
      Tips.prototype.init = function() {
        if (this._isInit) return;
        this._isInit = true;
        this._initNode();
      };
      Tips.prototype.show = function(text) {
        var _this = this;
        this.textNode.string = "" + text;
        this.containerNode.stopAllActions();
        this.containerNode.active = true;
        this.containerNode.opacity = 255;
        cc.tween(this.containerNode).delay(1).to(.3, {
          opacity: 0
        }).call(function() {
          _this.containerNode.active = false;
          _this.textNode.string = "";
        }).start();
      };
      Tips.prototype.hide = function() {
        var _this = this;
        this.containerNode.stopAllActions();
        cc.tween(this.containerNode).to(.3, {
          opacity: 0
        }).call(function() {
          _this.containerNode.active = false;
          _this.textNode.string = "";
        }).start();
      };
      Tips.prototype._initNode = function() {
        var node = new cc.Node("Tips");
        var parent = cc.find("Canvas").parent;
        parent.addChild(node);
        cc.game.addPersistRootNode(node);
        var widget = node.addComponent(cc.Widget);
        widget.isAlignVerticalCenter = true;
        widget.isAlignHorizontalCenter = true;
        var containerNode = new cc.Node("Container");
        containerNode.active = false;
        node.addChild(containerNode);
        containerNode.setContentSize(750, 80);
        this.containerNode = containerNode;
        var sprite = containerNode.addComponent(cc.Sprite);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        sprite.type = cc.Sprite.Type.SLICED;
        cc.loader.loadRes("texture/common/tip", cc.SpriteFrame, function(err, res) {
          if (err) return;
          sprite.spriteFrame = res;
          sprite.spriteFrame.insetTop = 20;
          sprite.spriteFrame.insetBottom = 20;
          sprite.spriteFrame.insetRight = 20;
          sprite.spriteFrame.insetLeft = 20;
        });
        var layout = containerNode.addComponent(cc.Layout);
        layout.type = cc.Layout.Type.GRID;
        layout.startAxis = cc.Layout.AxisDirection.HORIZONTAL;
        layout.resizeMode = cc.Layout.ResizeMode.CONTAINER;
        layout.type = cc.Layout.Type.GRID;
        layout.paddingRight = 20;
        layout.paddingLeft = 20;
        var textNode = new cc.Node("Text");
        containerNode.addChild(textNode);
        var label = textNode.addComponent(cc.Label);
        this.textNode = label;
        label.fontSize = 45;
        label.lineHeight = 45;
        label.overflow = cc.Label.Overflow.RESIZE_HEIGHT;
        label.horizontalAlign = cc.Label.HorizontalAlign.CENTER;
        label.verticalAlign = cc.Label.VerticalAlign.CENTER;
        textNode.width = 750;
      };
      return Tips;
    }();
    exports.tips = new Tips();
    cc._RF.pop();
  }, {} ],
  Transition: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bc336wqPodNJLHr+3q1R2wc", "Transition");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.transition = void 0;
    var Transition = function() {
      function Transition() {
        this._isInit = false;
        this.maskNode = null;
        this.isChange = false;
      }
      Transition.prototype.init = function() {
        if (this._isInit) return;
        this._isInit = true;
        this._initNode();
      };
      Transition.prototype.loadScene = function(scene, cb) {
        var _this = this;
        cc.director.preloadScene(scene);
        this.transitionIn(function() {
          cc.director.loadScene(scene, function() {
            cb && cb();
            _this.transitionOut();
            _this.isChange = false;
          });
        });
      };
      Transition.prototype.transitionIn = function(cb) {
        if (this.isChange) return;
        this.isChange = true;
        this.maskNode.group = "UI";
        this.maskNode.setContentSize(2e3, 2e3);
        this.maskNode.active = true;
        this.maskNode.stopAllActions();
        cc.tween(this.maskNode).to(.5, {
          width: 0,
          height: 0
        }).call(function() {
          cb && cb();
        }).start();
      };
      Transition.prototype.transitionOut = function() {
        var _this = this;
        this.maskNode.stopAllActions();
        cc.tween(this.maskNode).to(.5, {
          width: 2e3,
          height: 2e3
        }).call(function() {
          _this.maskNode.active = false;
          _this.isChange = false;
        }).start();
      };
      Transition.prototype._initNode = function() {
        var node = new cc.Node("Transition");
        var parent = cc.find("Canvas").parent;
        parent.addChild(node);
        cc.game.addPersistRootNode(node);
        var winSize = cc.winSize;
        node.position = cc.v2(winSize.width / 2, winSize.height / 2);
        var maskNode = new cc.Node("circleMaskNode");
        maskNode.setContentSize(2e3, 2e3);
        var mask = maskNode.addComponent(cc.Mask);
        maskNode.addComponent(cc.Button);
        maskNode.parent = node;
        mask.type = cc.Mask.Type.ELLIPSE;
        mask.inverted = true;
        mask.segements = 50;
        this.maskNode = maskNode;
        var spriteNode = new cc.Node("Sprite");
        spriteNode.setContentSize(2e3, 2e3);
        maskNode.addChild(spriteNode);
        var sprite = spriteNode.addComponent(cc.Sprite);
        sprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        sprite.node.color = new cc.Color(0, 0, 0);
        cc.loader.loadRes("texture/common/singleColor", cc.SpriteFrame, function(err, res) {
          if (err) return;
          sprite.spriteFrame = res;
        });
      };
      return Transition;
    }();
    exports.transition = new Transition();
    cc._RF.pop();
  }, {} ],
  UIManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "46b6brzxs5I+7vszg5pq2Qc", "UIManager");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.UIManager = void 0;
    var EnumDef_1 = require("../modules/EnumDef");
    var Util_1 = require("./Util");
    var ResManager_1 = require("./ResManager");
    var ViewData = function() {
      function ViewData(modName) {
        this.modName = modName;
        this.show = false;
        this.load = false;
      }
      ViewData.prototype.PlayAni = function(num, cb) {
        var _this = this;
        if (this.ani) {
          var aniState_1 = this.ani.getClips();
          if (0 == num) if (aniState_1[0]) {
            this.node.scale = 0;
            cc.tween(this.node).delay(.01).call(function() {
              _this.ani.play(aniState_1[num].name);
              _this.ani.once(cc.Animation.EventType.FINISHED, function() {
                cb && cb();
              });
            }).start();
          } else cb && cb(); else if (aniState_1[num]) {
            this.ani.play(aniState_1[num].name);
            this.ani.once(cc.Animation.EventType.FINISHED, function() {
              cb && cb();
            });
          } else cb && cb();
        } else cb && cb();
      };
      return ViewData;
    }();
    var _UIManager = function() {
      function _UIManager() {
        this._ViewMap = [];
      }
      _UIManager.prototype.Notif = function(constModName, uiInfo, body) {
        void 0 === body && (body = null);
        var viewData = this.getViewData(constModName);
        if (null == viewData.node) {
          if (uiInfo == EnumDef_1.UIInfo.ShowView) {
            viewData.show = true;
            this.LoadView(viewData, body);
            return;
          }
        } else {
          viewData.node.emit(EnumDef_1.UIInfo.UIEvent, uiInfo, body);
          if (uiInfo == EnumDef_1.UIInfo.ShowView) {
            viewData.show = true;
            viewData.node.active = true;
            viewData.node.pauseSystemEvents(true);
            viewData.PlayAni(0, function() {
              viewData.node.resumeSystemEvents(true);
            });
          } else if (uiInfo == EnumDef_1.UIInfo.CloseView) {
            viewData.node.pauseSystemEvents(true);
            viewData.PlayAni(1, function() {
              viewData.show = false;
              viewData.node.active = false;
            });
          }
        }
      };
      _UIManager.prototype.NotifAll = function(uiInfo, body) {
        void 0 === body && (body = null);
        for (var x = 0; x < this._ViewMap.length; x++) this.Notif(this._ViewMap[x].modName, uiInfo, body);
      };
      _UIManager.prototype.OpenUI = function(modName) {
        this.Notif(modName, EnumDef_1.UIInfo.ShowView);
      };
      _UIManager.prototype.CloseUI = function(modName) {
        this.Notif(modName, EnumDef_1.UIInfo.CloseView);
      };
      _UIManager.prototype.getViewData = function(constModName) {
        for (var x = 0; x < this._ViewMap.length; x++) {
          var element = this._ViewMap[x];
          if (element.modName == constModName) return element;
        }
        var viewData = new ViewData(constModName);
        this._ViewMap.push(viewData);
        return viewData;
      };
      _UIManager.prototype.LoadView = function(viewData, body) {
        return __awaiter(this, void 0, void 0, function() {
          var prefab;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (viewData.load) return [ 2 ];
              viewData.node = ResManager_1.ResManager.GetCacheResource(EnumDef_1.AssetBundleEnum.ui, EnumDef_1.UIName[viewData.modName]);
              if (!(null == viewData.node)) return [ 3, 2 ];
              viewData.load = true;
              return [ 4, Util_1.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.ui, EnumDef_1.UIName[viewData.modName]) ];

             case 1:
              prefab = _a.sent();
              viewData.load = false;
              viewData.node = cc.instantiate(prefab);
              _a.label = 2;

             case 2:
              this.AddChild(viewData);
              if (!viewData.show) return [ 2 ];
              this.Notif(viewData.modName, EnumDef_1.UIInfo.ShowView, body);
              return [ 2 ];
            }
          });
        });
      };
      _UIManager.prototype.InitUILayer = function(node) {
        this.UIRoot = node;
      };
      _UIManager.prototype.AddChild = function(viewData) {
        viewData.node.parent = this.UIRoot;
        viewData.node.position = cc.Vec3.ZERO;
        viewData.node.active = viewData.show;
        viewData.node.zIndex = viewData.modName;
        viewData.ani = viewData.node.getComponent(cc.Animation);
        var widget = viewData.node.getComponent(cc.Widget);
        if (widget) {
          widget.updateAlignment();
          widget.enabled = false;
        }
        var data = viewData.node.getComponent(EnumDef_1.UIName[viewData.modName]);
        console.log("Add", EnumDef_1.UIName[viewData.modName]);
        if (null == data) {
          console.log("UI" + EnumDef_1.UIName[viewData.modName] + "\u4e0a\u4e0d\u542b\u540c\u540d\u811a\u672c");
          return;
        }
        if (null == data.notif) {
          console.log(EnumDef_1.UIName[viewData.modName] + "\u672a\u5b9e\u73b0notif\u65b9\u6cd5");
          return;
        }
        viewData.node.on(EnumDef_1.UIInfo.UIEvent, function(uiinfo, body) {
          void 0 === body && (body = null);
          data.notif(uiinfo, body);
        }, this);
        for (var key in data) if (Object.prototype.hasOwnProperty.call(data, key)) {
          var element = data[key];
          element instanceof cc.Button && this.ChangeBtnSound(element, EnumDef_1.AudioType.audio_Click);
          if (element instanceof Array) for (var x = 0; x < element.length; x++) {
            var btn = element[x];
            if (!(btn instanceof cc.Button)) break;
            this.ChangeBtnSound(btn, EnumDef_1.AudioType.audio_Click);
          }
        }
      };
      _UIManager.prototype.ChangeBtnSound = function(btnStart, audioType) {
        var boolContain = false;
        for (var x = 0; x < btnStart.clickEvents.length; x++) {
          var element = btnStart.clickEvents[x];
          if ("Main" == element.component && eventHandler.target == this.UIRoot.parent && "PlayEffect" == eventHandler.handler) {
            boolContain = true;
            element.customEventData = audioType;
          }
        }
        if (!boolContain) {
          var eventHandler = new cc.Component.EventHandler();
          eventHandler.target = this.UIRoot.parent;
          eventHandler.component = "Main";
          eventHandler.handler = "PlayEffect";
          eventHandler.customEventData = audioType;
          btnStart.clickEvents.push(eventHandler);
        }
      };
      return _UIManager;
    }();
    exports.UIManager = new _UIManager();
    cc._RF.pop();
  }, {
    "../modules/EnumDef": "EnumDef",
    "./ResManager": "ResManager",
    "./Util": "Util"
  } ],
  UmSdk: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "a565cXj/z5FhqWsk22sNJpm", "UmSdk");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.umSdk = void 0;
    var umsdk = require("./umsdk.wechat.js");
    var Platform_1 = require("./Platform");
    var ExportPageController_1 = require("./ExportPageController");
    var EnumDef_1 = require("../../scripts/modules/EnumDef");
    var SDKManager_1 = require("../../scripts/SDK/SDKManager");
    var GameData_1 = require("../../scripts/modules/data/GameData");
    var UIManager_1 = require("../../scripts/Core/UIManager");
    var UmSdk = function() {
      function UmSdk() {
        this.UmSdk = null;
        this.OnLineGameValueList = null;
        this.DelayShowbanner = 0;
        this.BannerErrorDelay = 3;
        this.BannerDelayTime = 1;
        this.DelayShowMin = 1;
        this.DelayShowMax = 2;
        this.LuckBagGap = 0;
        this.LuckBagOpen = 0;
        this.ReturnControl = 0;
        this.LuckBag = 0;
        this.LuckState = 2;
        this.Jackpot = 2;
        this.ExportStatus = 0;
        this.ShowPrizeVideo = 0;
        this.ShowVideoIcon = 0;
        this.OldUserExportStatus = 0;
        this.invideoopen = 0;
        this.firsterroronoff = 0;
        this.MaxHeartNum = 10;
        this.GetHeartvalue = 3;
        this.MaxStrength = 30;
        this.MoreGamePageGapTime = 1;
        this.ReturnPageGapTime = 1;
        this.MysticBoxLimit = 3;
        this.MissClickStatus = 0;
        this.ReturnPageJump = 1;
        this.ShowAdView = 0;
        this.ErrorCity = "\u4e0a\u6d77\u5e02\uff0c\u6210\u90fd\u5e02\uff0c\u6b66\u6c49\u5e02\uff0c\u6df1\u5733\u5e02";
        this.Pullfrequency = 5;
        this.Bannerdatanum = [];
        this.clicknum = 0;
        this.ExportOpen = 1;
        this.BeginPower = 5;
        this.ADAddPower = 5;
        this.MaxPower = 5;
        this.CustomCloseBanner = 0;
        this.BoxChance = 100;
      }
      UmSdk.prototype.init = function() {
        if (SDKManager_1.SDKManager.CheckAdType(EnumDef_1.AdType.UMWeChat)) {
          console.log("\u5fae\u4fe1\u6d41\u7a0b");
          this.UmSdk = umsdk.default;
          this.UmSdk.init({
            gameId: 10064
          });
          this._getGameValue();
          ExportPageController_1.exportPageController.refreshAllPage(this.UmSdk);
        }
      };
      UmSdk.prototype.getOnLineGameValue = function(getNum) {
        switch (getNum) {
         case 1:
          return this.DelayShowbanner;

         case 2:
          return this.BannerErrorDelay;

         case 3:
          return this.BannerDelayTime;

         case 4:
          return this.DelayShowMin;

         case 5:
          return this.DelayShowMax;

         case 6:
          return this.LuckBagGap;

         case 7:
          return this.LuckBagOpen;

         case 8:
          return this.ShowVideoIcon;

         case 9:
          return this.ReturnControl;

         case 10:
          return this.GetHeartvalue;

         case 11:
          return this.MissClickStatus;

         case 12:
          return this.LuckBag;

         case 13:
          return this.LuckState;

         case 14:
          return this.Jackpot;

         case 15:
          return this.ExportStatus;

         case 16:
          return this.Pullfrequency;

         case 17:
          return this.Bannerdatanum;

         case 18:
          return this.clicknum;

         case 19:
          return this.invideoopen;

         case 20:
          return this.OldUserExportStatus;

         case 21:
          return this.firsterroronoff;

         case 22:
          return this.CustomCloseBanner;

         default:
          return this.OnLineGameValueList;
        }
      };
      UmSdk.prototype._getGameValue = function() {
        var _this = this;
        if (!this.UmSdk) return;
        this.UmSdk.fetchConfig().then(function(config) {
          console.log("fetchConfig(): ", config);
          _this.OnLineGameValueList = config;
          _this.ReturnControl = Number(config.ReturnControl) || 0;
          _this.DelayShowbanner = Number(config.DelayShowbanner);
          _this.BannerErrorDelay = Number(config.BannerErrorDelay) || Number(config.bannerErrorDelay);
          _this.BannerErrorDelay = _this.BannerErrorDelay ? _this.BannerErrorDelay : 0;
          _this.BannerDelayTime = Number(config.BannerDelayTime);
          _this.BannerDelayTime = _this.BannerDelayTime ? _this.BannerDelayTime : 0;
          _this.DelayShowMin = Number(config.DelayShowMin);
          _this.DelayShowMin = _this.DelayShowMin ? _this.DelayShowMin : 0;
          _this.DelayShowMax = Number(config.DelayShowMax);
          _this.DelayShowMax = _this.DelayShowMax ? _this.DelayShowMax : 0;
          _this.LuckBagGap = Number(config.LuckBagGap);
          _this.LuckBagOpen = Number(config.LuckBagOpen);
          _this.firsterroronoff = Number(config.firsterroronoff);
          _this.OldUserExportStatus = Number(config.OldUserExportStatus);
          _this.invideoopen = Number(config.invideoopen);
          _this.ShowVideoIcon = Number(config.ShowVideoIcon);
          _this.ExportStatus = Number(config.ExportStatus);
          _this.Pullfrequency = Number(config.pullfrequency);
          _this.Bannerdatanum = config.bannerdatanum.split(",");
          _this.clicknum = Number(config.clicknum);
          _this.clicknum = _this.clicknum ? _this.clicknum : 0;
          _this.ExportOpen = Number(config.ExportOpen);
          _this.BeginPower = Number(config.BeginPower);
          _this.ADAddPower = Number(config.ADAddPower);
          _this.MaxPower = Number(config.MaxPower);
          _this.CustomCloseBanner = Number(config.CustomCloseBanner);
          _this.BoxChance = Number(config.BoxChance);
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
          Platform_1.platform.initWxPlatfrom();
          if (0 == GameData_1.GameData.UserData.SdkChange) {
            GameData_1.GameData.UserData.SdkChange = 1;
            GameData_1.GameData.UserData.Power = _this.BeginPower;
            UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
            GameData_1.GameData.UserData.SaveData();
          }
          _this.ErrorCity = config.errorcity;
          _this.MissClickStatus = Number(config.DelayShowbanner);
          _this.UmSdk.fetchLocation().then(function(Location) {
            console.log("fetchLocation", Location);
            if (Location && Location.province) {
              _this.MissClickStatus = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.MissClickStatus : 0;
              _this.ReturnControl = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.ReturnControl : 0;
              _this.ShowVideoIcon = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.ShowVideoIcon : 0;
              _this.LuckBagOpen = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.LuckBagOpen : 0;
              _this.invideoopen = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.invideoopen : 0;
              _this.OldUserExportStatus = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.OldUserExportStatus : 0;
              _this.firsterroronoff = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.firsterroronoff : 0;
              _this.DelayShowbanner = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.DelayShowbanner : 0;
            }
            if (Location && Location.city) {
              _this.MissClickStatus = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.MissClickStatus : 0;
              _this.ReturnControl = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.ReturnControl : 0;
              _this.ShowVideoIcon = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.ShowVideoIcon : 0;
              _this.LuckBagOpen = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.LuckBagOpen : 0;
              _this.invideoopen = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.invideoopen : 0;
              _this.OldUserExportStatus = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.OldUserExportStatus : 0;
              _this.firsterroronoff = -1 === _this.ErrorCity.indexOf(Location.city) ? _this.firsterroronoff : 0;
              _this.DelayShowbanner = -1 === _this.ErrorCity.indexOf(Location.province) ? _this.DelayShowbanner : 0;
            }
          });
          _this._shieldingSceneValue();
        });
      };
      UmSdk.prototype._shieldingSceneValue = function() {
        var str = "1005,1006,1007,1008,1011,1012,1013,1014,1017,1020,1023,1024,1025,1027,1030,1031,1032,1036,1042,1044,1047,1048,1049,1053,1089,1102,1104,1106,1129";
        var keyArr = str.split(",");
        var res = Platform_1.platform.getLaunchOptions();
        if (res && -1 != keyArr.indexOf(res.scene.toString())) {
          this.MissClickStatus = 0;
          this.ReturnControl = 0;
          this.ShowVideoIcon = 0;
          this.LuckBagOpen = 0;
          this.invideoopen = 0;
          this.OldUserExportStatus = 0;
          this.firsterroronoff = 0;
        }
      };
      UmSdk.prototype.ReportAnalytics = function(eventName, data) {
        var _a;
        if (this.UmSdk) {
          var str = eventName;
          for (var key in data) if (Object.prototype.hasOwnProperty.call(data, key)) {
            var element = data[key];
            str += "_" + element;
          }
          null === (_a = this.UmSdk) || void 0 === _a ? void 0 : _a.event("event", str);
        }
      };
      return UmSdk;
    }();
    exports.umSdk = new UmSdk();
    cc._RF.pop();
  }, {
    "../../scripts/Core/UIManager": "UIManager",
    "../../scripts/SDK/SDKManager": "SDKManager",
    "../../scripts/modules/EnumDef": "EnumDef",
    "../../scripts/modules/data/GameData": "GameData",
    "./ExportPageController": "ExportPageController",
    "./Platform": "Platform",
    "./umsdk.wechat.js": "umsdk.wechat"
  } ],
  User: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "47624wAK2BLA50iJJ1xqr4R", "User");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.user = void 0;
    var User = function() {
      function User() {
        this.powerIncreaseHistoricalTime = new Date().getTime();
        this._isInit = false;
        this.timer = null;
        this.info = {
          openId: "",
          uuid: "",
          money: 0,
          levelData: [],
          isMute: false,
          isShock: true,
          gameLevel: 0
        };
      }
      User.prototype.init = function() {
        if (this._isInit) return;
        this._isInit = true;
        this.load();
      };
      User.prototype.save = function() {
        console.log("save()");
        cc.sys.localStorage.setItem("user_data_wininthinking2", JSON.stringify(this.info));
      };
      User.prototype.load = function() {
        var data = cc.sys.localStorage.getItem("user_data_wininthinking2");
        if (data) {
          data = JSON.parse(data);
          for (var key in data) this.info[key] = data[key];
        }
      };
      User.prototype.readySave = function() {
        var _this = this;
        console.log("readySave()");
        clearTimeout(this.timer);
        this.timer = setTimeout(function() {
          _this.save();
        }, 1);
      };
      User.prototype.setMoney = function(money) {
        this.info.money = money;
        this.readySave();
      };
      User.prototype.getMoney = function() {
        return this.info.money || 0;
      };
      User.prototype.getLevelsData = function() {
        return this.info.levelData;
      };
      User.prototype.setLevelsData = function(levelData) {
        this.info.levelData = levelData;
        this.readySave();
      };
      User.prototype.getStar = function() {
        var userStar = 0;
        this.info.levelData.forEach(function(data) {
          userStar += data.star;
        });
        return userStar;
      };
      User.prototype.getIsMute = function() {
        return this.info.isMute;
      };
      User.prototype.setIsMute = function(isMute) {
        this.info.isMute = isMute;
        this.readySave();
      };
      User.prototype.getIsShock = function() {
        return this.info.isShock;
      };
      User.prototype.setIsShock = function(isShock) {
        this.info.isShock = isShock;
        this.readySave();
      };
      User.prototype.getGameLevel = function() {
        return this.info.gameLevel;
      };
      User.prototype.setGameLevel = function(level) {
        this.info.gameLevel = level;
      };
      User.prototype.getUUid = function() {
        if ("" == this.info.uuid) {
          this.info.uuid = this._getUUid();
          this.readySave();
          return this.info.uuid;
        }
        return this.info.uuid;
      };
      User.prototype._getUUid = function() {
        var len = 32;
        var chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678";
        var maxPos = chars.length;
        var pwd = "";
        for (var i = 0; i < len; i++) pwd += chars.charAt(Math.floor(Math.random() * maxPos));
        return pwd;
      };
      return User;
    }();
    exports.user = new User();
    cc._RF.pop();
  }, {} ],
  Utils: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9dbcdiC8WVAbY69HZ6TyXSH", "Utils");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.utils = void 0;
    var Utils = function() {
      function Utils() {}
      Utils.prototype.randomArr = function(oldArr) {
        var _a;
        var newArr = new Array();
        oldArr.forEach(function(element) {
          newArr.push(element);
        });
        var i = newArr.length;
        while (i) {
          var j = Math.floor(Math.random() * i--);
          _a = [ newArr[i], newArr[j] ], newArr[j] = _a[0], newArr[i] = _a[1];
        }
        return newArr;
      };
      Utils.prototype.setIcon = function(path, sprite) {
        cc.loader.loadRes(path, cc.SpriteFrame, function(err, res) {
          if (err) return;
          sprite.spriteFrame = res;
        });
      };
      Utils.prototype.getStrACAList = function(_str) {
        var temp = _str.split("|");
        return temp;
      };
      Utils.prototype.setColor = function(c1, c2, ratio) {
        ratio = Math.max(Math.min(Number(ratio), 1), 0);
        var r1 = parseInt(c1.substring(1, 3), 16);
        var g1 = parseInt(c1.substring(3, 5), 16);
        var b1 = parseInt(c1.substring(5, 7), 16);
        var r2 = parseInt(c2.substring(1, 3), 16);
        var g2 = parseInt(c2.substring(3, 5), 16);
        var b2 = parseInt(c2.substring(5, 7), 16);
        var r = Math.round(r1 * ratio + r2 * (1 - ratio));
        var g = Math.round(g1 * ratio + g2 * (1 - ratio));
        var b = Math.round(b1 * ratio + b2 * (1 - ratio));
        r = ("0" + (r || 0).toString(16)).slice(-2);
        g = ("0" + (g || 0).toString(16)).slice(-2);
        b = ("0" + (b || 0).toString(16)).slice(-2);
        return "#" + r + g + b;
      };
      return Utils;
    }();
    exports.utils = new Utils();
    cc._RF.pop();
  }, {} ],
  Util: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "fff30qxoIlIj5vQSkjk7aDP", "Util");
    "use strict";
    var __awaiter = this && this.__awaiter || function(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function(resolve) {
          resolve(value);
        });
      }
      return new (P || (P = Promise))(function(resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }
        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }
        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    };
    var __generator = this && this.__generator || function(thisArg, body) {
      var _ = {
        label: 0,
        sent: function() {
          if (1 & t[0]) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      }, f, y, t, g;
      return g = {
        next: verb(0),
        throw: verb(1),
        return: verb(2)
      }, "function" === typeof Symbol && (g[Symbol.iterator] = function() {
        return this;
      }), g;
      function verb(n) {
        return function(v) {
          return step([ n, v ]);
        };
      }
      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
          if (f = 1, y && (t = 2 & op[0] ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 
          0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          (y = 0, t) && (op = [ 2 & op[0], t.value ]);
          switch (op[0]) {
           case 0:
           case 1:
            t = op;
            break;

           case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

           case 5:
            _.label++;
            y = op[1];
            op = [ 0 ];
            continue;

           case 7:
            op = _.ops.pop();
            _.trys.pop();
            continue;

           default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (6 === op[0] || 2 === op[0])) {
              _ = 0;
              continue;
            }
            if (3 === op[0] && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }
            if (6 === op[0] && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }
            if (t && _.label < t[2]) {
              _.label = t[2];
              _.ops.push(op);
              break;
            }
            t[2] && _.ops.pop();
            _.trys.pop();
            continue;
          }
          op = body.call(thisArg, _);
        } catch (e) {
          op = [ 6, e ];
          y = 0;
        } finally {
          f = t = 0;
        }
        if (5 & op[0]) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.Util = void 0;
    var EnumDef_1 = require("../modules/EnumDef");
    var _Util = function() {
      function _Util() {
        this.Audio = new _AudioUtil();
        this.File = new _FileUtil();
        this.LZW = new _LZW();
        this.Random = new _RandomUtil();
        this.Res = new _ResUtil();
        this.Save = new _SaveUtil();
        this.Scene = new _SceneUtil();
      }
      _Util.prototype.getUUid = function() {
        var len = 32;
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
        var maxPos = chars.length;
        var pwd = "";
        for (var i = 0; i < len; i++) pwd += chars.charAt(Math.floor(Math.random() * maxPos));
        return pwd;
      };
      return _Util;
    }();
    var _AudioUtil = function() {
      function _AudioUtil() {}
      _AudioUtil.prototype.PlayBGM = function(name, boolLoop) {
        void 0 === boolLoop && (boolLoop = true);
        return __awaiter(this, void 0, void 0, function() {
          var audio;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (name == EnumDef_1.AudioType.None) return [ 2 ];
              return [ 4, exports.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.audio, name) ];

             case 1:
              audio = _a.sent();
              cc.audioEngine.playMusic(audio, boolLoop);
              return [ 2 ];
            }
          });
        });
      };
      _AudioUtil.prototype.PlayEffect = function(name, loop) {
        void 0 === loop && (loop = false);
        return __awaiter(this, void 0, Promise, function() {
          var audio;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (name == EnumDef_1.AudioType.None) return [ 2 ];
              return [ 4, exports.Util.Res.LoadAssetRes(EnumDef_1.AssetBundleEnum.audio, name) ];

             case 1:
              audio = _a.sent();
              return [ 2, cc.audioEngine.playEffect(audio, loop) ];
            }
          });
        });
      };
      _AudioUtil.prototype.StopBGM = function() {
        cc.audioEngine.stopMusic();
      };
      _AudioUtil.prototype.StopEffect = function(id) {
        cc.audioEngine.stopEffect(id);
      };
      return _AudioUtil;
    }();
    var _FileUtil = function() {
      function _FileUtil() {}
      _FileUtil.prototype.WriteFile = function(fileName, content, cb) {
        console.log("export");
        try {
          console.log("data: ", content);
          var link = document.createElement("a");
          link.download = fileName;
          link.style.display = "none";
          var blob = new Blob([ content ]);
          link.href = URL.createObjectURL(blob);
          link.click();
          cb && cb();
        } catch (err) {
          console.error("exportData() \u5bfc\u5165\u6570\u636e\u5f02\u5e38");
        }
      };
      _FileUtil.prototype.ReadFile = function(cb) {
        try {
          var input_1 = document.createElement("input");
          input_1.type = "file";
          input_1.click();
          input_1.onchange = function() {
            var file = input_1.files[0];
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function(env) {
              var result = env.target.result;
              var data = JSON.parse(result);
              console.log("input data: ", data);
              cb(data);
            };
          };
        } catch (err) {
          console.error("importData() \u5bfc\u51fa\u6570\u636e\u5f02\u5e38");
        }
      };
      return _FileUtil;
    }();
    var Binary = function() {
      function Binary(initData, p, l, bl) {
        void 0 === initData && (initData = null);
        void 0 === p && (p = null);
        void 0 === l && (l = null);
        void 0 === bl && (bl = null);
        this.Data = initData && initData.constructor == Array ? initData.slice() : [];
        this.p = 0 | p;
        this.l = 0 | l;
        this.bl = Math.max(0 | (bl || 8), 1);
        this.mask = this.m(bl);
        this._m = 4294967295;
      }
      Binary.prototype.data = function(index, value) {
        isNaN(value) || (this.Data[index] = 0 | value || 0);
        return isNaN(index) ? this.Data.slice() : this.Data[index];
      };
      Binary.prototype.read = function() {
        var re;
        if (this.p >= this.l) return 0;
        re = 32 - this.p % 32 < this.bl ? ((this.Data[this.p >> 5] & this.m(32 - this.p % 32)) << (this.p + this.bl) % 32 | this.Data[1 + (this.p >> 5)] >>> 32 - (this.p + this.bl) % 32) & this.mask : this.Data[this.p >> 5] >>> 32 - (this.p + this.bl) % 32 & this.mask;
        this.p += this.bl;
        return re;
      };
      Binary.prototype.write = function(i) {
        i &= this.mask;
        if (32 - this.l % 32 < this.bl) {
          this.Data[this.l >> 5] |= i >>> this.bl - (32 - this.l % 32);
          this.Data[1 + (this.l >> 5)] |= i << 32 - (this.l + this.bl) % 32 & this._m;
        } else this.Data[this.l >> 5] |= i << 32 - (this.l + this.bl) % 32 & this._m;
        this.l += this.bl;
      };
      Binary.prototype.eof = function() {
        return this.p >= this.l;
      };
      Binary.prototype.reset = function() {
        this.p = 0;
        this.mask = this.m(this.bl);
      };
      Binary.prototype.resetAll = function() {
        this.Data = [];
        this.p = 0;
        this.l = 0;
        this.bl = 8;
        this.mask = this.m(this.bl);
        this._m = 4294967295;
      };
      Binary.prototype.setBitLength = function(len) {
        this.bl = Math.max(0 | len, 1);
        this.mask = this.m(this.bl);
      };
      Binary.prototype.toHexString = function() {
        var re = [];
        for (var i = 0; i < this.Data.length; i++) this.Data[i] < 0 ? re.push(this.pad((this.Data[i] >>> 16).toString(16), 4) + this.pad((65535 & this.Data[i]).toString(16), 4)) : re.push(this.pad(this.Data[i].toString(16), 8));
        return re.join("");
      };
      Binary.prototype.toBinaryString = function() {
        var re = [];
        for (var i = 0; i < this.Data.length; i++) this.Data[i] < 0 ? re.push(this.pad((this.Data[i] >>> 1).toString(2), 31) + (1 & this.Data[i])) : re.push(this.pad(this.Data[i].toString(2), 32));
        return re.join("").substring(0, this.l);
      };
      Binary.prototype.toCString = function() {
        var _p = this.p, _bl = this.bl, re = [];
        this.setBitLength(13);
        this.reset();
        while (this.p < this.l) re.push(this.C(this.read()));
        this.setBitLength(_bl);
        this.p = _p;
        return this.C(this.l >>> 13) + this.C(this.l & this.m(13)) + re.join("");
      };
      Binary.prototype.fromCString = function(str) {
        this.resetAll();
        this.setBitLength(13);
        for (var i = 2; i < str.length; i++) this.write(this.D(str, i));
        this.l = this.D(str, 0) << 13 | this.D(str, 1) & this.m(13);
        return this;
      };
      Binary.prototype.clone = function() {
        return new Binary(this.Data, this.p, this.l, this.bl);
      };
      Binary.prototype.m = function(len) {
        return (1 << len) - 1;
      };
      Binary.prototype.pad = function(s, len) {
        return new Array(len + 1).join("0").substring(s.length) + s;
      };
      Binary.prototype.C = function(i) {
        return String.fromCharCode(i + 19968);
      };
      Binary.prototype.D = function(s, i) {
        return s.charCodeAt(i) - 19968;
      };
      return Binary;
    }();
    var _LZW = function() {
      function _LZW() {}
      _LZW.prototype.Compress = function(str) {
        var b = new Binary();
        var code_index = -1;
        var char_len = 8;
        var str = str.replace(/[\u0100-\uFFFF]/g, function(s) {
          return "&#u" + pad(s.charCodeAt(0).toString(16), 4) + ";";
        });
        var dic = {}, cp = [], cpi, bl = 8;
        b.setBitLength(bl);
        for (var i = 0; i < 2 + (1 << char_len); i++) dic[i] = ++code_index;
        cp[0] = str.charCodeAt(0);
        for (var i = 1; i < str.length; i++) {
          cp[1] = str.charCodeAt(i);
          cpi = cp[0] << 16 | cp[1];
          if (void 0 == dic[cpi]) {
            dic[cpi] = ++code_index;
            if (cp[0] > m(bl)) {
              b.write(128);
              b.setBitLength(++bl);
            }
            b.write(cp[0]);
            cp[0] = cp[1];
          } else cp[0] = dic[cpi];
        }
        b.write(cp[0]);
        function pad(s, len) {
          return new Array(len + 1).join("0").substring(s.length) + s;
        }
        function m(len) {
          return (1 << len) - 1;
        }
        return b.toCString();
      };
      _LZW.prototype.Decompress = function(str) {
        var b = new Binary();
        b.fromCString(str);
        b.reset();
        var result = [], dic_code = -1;
        var dic = {}, cp = [], bl = 8;
        for (var i = 0; i < 2 + (1 << bl); i++) dic[i] = String.fromCharCode(++dic_code);
        b.setBitLength(bl);
        cp[0] = b.read();
        while (!b.eof()) {
          cp[1] = b.read();
          if (128 == cp[1]) {
            b.setBitLength(++bl);
            cp[1] = b.read();
          }
          void 0 == dic[cp[1]] ? dic[++dic_code] = dic[cp[0]] + dic[cp[0]].charAt(0) : dic[++dic_code] = dic[cp[0]] + dic[cp[1]].charAt(0);
          result.push(dic[cp[0]]);
          cp[0] = cp[1];
        }
        result.push(dic[cp[0]]);
        return result.join("").replace(/\&\#u[0-9a-fA-F]{4};/g, function(w) {
          return String.fromCharCode(parseInt(w.substring(3, 7), 16));
        });
      };
      return _LZW;
    }();
    var _RandomUtil = function() {
      function _RandomUtil() {}
      _RandomUtil.prototype.getRandom = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      };
      _RandomUtil.prototype.ArrayBreakOrder = function(arr) {
        var _a;
        for (var i = 1; i < arr.length; i++) {
          var random = Math.floor(Math.random() * (i + 1));
          _a = [ arr[random], arr[i] ], arr[i] = _a[0], arr[random] = _a[1];
        }
      };
      return _RandomUtil;
    }();
    var _ResUtil = function() {
      function _ResUtil() {
        this.ResData = [];
        this.ABData = [];
        this.RemoteData = [];
      }
      _ResUtil.prototype.Release = function(asset) {
        null != asset && cc.assetManager.releaseAsset(asset);
      };
      _ResUtil.prototype.loadRemoteRes = function(path) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve, reject) {
                return __awaiter(_this, void 0, void 0, function() {
                  var x, element;
                  var _this = this;
                  return __generator(this, function(_a) {
                    for (x = 0; x < this.RemoteData.length; x++) {
                      element = this.RemoteData[x];
                      path == element.url && resolve(element.asset);
                    }
                    cc.assetManager.loadRemote(path, function(err, res) {
                      if (err) {
                        console.log("loadRemoteRes fail", err, path);
                        reject(null);
                        return;
                      }
                      _this.RemoteData.push({
                        url: path,
                        asset: res
                      });
                      resolve(res);
                    });
                    return [ 2 ];
                  });
                });
              }) ];

             case 1:
              return [ 2, _a.sent() ];
            }
          });
        });
      };
      _ResUtil.prototype.LoadAtlas = function(sprite, picName) {
        return __awaiter(this, void 0, void 0, function() {
          var atlas, asset, asset;
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              console.log("PicName", picName);
              if (!(null != this.ResData[EnumDef_1.AssetBundleEnum.atlas])) return [ 3, 1 ];
              atlas = this.ResData[EnumDef_1.AssetBundleEnum.atlas];
              sprite.spriteFrame = atlas.getSpriteFrame(picName);
              return [ 3, 5 ];

             case 1:
              if (!!cc.sys.isBrowser) return [ 3, 3 ];
              return [ 4, this.LoadAssetBundle(EnumDef_1.AssetBundleEnum.atlas) ];

             case 2:
              asset = _a.sent();
              asset.load(EnumDef_1.AssetBundleEnum.atlas, function(err, res) {
                if (null != err) {
                  console.log("LoadAssetRes", EnumDef_1.AssetBundleEnum.atlas, "" + picName, "Fail", err);
                  return;
                }
                _this.ResData[EnumDef_1.AssetBundleEnum.atlas] = res;
                sprite.spriteFrame = res.getSpriteFrame(picName);
              });
              return [ 3, 5 ];

             case 3:
              return [ 4, this.LoadAssetBundle(EnumDef_1.AssetBundleEnum.atlas) ];

             case 4:
              asset = _a.sent();
              asset.load("" + picName, function(err, res) {
                if (null != err) {
                  console.log("LoadAssetRes", EnumDef_1.AssetBundleEnum.atlas, "" + picName, "Fail", err);
                  return;
                }
                sprite.spriteFrame = new cc.SpriteFrame(res);
              });
              _a.label = 5;

             case 5:
              return [ 2 ];
            }
          });
        });
      };
      _ResUtil.prototype.LoadAssetRes = function(BundleName, path) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              return [ 4, new Promise(function(resolve, reject) {
                return __awaiter(_this, void 0, void 0, function() {
                  var asset;
                  var _this = this;
                  return __generator(this, function(_a) {
                    switch (_a.label) {
                     case 0:
                      null == this.ResData[BundleName] && (this.ResData[BundleName] = []);
                      if (!(null != this.ResData[BundleName][path])) return [ 3, 1 ];
                      resolve(this.ResData[BundleName][path]);
                      return [ 3, 3 ];

                     case 1:
                      this.ResData[BundleName] = [];
                      return [ 4, this.LoadAssetBundle(BundleName) ];

                     case 2:
                      asset = _a.sent();
                      asset.load(path, function(err, res) {
                        if (null != err) {
                          console.log("LoadAssetRes", BundleName, path, "Fail", err);
                          resolve(null);
                          return;
                        }
                        _this.ResData[BundleName][path] = res;
                        resolve(res);
                      });
                      _a.label = 3;

                     case 3:
                      return [ 2 ];
                    }
                  });
                });
              }) ];

             case 1:
              return [ 2, _a.sent() ];
            }
          });
        });
      };
      _ResUtil.prototype.ReleaseAssetRes = function(BundleName, path) {
        return __awaiter(this, void 0, void 0, function() {
          var ab;
          return __generator(this, function(_a) {
            switch (_a.label) {
             case 0:
              if (null == this.ResData[BundleName]) return [ 2 ];
              if (null == this.ResData[BundleName][path]) return [ 2 ];
              return [ 4, this.LoadAssetBundle(BundleName) ];

             case 1:
              ab = _a.sent();
              this.ResData[BundleName][path] = null;
              ab.release(path);
              return [ 2 ];
            }
          });
        });
      };
      _ResUtil.prototype.LoadAssetBundle = function(BundleName) {
        return __awaiter(this, void 0, Promise, function() {
          var _this = this;
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              null != _this.ABData[BundleName] ? resolve(_this.ABData[BundleName]) : cc.assetManager.loadBundle(BundleName, function(err, bundle) {
                if (err) {
                  console.log("LoadAssetBundle", BundleName, "Fail", err);
                  reject(null);
                  return;
                }
                _this.ABData[BundleName] = bundle;
                resolve(bundle);
              });
            }) ];
          });
        });
      };
      return _ResUtil;
    }();
    var _SaveUtil = function() {
      function _SaveUtil() {}
      _SaveUtil.prototype.SaveData = function(key, data) {
        cc.sys.localStorage.setItem(key, JSON.stringify(data));
      };
      _SaveUtil.prototype.GetData = function(key) {
        return JSON.parse(cc.sys.localStorage.getItem(key) || "{}");
      };
      return _SaveUtil;
    }();
    var _SceneUtil = function() {
      function _SceneUtil() {}
      _SceneUtil.prototype.LoadScene = function(name) {
        return __awaiter(this, void 0, Promise, function() {
          return __generator(this, function(_a) {
            return [ 2, new Promise(function(resolve, reject) {
              cc.director.loadScene(name, function() {
                resolve();
              });
            }) ];
          });
        });
      };
      return _SceneUtil;
    }();
    exports.Util = new _Util();
    cc._RF.pop();
  }, {
    "../modules/EnumDef": "EnumDef"
  } ],
  Vibrate: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9d88669AwFCiYaeoURKAkMM", "Vibrate");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.TTPhysical = exports.NonePhysical = exports.Physical = void 0;
    var Physical = function() {
      function Physical() {}
      return Physical;
    }();
    exports.Physical = Physical;
    var NonePhysical = function() {
      function NonePhysical() {}
      NonePhysical.prototype.VibrateLong = function() {};
      NonePhysical.prototype.VibrateShort = function() {};
      return NonePhysical;
    }();
    exports.NonePhysical = NonePhysical;
    var TTPhysical = function() {
      function TTPhysical() {}
      TTPhysical.prototype.VibrateShort = function() {};
      TTPhysical.prototype.VibrateLong = function() {};
      return TTPhysical;
    }();
    exports.TTPhysical = TTPhysical;
    cc._RF.pop();
  }, {} ],
  WashTank: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "405a6PuNBJKIITruaZXfTE/", "WashTank");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var WashTank = function(_super) {
      __extends(WashTank, _super);
      function WashTank() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nowType = BasePipe_1.Liquid.Empty;
        _this.water = void 0;
        _this.poison = void 0;
        _this.health = void 0;
        _this.fire = void 0;
        _this.ice = void 0;
        _this._liquid = BasePipe_1.Liquid.Empty;
        return _this;
      }
      WashTank.prototype.start = function() {
        "water" == this.node.name && (this.nowType = BasePipe_1.Liquid.Water);
        "poison" == this.node.name && (this.nowType = BasePipe_1.Liquid.Poison);
        "health" == this.node.name && (this.nowType = BasePipe_1.Liquid.Health);
        "fire" == this.node.name && (this.nowType = BasePipe_1.Liquid.Fire);
        "ice" == this.node.name && (this.nowType = BasePipe_1.Liquid.Ice);
        this.Liquid = this.nowType;
        this.water.active = this.Liquid == BasePipe_1.Liquid.Water;
        this.poison.active = this.Liquid == BasePipe_1.Liquid.Poison;
        this.health.active = this.Liquid == BasePipe_1.Liquid.Health;
        this.fire.active = this.Liquid == BasePipe_1.Liquid.Fire;
        this.ice.active = this.Liquid == BasePipe_1.Liquid.Ice;
      };
      WashTank.prototype.AddJoint = function(selfJoint) {
        null != this._From && this._liquid != BasePipe_1.Liquid.Empty && this.Output();
      };
      WashTank.prototype.Input = function(liquid, joint) {
        var _this = this;
        this._From = joint;
        this._liquid = liquid;
        liquid == BasePipe_1.Liquid.Empty ? this.Joint.forEach(function(e) {
          e != _this._From && e.Output(BasePipe_1.Liquid.Empty);
        }) : this.Filling();
      };
      WashTank.prototype.Output = function() {
        var _this = this;
        this.Joint.forEach(function(e) {
          e != _this._From && (null == _this._From ? e.Output(BasePipe_1.Liquid.Empty) : e.Output(_this.Liquid));
        });
      };
      WashTank.prototype.Filling = function() {
        this.Output();
      };
      WashTank.prototype.RemoveJoint = function(joint) {
        if (joint == this._From) {
          this._From = null;
          this.Filling();
        }
      };
      __decorate([ property({
        type: cc.Enum(BasePipe_1.Liquid)
      }) ], WashTank.prototype, "nowType", void 0);
      __decorate([ property(cc.Node) ], WashTank.prototype, "water", void 0);
      __decorate([ property(cc.Node) ], WashTank.prototype, "poison", void 0);
      __decorate([ property(cc.Node) ], WashTank.prototype, "health", void 0);
      __decorate([ property(cc.Node) ], WashTank.prototype, "fire", void 0);
      __decorate([ property(cc.Node) ], WashTank.prototype, "ice", void 0);
      WashTank = __decorate([ ccclass ], WashTank);
      return WashTank;
    }(BasePipe_1.default);
    exports.default = WashTank;
    cc._RF.pop();
  }, {
    "./BasePipe": "BasePipe"
  } ],
  WaterTank: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "eff00ZP+4dGHL0lwWmEMy6K", "WaterTank");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var GameData_1 = require("../data/GameData");
    var BasePipe_1 = require("./BasePipe");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var WaterTank = function(_super) {
      __extends(WaterTank, _super);
      function WaterTank() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nowType = BasePipe_1.Liquid.Empty;
        _this.water = void 0;
        _this.poison = void 0;
        _this.health = void 0;
        _this.fire = void 0;
        _this.ice = void 0;
        return _this;
      }
      WaterTank.prototype.start = function() {
        var _this = this;
        "water" == this.node.name && (this.nowType = BasePipe_1.Liquid.Water);
        "poison" == this.node.name && (this.nowType = BasePipe_1.Liquid.Poison);
        "health" != this.node.name && "power" != this.node.name || (this.nowType = BasePipe_1.Liquid.Health);
        "fire" == this.node.name && (this.nowType = BasePipe_1.Liquid.Fire);
        "ice" == this.node.name && (this.nowType = BasePipe_1.Liquid.Ice);
        this.Liquid = this.nowType;
        this.water.active = this.Liquid == BasePipe_1.Liquid.Water;
        var par = this.water.getComponentInChildren(cc.ParticleSystem);
        par && (par.startColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Water));
        this.poison.active = this.Liquid == BasePipe_1.Liquid.Poison;
        par = this.water.getComponentInChildren(cc.ParticleSystem);
        par && (par.startColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Poison));
        this.health.active = this.Liquid == BasePipe_1.Liquid.Health;
        par = this.water.getComponentInChildren(cc.ParticleSystem);
        par && (par.startColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Health));
        this.fire.active = this.Liquid == BasePipe_1.Liquid.Fire;
        par = this.water.getComponentInChildren(cc.ParticleSystem);
        par && (par.startColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Fire));
        this.ice.active = this.Liquid == BasePipe_1.Liquid.Ice;
        par = this.water.getComponentInChildren(cc.ParticleSystem);
        par && (par.startColor = GameData_1.GameData.GetColorByLiquid(BasePipe_1.Liquid.Ice));
        this.Joint.forEach(function(e) {
          e.Output(_this.Liquid);
        });
      };
      WaterTank.prototype.AddJoint = function(selfJoint) {
        var _this = this;
        this.Joint.forEach(function(e) {
          e.Output(_this.Liquid);
        });
      };
      WaterTank.prototype.Input = function(liquid, from) {};
      WaterTank.prototype.Output = function() {};
      WaterTank.prototype.Filling = function() {};
      WaterTank.prototype.RemoveJoint = function(joint) {
        var _this = this;
        this.Joint.forEach(function(e) {
          e.Output(_this.Liquid);
        });
      };
      __decorate([ property({
        type: cc.Enum(BasePipe_1.Liquid)
      }) ], WaterTank.prototype, "nowType", void 0);
      __decorate([ property(cc.Node) ], WaterTank.prototype, "water", void 0);
      __decorate([ property(cc.Node) ], WaterTank.prototype, "poison", void 0);
      __decorate([ property(cc.Node) ], WaterTank.prototype, "health", void 0);
      __decorate([ property(cc.Node) ], WaterTank.prototype, "fire", void 0);
      __decorate([ property(cc.Node) ], WaterTank.prototype, "ice", void 0);
      WaterTank = __decorate([ ccclass ], WaterTank);
      return WaterTank;
    }(BasePipe_1.default);
    exports.default = WaterTank;
    cc._RF.pop();
  }, {
    "../data/GameData": "GameData",
    "./BasePipe": "BasePipe"
  } ],
  WebPlatform: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "9dffbvwMkRD3qqxsbfqiRKm", "WebPlatform");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var WebPlatform = function() {
      function WebPlatform() {}
      WebPlatform.prototype.init = function(gameData) {};
      WebPlatform.prototype.vibrate = function(isLong) {
        void 0 === isLong && (isLong = false);
      };
      WebPlatform.prototype.showRewardVideo = function(cb) {
        cb(false);
      };
      WebPlatform.prototype.showInsertVideo = function(cb) {};
      WebPlatform.prototype.showBanner = function() {};
      WebPlatform.prototype.hideBanner = function() {};
      WebPlatform.prototype.shareVideo = function(cb) {
        cb(false);
      };
      WebPlatform.prototype.shareMessage = function(cb) {
        cb(false);
      };
      WebPlatform.prototype.startRecord = function(duration) {};
      WebPlatform.prototype.stopRecord = function() {};
      WebPlatform.prototype.pauseRecord = function() {};
      WebPlatform.prototype.resumeRecord = function() {};
      WebPlatform.prototype.showMoreGame = function(style, appids, cb) {};
      WebPlatform.prototype.hideMoreGame = function() {};
      WebPlatform.prototype.showLoading = function(data) {
        void 0 === data && (data = {
          title: " ",
          mask: true
        });
      };
      WebPlatform.prototype.hideLoading = function() {};
      WebPlatform.prototype.onShow = function(cb) {};
      WebPlatform.prototype.onHide = function(cb) {};
      WebPlatform.prototype.startAccelerometer = function(cb) {};
      WebPlatform.prototype.stopAccelerometer = function() {};
      WebPlatform.prototype.report = function(key, data) {};
      WebPlatform.prototype.getSystemInfo = function() {};
      WebPlatform.prototype.getLaunchOptions = function() {};
      return WebPlatform;
    }();
    exports.default = WebPlatform;
    cc._RF.pop();
  }, {} ],
  WifeItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "c45calJLNhDpbEqRF1T1cf4", "WifeItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.WifeItem = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var WifeItem = function(_super) {
      __extends(WifeItem, _super);
      function WifeItem() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeOnUse_arr = [];
        _this.nodeAd_arr = [];
        _this.labelMoney_arr = [];
        _this.nodeMoney_arr = [];
        _this.nodeUse_arr = [];
        _this.nodeRole_arr = [];
        _this.display_arr = [];
        return _this;
      }
      WifeItem.prototype.start = function() {
        var _this = this;
        this.nodeAd_arr.forEach(function(e, x) {
          e.on(cc.Node.EventType.TOUCH_END, function() {
            SDKManager_1.SDKManager.PlayVideo("\u5973\u65f6\u88c5", function() {
              GameData_1.GameData.AddWife(_this._num + x);
              GameData_1.GameData.UseWife(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.WifeView, EnumDef_1.UIInfo.RefreshView);
            });
          });
        });
        this.nodeMoney_arr.forEach(function(e, x) {
          e.on(cc.Node.EventType.TOUCH_END, function() {
            var money = GameData_1.GameData.WifeMoney[_this._num + x];
            if (GameData_1.GameData.UserData.Money >= money) {
              GameData_1.GameData.UserData.Money -= money;
              GameData_1.GameData.AddWife(_this._num + x);
              GameData_1.GameData.UseWife(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.WifeView, EnumDef_1.UIInfo.RefreshView);
            }
          });
          _this.nodeUse_arr.forEach(function(e, x) {
            e.on(cc.Node.EventType.TOUCH_END, function() {
              GameData_1.GameData.UseWife(_this._num + x);
              UIManager_1.UIManager.Notif(EnumDef_1.UIName.WifeView, EnumDef_1.UIInfo.RefreshView);
            });
          });
        });
      };
      WifeItem.prototype.SetData = function(num) {
        this._num = num;
        for (var x = 0; x < 3; x++) this.SetOne(num, x);
      };
      WifeItem.prototype.SetOne = function(num, x) {
        var Wife = GameData_1.GameData.WifeData[num + x];
        if (null != Wife) {
          this.nodeRole_arr[x].active = true;
          this.display_arr[x].playAnimation(Wife + "\u5f85\u673a", 0);
          this.nodeOnUse_arr[x].active = GameData_1.GameData.UserData.NowWife == Wife;
          this.nodeAd_arr[x].active = !GameData_1.GameData.HadWife(Wife) && GameData_1.GameData.WifeMoney[num + x] < 10;
          this.nodeMoney_arr[x].active = !GameData_1.GameData.HadWife(Wife) && GameData_1.GameData.WifeMoney[num + x] > 10;
          this.labelMoney_arr[x].string = "" + GameData_1.GameData.WifeMoney[num + x];
          this.nodeUse_arr[x].active = GameData_1.GameData.HadWife(Wife) && GameData_1.GameData.UserData.NowWife != Wife;
        } else this.nodeRole_arr[x].active = false;
      };
      __decorate([ property(cc.Node) ], WifeItem.prototype, "nodeOnUse_arr", void 0);
      __decorate([ property(cc.Node) ], WifeItem.prototype, "nodeAd_arr", void 0);
      __decorate([ property(cc.Label) ], WifeItem.prototype, "labelMoney_arr", void 0);
      __decorate([ property(cc.Node) ], WifeItem.prototype, "nodeMoney_arr", void 0);
      __decorate([ property(cc.Node) ], WifeItem.prototype, "nodeUse_arr", void 0);
      __decorate([ property(cc.Node) ], WifeItem.prototype, "nodeRole_arr", void 0);
      __decorate([ property(dragonBones.ArmatureDisplay) ], WifeItem.prototype, "display_arr", void 0);
      WifeItem = __decorate([ ccclass ], WifeItem);
      return WifeItem;
    }(cc.Component);
    exports.WifeItem = WifeItem;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  WifeView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6ff07mmLAlOKbJtt1ye9Kdt", "WifeView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.WifeView = void 0;
    var UIManager_1 = require("../../Core/UIManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var WifeItem_1 = require("./WifeItem");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var WifeView = function(_super) {
      __extends(WifeView, _super);
      function WifeView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.btnReturn = void 0;
        _this.nodeLayout = void 0;
        _this.wifeItem = void 0;
        return _this;
      }
      WifeView.prototype.click_btn_return = function(e) {
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.WifeView);
      };
      WifeView.prototype.notif = function(info, msg) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
         case EnumDef_1.UIInfo.RefreshView:
          this.ShowAllFashion();
        }
      };
      WifeView.prototype.ShowAllFashion = function() {
        var num = 0;
        this.nodeLayout.children.forEach(function(e) {
          return e.active = false;
        });
        for (var x = 0; x < GameData_1.GameData.WifeData.length; x += 3) {
          var node = this.nodeLayout.children[num];
          if (null == node) {
            node = cc.instantiate(this.wifeItem);
            node.setParent(this.nodeLayout);
          }
          node.getComponent(WifeItem_1.WifeItem).SetData(num);
          num++;
          node.active = true;
        }
      };
      __decorate([ property(cc.Button) ], WifeView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Node) ], WifeView.prototype, "nodeLayout", void 0);
      __decorate([ property(cc.Prefab) ], WifeView.prototype, "wifeItem", void 0);
      WifeView = __decorate([ ccclass ], WifeView);
      return WifeView;
    }(cc.Component);
    exports.WifeView = WifeView;
    cc._RF.pop();
  }, {
    "../../Core/UIManager": "UIManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData",
    "./WifeItem": "WifeItem"
  } ],
  Win2View: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "3b9cbTUpN9E8ZNlGY77zvow", "Win2View");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.Win2View = void 0;
    var SDKEnum_1 = require("../../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../../global/libs/ExportPageController");
    var Platform_1 = require("../../../global/libs/Platform");
    var UmSdk_1 = require("../../../global/libs/UmSdk");
    var UIManager_1 = require("../../Core/UIManager");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var Win2View = function(_super) {
      __extends(Win2View, _super);
      function Win2View() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.nodeAd_arr = [];
        _this.btnReplay = void 0;
        _this.btnAdNext = void 0;
        _this.nodeChoose = void 0;
        _this.btnChoose = void 0;
        return _this;
      }
      Win2View.prototype.click_btn_replay = function(e) {
        if (this.nodeChoose.active) {
          GameData_1.GameData.UserData.Mission--;
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Win2View);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        } else SDKManager_1.SDKManager.PlayVideo("\u80dc\u5229\u91cd\u73a9", function() {
          GameData_1.GameData.UserData.Mission--;
          GameData_1.GameData.UserData.Power++;
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Win2View);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        });
      };
      Win2View.prototype.click_btn_ad_next = function(e) {
        if (this.nodeChoose.active) {
          1 != GameData_1.GameData.UserData.Mission && GameData_1.GameData.UserData.Mission % 5 == 1 && UIManager_1.UIManager.Notif(EnumDef_1.UIName.RoomDetailView, EnumDef_1.UIInfo.ShowView, true);
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Win2View);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        } else SDKManager_1.SDKManager.PlayVideo("\u80dc\u5229\u4e0b\u4e00\u5173", function() {
          GameData_1.GameData.UserData.Power++;
          1 != GameData_1.GameData.UserData.Mission && GameData_1.GameData.UserData.Mission % 5 == 1 && UIManager_1.UIManager.Notif(EnumDef_1.UIName.RoomDetailView, EnumDef_1.UIInfo.ShowView, true);
          UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.Win2View);
          UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        });
      };
      Win2View.prototype.click_btn_choose = function(e) {
        var _this = this;
        this.nodeChoose.active = !this.nodeChoose.active;
        this.nodeAd_arr.forEach(function(e) {
          return e.active = !_this.nodeChoose.active;
        });
      };
      Win2View.prototype.notif = function(info, data) {
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          this.nodeAd_arr.forEach(function(e) {
            return e.active = true;
          });
          this.nodeChoose.active = false;
          SDKManager_1.SDKManager.ShowCenterCustomAd();
          1 == UmSdk_1.umSdk.CustomCloseBanner && Platform_1.platform.hideBanner();
          break;

         case EnumDef_1.UIInfo.CloseView:
          SDKManager_1.SDKManager.CloseCenterCustomAd();
          1 == UmSdk_1.umSdk.CustomCloseBanner && Platform_1.platform.showBanner();
          ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.MoreGamePage, null);
        }
      };
      __decorate([ property(cc.Node) ], Win2View.prototype, "nodeAd_arr", void 0);
      __decorate([ property(cc.Button) ], Win2View.prototype, "btnReplay", void 0);
      __decorate([ property(cc.Button) ], Win2View.prototype, "btnAdNext", void 0);
      __decorate([ property(cc.Node) ], Win2View.prototype, "nodeChoose", void 0);
      __decorate([ property(cc.Button) ], Win2View.prototype, "btnChoose", void 0);
      Win2View = __decorate([ ccclass ], Win2View);
      return Win2View;
    }(cc.Component);
    exports.Win2View = Win2View;
    cc._RF.pop();
  }, {
    "../../../export/scripts/SDKEnum": "SDKEnum",
    "../../../global/libs/ExportPageController": "ExportPageController",
    "../../../global/libs/Platform": "Platform",
    "../../../global/libs/UmSdk": "UmSdk",
    "../../Core/UIManager": "UIManager",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  WinView: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "78c674niEBCG4m3gpFQJxhf", "WinView");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) Object.prototype.hasOwnProperty.call(b, p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.WinView = void 0;
    var SDKEnum_1 = require("../../../export/scripts/SDKEnum");
    var ExportPageController_1 = require("../../../global/libs/ExportPageController");
    var UmSdk_1 = require("../../../global/libs/UmSdk");
    var UIManager_1 = require("../../Core/UIManager");
    var Util_1 = require("../../Core/Util");
    var SDKManager_1 = require("../../SDK/SDKManager");
    var GameData_1 = require("../data/GameData");
    var EnumDef_1 = require("../EnumDef");
    var property = cc._decorator.property;
    var ccclass = cc._decorator.ccclass;
    var WinView = function(_super) {
      __extends(WinView, _super);
      function WinView() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.spriteMoneyFill = void 0;
        _this.labelFill = void 0;
        _this.labelAddMoney = void 0;
        _this.btnNext = void 0;
        _this.btnReturn = void 0;
        _this.btnAdReward = void 0;
        _this.nodeColor = void 0;
        _this._MustAd = false;
        return _this;
      }
      WinView.prototype.click_btn_ad_reward = function(e) {
        var _this = this;
        console.log("???");
        this._MustAd = false;
        cc.Tween.stopAllByTarget(this.btnAdReward.node);
        SDKManager_1.SDKManager.PlayVideo("\u80dc\u5229\u89c6\u9891", function() {
          GameData_1.GameData.UserData.Money += 4 * GameData_1.GameData.MissionGold;
          GameData_1.GameData.UserData.SaveData();
          _this.labelAddMoney.string = "+" + 5 * GameData_1.GameData.MissionGold;
          _this.btnAdReward.node.active = false;
          UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
        });
      };
      WinView.prototype.click_btn_close = function(e) {
        if (this._MustAd) return;
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.WinView);
        UIManager_1.UIManager.Notif(EnumDef_1.UIName.GameView, EnumDef_1.UIInfo.RefreshView);
        UIManager_1.UIManager.NotifAll(EnumDef_1.UIInfo.RefreshToken);
      };
      WinView.prototype.click_btn_next = function(e) {
        if (this._MustAd) return;
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.WinView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.Win2View);
      };
      WinView.prototype.click_btn_return = function(e) {
        if (this._MustAd) return;
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.WinView);
        UIManager_1.UIManager.CloseUI(EnumDef_1.UIName.GameView);
        UIManager_1.UIManager.OpenUI(EnumDef_1.UIName.BeginView);
      };
      WinView.prototype.notif = function(info, data) {
        var _this = this;
        switch (info) {
         case EnumDef_1.UIInfo.ShowView:
          Util_1.Util.Audio.PlayEffect(EnumDef_1.AudioType.audio_Win);
          GameData_1.GameData.UserData.Money += GameData_1.GameData.MissionGold;
          GameData_1.GameData.UserData.SaveData();
          GameData_1.GameData.UserData.WinPercent += 10;
          this.labelAddMoney.string = "+" + GameData_1.GameData.MissionGold;
          this.spriteMoneyFill.fillRange = GameData_1.GameData.UserData.WinPercent / 100;
          this.labelFill.string = GameData_1.GameData.UserData.WinPercent + "%";
          GameData_1.GameData.UserData.WinPercent >= 100 && (GameData_1.GameData.UserData.WinPercent = 0);
          GameData_1.GameData.UserData.SaveData();
          this.btnAdReward.node.active = true;
          ExportPageController_1.exportPageController.showExport(SDKEnum_1.PageType.RightDrawerPage, null);
          if (1 == UmSdk_1.umSdk.getOnLineGameValue(1)) {
            this._MustAd = true;
            cc.tween(this.btnAdReward.node).delay(3).call(function() {
              _this.click_btn_ad_reward(null);
            }).start();
          }
          GameData_1.GameData.UserData.Mission++;
          break;

         case EnumDef_1.UIInfo.CloseView:
          ExportPageController_1.exportPageController.closeExport(SDKEnum_1.PageType.RightDrawerPage);
        }
      };
      __decorate([ property(cc.Sprite) ], WinView.prototype, "spriteMoneyFill", void 0);
      __decorate([ property(cc.Label) ], WinView.prototype, "labelFill", void 0);
      __decorate([ property(cc.Label) ], WinView.prototype, "labelAddMoney", void 0);
      __decorate([ property(cc.Button) ], WinView.prototype, "btnNext", void 0);
      __decorate([ property(cc.Button) ], WinView.prototype, "btnReturn", void 0);
      __decorate([ property(cc.Button) ], WinView.prototype, "btnAdReward", void 0);
      __decorate([ property(cc.Node) ], WinView.prototype, "nodeColor", void 0);
      WinView = __decorate([ ccclass ], WinView);
      return WinView;
    }(cc.Component);
    exports.WinView = WinView;
    cc._RF.pop();
  }, {
    "../../../export/scripts/SDKEnum": "SDKEnum",
    "../../../global/libs/ExportPageController": "ExportPageController",
    "../../../global/libs/UmSdk": "UmSdk",
    "../../Core/UIManager": "UIManager",
    "../../Core/Util": "Util",
    "../../SDK/SDKManager": "SDKManager",
    "../EnumDef": "EnumDef",
    "../data/GameData": "GameData"
  } ],
  WxPlatform: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "0d58fRS9q9A9aeTAE0HXm7F", "WxPlatform");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var UmSdk_1 = require("../UmSdk");
    var wx = window["wx"];
    var WxPlatfrom = function() {
      function WxPlatfrom() {
        this.rewardVideoId = null;
        this.insertVideoId = null;
        this.bannerId = null;
        this.customAdId = [];
        this.customAd = [];
        this.gridAdId = [];
        this.gridAd = [];
        this.rewardVideo = null;
        this.rewardVideoLoaded = false;
        this.rewardVideoCb = null;
        this.banner = null;
        this.bannerList = [];
        this.bannerShowing = false;
        this.insertVideo = null;
        this.insertVideoLoaded = false;
        this.insertVideoCb = null;
        this.shareData = null;
        this.shareTime = 0;
        this.shareCb = null;
        this.showBannerIndex = 0;
      }
      WxPlatfrom.prototype.init = function(gameData) {
        var _this = this;
        gameData.rewardVideoId && (this.rewardVideoId = gameData.rewardVideoId);
        gameData.insertVideoId && (this.insertVideoId = gameData.insertVideoId);
        gameData.customAdId && (this.customAdId = gameData.customAdId);
        gameData.gridAdId && (this.gridAdId = gameData.gridAdId);
        UmSdk_1.umSdk.getOnLineGameValue(17).length ? this.bannerId = UmSdk_1.umSdk.getOnLineGameValue(17) : gameData.bannerId && (this.bannerId = gameData.bannerId);
        gameData.shareData && (this.shareData = gameData.shareData);
        wx.showShareMenu({
          withShareTicket: true,
          menus: [ "shareAppMessage", "shareTimeline" ]
        });
        wx.onShareAppMessage(function() {
          var data = _this.shareData && _this.shareData.length ? _this.shareData[Math.floor(Math.random() * _this.shareData.length)] : _this.shareData;
          return {
            title: data.title || "",
            imageUrl: data.imageUrl || ""
          };
        });
        wx.onShow(function() {
          if (_this.shareTime && _this.shareCb) {
            var now = Math.floor(new Date().getTime() / 1e3);
            _this.shareCb(now - _this.shareTime < 3, null);
          }
          _this.shareTime = 0;
          _this.shareCb = null;
        });
        wx.onHide(function() {});
        this.bannerId.forEach(function(element) {
          _this.initBanner(element);
        });
        this.initInsertVideo();
        this.initCustomAd(this.customAdId);
        this.initGridAd(this.gridAdId);
      };
      WxPlatfrom.prototype.shareMessage = function(cb) {
        this.shareTime = Math.floor(new Date().getTime() / 1e3);
        this.shareCb = cb;
        var data = this.shareData && this.shareData.length ? this.shareData[Math.floor(Math.random() * this.shareData.length)] : this.shareData;
        wx.shareAppMessage({
          title: data.title || "",
          imageUrl: data.imageUrl || ""
        });
      };
      WxPlatfrom.prototype.showRewardVideo = function(cb) {
        if (!this.rewardVideo) return cb(true, -1);
        this.rewardVideoCb = cb;
        this.rewardVideoLoaded ? this.rewardVideo.show() : this.rewardVideo.load();
      };
      WxPlatfrom.prototype.showInsertVideo = function(cb) {
        var _this = this;
        if (!this.insertVideo) return cb(true, -1);
        this.insertVideoCb = cb;
        this.insertVideoLoaded ? this.insertVideo.show().catch(function(res) {
          console.log("showInsertVideo \u5f02\u5e38\u56de\u8c03", res);
          _this.insertVideoCb(true);
          _this.insertVideoCb = null;
        }) : this.insertVideo.load();
      };
      WxPlatfrom.prototype.showBanner = function() {
        var _this = this;
        var maxBanner = this.bannerList.length;
        if (!maxBanner) return;
        if (this.bannerShowing) return;
        this.bannerShowing = true;
        this.showBannerIndex++;
        this.showBannerIndex >= maxBanner && (this.showBannerIndex = 0);
        this.banner = this.bannerList[this.showBannerIndex];
        console.log("this.showBannerIndex", this.showBannerIndex);
        if (this.banner) {
          this.banner.show(this.banner);
          if (this.bannerId.length <= 1) return;
          var dt = UmSdk_1.umSdk.getOnLineGameValue(16);
          cc.tween(cc.find("Canvas")).delay(dt).call(function() {
            _this.refreshBanner();
          }).start();
        }
      };
      WxPlatfrom.prototype.hideBanner = function() {
        if (!this.banner) return;
        if (!this.bannerShowing) return;
        this.bannerShowing = false;
        if (this.banner) {
          this.banner.hide();
          cc.find("Canvas").stopAllActions();
        }
      };
      WxPlatfrom.prototype.showLoading = function(data) {
        wx.showLoading(data);
      };
      WxPlatfrom.prototype.hideLoading = function() {
        wx.hideLoading();
      };
      WxPlatfrom.prototype.onShow = function(cb) {
        wx.onShow(cb);
      };
      WxPlatfrom.prototype.onHide = function(cb) {
        wx.onHide(cb);
      };
      WxPlatfrom.prototype.vibrate = function(isLong) {
        void 0 === isLong && (isLong = false);
        isLong ? wx.vibrateLong() : wx.vibrateShort();
      };
      WxPlatfrom.prototype.startAccelerometer = function(cb) {
        wx.startAccelerometer();
        wx.onAccelerometerChange(function(res) {
          cb && cb(res.x, res.y, res.z);
        });
      };
      WxPlatfrom.prototype.stopListenAccelerometerChange = function() {
        wx.stopAccelerometer();
      };
      WxPlatfrom.prototype.getSystemInfo = function() {
        try {
          var res = wx.getSystemInfoSync();
          return res;
        } catch (e) {
          console.log("getSystemInfo() \u5f02\u5e38\u56de\u8c03", e);
        }
      };
      WxPlatfrom.prototype.getLaunchOptions = function() {
        try {
          var res = wx.getLaunchOptionsSync();
          return res;
        } catch (e) {
          console.log("getLaunchOptions() \u5f02\u5e38\u56de\u8c03", e);
        }
      };
      WxPlatfrom.prototype.report = function(key, value) {
        void 0 === value && (value = 0);
        wx.reportMonitor(key, value);
      };
      WxPlatfrom.prototype.initRewardVideo = function() {
        var _this = this;
        this.rewardVideo = UmSdk_1.umSdk.UmSdk.createRewardedVideoAd({
          adUnitId: "adunit-f1ca0af9ba02c387",
          slotId: "5597d74899088123"
        });
        this.rewardVideo.onLoad(function() {
          _this.rewardVideoLoaded = true;
          _this.rewardVideoCb && _this.rewardVideo.show();
        });
        this.rewardVideo.onError(function(res) {
          console.log("RewardVideo \u5f02\u5e38\u56de\u8c03", res);
          _this.rewardVideoCb && _this.rewardVideoCb(true, res.errCode);
        });
        this.rewardVideo.onClose(function(res) {
          _this.rewardVideoLoaded = false;
          console.log("res", res);
          if (_this.rewardVideoCb) {
            _this.rewardVideoCb(!res.isEnded);
            _this.rewardVideoCb = null;
          }
        });
      };
      WxPlatfrom.prototype.refreshBanner = function() {
        this.hideBanner();
        this.showBanner();
      };
      WxPlatfrom.prototype.initBanner = function(bannerId) {
        if (!bannerId) return console.log("\u672a\u914d\u7f6e Wx BannerId");
        var banner = UmSdk_1.umSdk.UmSdk.createBannerAd({
          adUnitId: bannerId,
          slotId: "4bfb2e84e7edc7a1",
          adIntervals: 30,
          style: {
            width: this.getSystemInfo().windowWidth,
            height: 150,
            top: 0,
            left: 0
          }
        });
        banner.onError(function(err) {
          console.log("banner \u9519\u8bef", err);
        });
        banner.onLoad(function() {});
        banner.onResize(function(size) {
          var pos = wx.getSystemInfoSync();
          banner.style.top = pos.windowHeight - size.height;
          banner.style.left = (pos.windowWidth - size.width) / 2;
        });
        console.log("init banner:", bannerId);
        this.bannerList.push(banner);
      };
      WxPlatfrom.prototype.initInsertVideo = function() {
        var _this = this;
        if (!this.insertVideoId) return console.log("\u672a\u914d\u7f6e Wx InsertVideo");
        this.insertVideo = wx.createInterstitialAd({
          adUnitId: this.insertVideoId
        });
        this.insertVideo.onLoad(function() {
          _this.insertVideoLoaded = true;
          _this.insertVideoCb && _this.insertVideo.show();
        });
        this.insertVideo.onError(function(res) {
          console.log("InsertVideo \u5f02\u5e38\u56de\u8c03", res);
          _this.insertVideoCb && _this.insertVideoCb(true, res.errCode);
        });
        this.insertVideo.onClose(function() {
          _this.insertVideoLoaded = false;
          if (_this.insertVideoCb) {
            _this.insertVideoCb(false);
            _this.insertVideoCb = null;
          }
        });
      };
      WxPlatfrom.prototype.initCustomAd = function(id) {
        this.customAd = [];
        var version = this.getSystemInfo().SDKVersion;
        version = parseInt(version.replace(/\./g, ""));
        if (version >= 2111) {
          var pos = wx.getSystemInfoSync();
          var _loop_1 = function(i) {
            var tmpTop = 0;
            var tmpLeft = 0;
            switch (i) {
             case 0:
              tmpTop = pos.windowHeight - 106;
              tmpLeft = 7.5;
              break;

             case 1:
              tmpTop = pos.windowHeight - pos.windowHeight / 3.5;
              tmpLeft = pos.windowWidth - pos.windowWidth / 4.6 + 60;
            }
            var temp_ad = wx.createCustomAd({
              adUnitId: id[i],
              adIntervals: 30,
              style: {
                top: tmpTop,
                left: tmpLeft,
                fixed: false
              }
            });
            temp_ad.onError(function(err) {
              console.log("initCustomAd \u9519\u8bef", err, id[i]);
            });
            temp_ad.onLoad(function() {});
            this_1.customAd.push(temp_ad);
          };
          var this_1 = this;
          for (var i = 0; i < id.length; i++) _loop_1(i);
        } else console.warn("\u7528\u6237\u7aefsdk\u7248\u672c\u4e0d\u652f\u6301\u539f\u751f\u5e7f\u544a");
      };
      WxPlatfrom.prototype.initGridAd = function(id) {
        this.gridAd = [];
        var version = this.getSystemInfo().SDKVersion;
        version = parseInt(version.replace(/\./g, ""));
        if (version >= 292) {
          var pos = wx.getSystemInfoSync();
          var _loop_2 = function(i) {
            var tmpTop = 0;
            var tmpLeft = 0;
            switch (i) {
             case 0:
              tmpTop = pos.windowHeight / 2 - 50 - 30;
              tmpLeft = pos.windowWidth / 2 - 125;
            }
            var temp_ad = wx.createGridAd({
              adUnitId: id[i],
              adIntervals: 30,
              adTheme: "white",
              gridCount: 5,
              style: {
                top: tmpTop,
                left: tmpLeft,
                width: 250,
                height: 70
              }
            });
            temp_ad.onError(function(err) {
              console.log("initGridAd \u9519\u8bef", err, id[i]);
            });
            temp_ad.onLoad(function() {});
            this_2.gridAd.push(temp_ad);
          };
          var this_2 = this;
          for (var i = 0; i < id.length; i++) _loop_2(i);
        } else console.warn("\u7528\u6237\u7aefsdk\u7248\u672c\u4e0d\u652f\u6301\u683c\u5b50\u5e7f\u544a");
      };
      WxPlatfrom.prototype.showCustomAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this.customAd.length) return;
        for (var i = 0; i < this.customAd.length; i++) {
          if (-1 == ids.indexOf(i)) continue;
          this.customAd[i] && this.customAd[i].show();
        }
      };
      WxPlatfrom.prototype.hideCustomAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this.customAd.length) return;
        for (var i = 0; i < this.customAd.length; i++) {
          if (-1 == ids.indexOf(i)) continue;
          this.customAd[i] && this.customAd[i].hide();
        }
      };
      WxPlatfrom.prototype.destroyCustomAd = function() {
        if (!this.customAd.length) return;
        for (var i = 0; i < this.customAd.length; i++) this.customAd[i] && this.customAd[i].destroy();
      };
      WxPlatfrom.prototype.showGridAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this.gridAd.length) return;
        for (var i = 0; i < this.gridAd.length; i++) {
          if (-1 == ids.indexOf(i)) continue;
          this.gridAd[i] && this.gridAd[i].show();
        }
      };
      WxPlatfrom.prototype.hideGridAd = function(ids) {
        void 0 === ids && (ids = [ 0 ]);
        if (!this.gridAd.length) return;
        for (var i = 0; i < this.gridAd.length; i++) {
          if (-1 == ids.indexOf(i)) continue;
          this.gridAd[i] && this.gridAd[i].hide();
        }
      };
      WxPlatfrom.prototype.destroyGridAd = function() {
        if (!this.gridAd.length) return;
        for (var i = 0; i < this.gridAd.length; i++) this.gridAd[i] && this.gridAd[i].destroy();
      };
      return WxPlatfrom;
    }();
    exports.default = WxPlatfrom;
    cc._RF.pop();
  }, {
    "../UmSdk": "UmSdk"
  } ],
  "umsdk.toutiao": [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "209a8VuzfhGmYdwZ4fb/K+Q", "umsdk.toutiao");
    "use strict";
    function _createForOfIteratorHelperLoose(o, allowArrayLike) {
      var it;
      if ("undefined" === typeof Symbol || null == o[Symbol.iterator]) {
        if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && "number" === typeof o.length) {
          it && (o = it);
          var i = 0;
          return function() {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          };
        }
        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
      }
      it = o[Symbol.iterator]();
      return it.next.bind(it);
    }
    function _unsupportedIterableToArray(o, minLen) {
      if (!o) return;
      if ("string" === typeof o) return _arrayLikeToArray(o, minLen);
      var n = Object.prototype.toString.call(o).slice(8, -1);
      "Object" === n && o.constructor && (n = o.constructor.name);
      if ("Map" === n || "Set" === n) return Array.from(o);
      if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
    }
    function _arrayLikeToArray(arr, len) {
      (null == len || len > arr.length) && (len = arr.length);
      for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
      return arr2;
    }
    function _inheritsLoose(subClass, superClass) {
      subClass.prototype = Object.create(superClass.prototype);
      subClass.prototype.constructor = subClass;
      subClass.__proto__ = superClass;
    }
    !function(t, e) {
      "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.umsdk = e() : t.umsdk = e();
    }(void 0, function() {
      return function(t) {
        var e = {};
        function i(r) {
          if (e[r]) return e[r].exports;
          var s = e[r] = {
            i: r,
            l: !1,
            exports: {}
          };
          return t[r].call(s.exports, s, s.exports, i), s.l = !0, s.exports;
        }
        return i.m = t, i.c = e, i.d = function(t, e, r) {
          i.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: r
          });
        }, i.r = function(t) {
          "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
          }), Object.defineProperty(t, "__esModule", {
            value: !0
          });
        }, i.t = function(t, e) {
          if (1 & e && (t = i(t)), 8 & e) return t;
          if (4 & e && "object" == typeof t && t && t.__esModule) return t;
          var r = Object.create(null);
          if (i.r(r), Object.defineProperty(r, "default", {
            enumerable: !0,
            value: t
          }), 2 & e && "string" != typeof t) for (var s in t) i.d(r, s, function(e) {
            return t[e];
          }.bind(null, s));
          return r;
        }, i.n = function(t) {
          var e = t && t.__esModule ? function() {
            return t["default"];
          } : function() {
            return t;
          };
          return i.d(e, "a", e), e;
        }, i.o = function(t, e) {
          return Object.prototype.hasOwnProperty.call(t, e);
        }, i.p = "", i(i.s = 0);
      }([ function(t, e, i) {
        i.r(e);
        var r = function() {
          function r(t, e) {
            this.runtime = e, this.data = {
              sid: t.slotId
            }, this.ad = t.createAd(t.params), this.ad.onError(function(t) {
              self.runtime.event("ad", "imp", {
                lbl: self.data.stype,
                val: -2,
                sub5: self.data.sid,
                sub7: t.errMsg,
                subi4: t.errCode
              });
            });
          }
          var _proto = r.prototype;
          _proto.load = function load() {
            return this.ad.load();
          };
          _proto.show = function show() {
            var t = this;
            return this.ad.show().then(function() {
              t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              });
            }, function(e) {
              throw t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -1,
                sub5: t.data.sid,
                sub7: e.errMsg,
                subi4: e.errCode
              }), e;
            });
          };
          _proto.destroy = function destroy() {
            this.ad.destroy();
          };
          _proto.onClose = function onClose(t) {
            this.ad.onClose(t);
          };
          _proto.offClose = function offClose(t) {
            this.ad.offClose(t);
          };
          _proto.onLoad = function onLoad(t) {
            this.ad.onLoad(t);
          };
          _proto.offLoad = function offLoad(t) {
            this.ad.offLoad(t);
          };
          _proto.onError = function onError(t) {
            this.ad.onError(t);
          };
          _proto.offError = function offError(t) {
            this.ad.offError(t);
          };
          return r;
        }();
        var s = function(_r) {
          _inheritsLoose(s, _r);
          function s(t, e) {
            var _this;
            _this = _r.call(this, t, e) || this, _this.data.stype = "banner", _this.showed = !1;
            return _this;
          }
          var _proto2 = s.prototype;
          _proto2.show = function show() {
            var t = this;
            return this.ad.show().then(function() {
              t.showed || (t.showed = !0, t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              }));
            }, function(e) {
              throw t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -1,
                sub5: t.data.sid,
                subi4: e.errCode
              }), e;
            });
          };
          _proto2.hide = function hide() {
            this.ad.hide();
          };
          _proto2.offResize = function offResize(t) {
            this.ad.offResize(t);
          };
          _proto2.onResize = function onResize(t) {
            this.ad.onResize(t);
          };
          return s;
        }(r);
        var a = function(_r2) {
          _inheritsLoose(a, _r2);
          function a(t, e) {
            var _this2;
            _this2 = _r2.call(this, t, e) || this, _this2.data.stype = "video", _this2.adUnitId = t.adUnitId, 
            _this2.onCloseCallbacks = [], _this2.onLoadCallbacks = [], _this2.onErrorCallbacks = [];
            return _this2;
          }
          var _proto3 = a.prototype;
          _proto3.load = function load() {
            var t = this;
            return this.ad.offError(), this.ad.onError(function(e) {
              t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -2,
                sub5: t.data.sid,
                sub7: e.errMsg,
                subi4: e.errCode
              });
              for (var _iterator = _createForOfIteratorHelperLoose(t.onErrorCallbacks), _step; !(_step = _iterator()).done; ) {
                var _i = _step.value;
                _i(e);
              }
            }), this.ad.offLoad(), this.ad.onLoad(function() {
              for (var _iterator2 = _createForOfIteratorHelperLoose(t.onLoadCallbacks), _step2; !(_step2 = _iterator2()).done; ) {
                var _e = _step2.value;
                _e();
              }
            }), this.ad.load();
          };
          _proto3.show = function show() {
            var t = this;
            return this.ad.offClose(), this.ad.onClose(function(e) {
              e && e.isEnded || void 0 === e ? t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              }) : t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 1,
                sub5: t.data.sid
              });
              for (var _iterator3 = _createForOfIteratorHelperLoose(t.onCloseCallbacks), _step3; !(_step3 = _iterator3()).done; ) {
                var _i2 = _step3.value;
                _i2(e);
              }
            }), this.ad.show();
          };
          _proto3.onClose = function onClose(t) {
            this.onCloseCallbacks.push(t);
          };
          _proto3.onLoad = function onLoad(t) {
            this.onLoadCallbacks.push(t), this.ad.offLoad(), this.ad.onLoad(t);
          };
          _proto3.onError = function onError(t) {
            this.onErrorCallbacks.push(t);
            var e = this;
            this.ad.offError(), this.ad.onError(function(i) {
              e.runtime.event("ad", "imp", {
                lbl: e.data.stype,
                val: -2,
                sub5: e.adUnitId,
                sub7: i.errMsg,
                subi4: i.errCode
              }), t(i);
            });
          };
          return a;
        }(r);
        function o(t, e) {
          if (!t) {
            var _e2 = (t = window.location.search).indexOf("?");
            -1 != _e2 && (t = t.substr(_e2 + 1));
          }
          var i = {}, r = t.split(e || "&");
          for (var _t = 0; _t < r.length; _t++) {
            if (!r[_t]) continue;
            var _e3 = r[_t].split("=");
            i[_e3[0].trim()] = decodeURIComponent(_e3[1].trim());
          }
          return i;
        }
        function n(t, e) {
          var i = [];
          for (var _e4 in t) t[_e4] && i.push(_e4 + "=" + encodeURIComponent(t[_e4]));
          return i.join(e || "&");
        }
        function d(t) {
          var e = "";
          var i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          for (var _r3 = 0; _r3 < t; _r3++) e += i.charAt(Math.floor(Math.random() * i.length));
          return e;
        }
        function u(t, e) {
          for (var _i3 in t) "__proto__" != _i3 && t[_i3] && (e[_i3] = t[_i3]);
        }
        function c(t, e) {
          var i = t;
          for (var _t2 in e) i = i.replace("{" + _t2 + "}", e[_t2]);
          return i;
        }
        function h(t, e, i, r) {
          return new Promise(function(s, a) {
            var o;
            o = {
              url: e,
              method: t,
              header: i,
              data: r,
              success: function success(t) {
                200 == t.statusCode ? s(t.data) : a(Error(t.statusCode));
              },
              fail: function fail(t) {
                a(t);
              }
            }, tt.request(o);
          });
        }
        var l = new (function() {
          function _class() {}
          var _proto4 = _class.prototype;
          _proto4.get = function get(t, e) {
            return h("GET", t, e)["catch"](function(i) {
              if ("request:fail timeout" == i.errMsg) return h("GET", t, e);
              throw i;
            });
          };
          _proto4.post = function post(t, e, i) {
            return h("POST", t, e, i)["catch"](function(r) {
              if ("request:fail timeout" == r.errMsg) return h("POST", t, e, i);
              throw r;
            });
          };
          return _class;
        }())();
        function m(t) {
          tt.login(t);
        }
        function f(t) {
          var e = t.success;
          t.success = function(t) {
            e && e(t);
          };
          var i = t.fail;
          t.fail = function(t) {
            i && i(t);
          }, tt.getStorage(t);
        }
        function p(t) {
          var e = t.complete;
          t.complete = function(t) {
            t.errMsg, e && e(t);
          }, tt.setStorage(t);
        }
        function g(t) {
          tt.onShareAppMessage(t);
        }
        function b(t) {
          tt.shareAppMessage(t);
        }
        function w(t) {
          return tt.uploadFile(t);
        }
        var v = function(_s) {
          _inheritsLoose(v, _s);
          function v(t, e) {
            var _this3;
            t.createAd = tt.createBannerAd, t.params = {
              adUnitId: t.adUnitId,
              style: t.style
            }, _this3 = _s.call(this, t, e) || this;
            return _this3;
          }
          return v;
        }(s);
        var y = function y(t, e) {
          throw Error("toutiao does not support InterstitialAd");
        };
        var E = function(_a) {
          _inheritsLoose(E, _a);
          function E(t, e) {
            var _this4;
            t.createAd = tt.createRewardedVideoAd, t.params = {
              adUnitId: t.adUnitId
            }, _this4 = _a.call(this, t, e) || this;
            return _this4;
          }
          return E;
        }(a);
        var k = function() {
          function k(t, e) {
            this.data = {}, u(t, this.data), this.runtime = e, this.showed = !1;
          }
          var _proto5 = k.prototype;
          _proto5.show = function show(t) {
            if (this.showed) return;
            var e = this.data.impurl, i = {};
            var r = this.data.impurl.indexOf("?");
            -1 != r && (e = this.data.impurl.substr(0, r), i = o(this.data.impurl.substr(r + 1))), 
            t && (i.em = t, i.ec = -1), e = e + "?" + n(i), l.get(e), this.showed = !0;
          };
          _proto5.click = function click(t) {
            t = t || {}, function(t, e) {
              tt.navigateToMiniProgram({
                appId: t.appid,
                path: t.path,
                extraData: e.extraData,
                envVersion: e.envVersion,
                success: e.success,
                fail: e.fail,
                complete: function complete(i) {
                  var r = t.clkurl, s = {};
                  var a = t.clkurl.indexOf("?");
                  -1 != a && (r = t.clkurl.substr(0, a), s = o(t.clkurl.substr(a + 1))), "navigateToMiniProgram:ok" != i.errMsg && (s.em = i.errMsg, 
                  s.ec = -1), r = r + "?" + n(s), l.get(r), e.complete && e.complete(i);
                }
              });
            }(this.data, t);
          };
          _proto5.dot = function dot() {
            return this.data.dot;
          };
          return k;
        }();
        var I = function() {
          function I(t, e) {
            this.runtime = e, this.data = {
              stype: "icon",
              sid: t.slotId,
              limit: t.limit || 10,
              min_size: t.min_size || 0,
              max_size: t.max_size || 0,
              cdn: t.cdn || "icon.cdn.umgame.cn"
            }, this.data.limit <= 0 && (this.data.limit = 1);
          }
          var _proto6 = I.prototype;
          _proto6.load = function load() {
            var t = this;
            return t.runtime.wait4init().then(function() {
              var e = {};
              return u(t.data, e), u(t.runtime.data, e), l.post("https://gw.api.umgame.cn/api/v1/slot", {}, e).then(function(e) {
                if (0 !== e.c) throw Error(e.m);
                var i = e.d, r = [];
                for (var _e5 = 0; _e5 < i.length; _e5++) {
                  i[_e5].sid = t.data.sid;
                  var _s2 = new k(i[_e5], t.runtime);
                  r.push(_s2);
                }
                return t.onLoadCallback && t.onLoadCallback(r), r;
              })["catch"](function(e) {
                return t.onErrorCallback && t.onErrorCallback(e), e;
              });
            });
          };
          _proto6.onLoad = function onLoad(t) {
            this.onLoadCallback = t;
          };
          _proto6.onError = function onError(t) {
            this.onErrorCallback = t;
          };
          return I;
        }();
        var P = {
          Like: "like",
          Time: "time"
        }, q = {
          RealTime: "realtime",
          LastWeek: "lastweek"
        };
        var S = new (function() {
          function _class2() {}
          var _proto7 = _class2.prototype;
          _proto7.init = function init(t) {
            if (this.getSystemInfoPromise = null, this.getSettingPromise = null, this.authPromise = null, 
            this.initPromise = null, this.configPromise = null, this.loginPromise = null, !t.gameId) throw Error("obj.gameId is required by init(obj)");
            var e = tt.getLaunchOptionsSync();
            this.data = {
              sdkv: "0.0.14",
              gid: parseInt(t.gameId),
              uid: t.uid,
              uinfo: t.userInfo || {},
              cid: parseInt(e.query.cid || 0),
              utmsrc: e.query.utmsrc || "",
              suid: e.query.suid || "",
              ssid: e.query.ssid || "",
              smid: parseInt(e.query.smid || 0)
            };
            var i = this;
            var r;
            this.getSystemInfoPromise = new Promise(function(t, e) {
              new Promise(function(t, e) {
                tt.getSystemInfo({
                  success: function success(e) {
                    var i = {
                      lang: e.language,
                      os: e.platform,
                      osv: e.system,
                      make: e.brand,
                      model: e.model,
                      width: e.screenWidth,
                      height: e.screenHeight,
                      pv: e.version,
                      plv: e.SDKVersion
                    };
                    e.windowWidth > e.windowHeight ? i.o = "landscape" : i.o = "portrait", t(i);
                  },
                  fail: e
                });
              }).then(function(e) {
                u(e, i.data), t(e);
              }, e);
            }), this.getSettingPromise = new Promise(function(t, e) {
              !function(t) {
                tt.getSetting(t);
              }({
                withSubscriptions: !0,
                success: function success(e) {
                  var r = (e.subscriptionsSetting || {}).itemSettings || {}, s = [];
                  for (var _t3 in r) "accept" == r[_t3] && s.push(_t3);
                  i.data.tids = s, t(e);
                },
                fail: e
              });
            }), this.authPromise = new Promise(function(e, r) {
              t.userId ? (i.setUserId(t.userId), e({
                openid: t.userId
              })) : i.login().then(function(t) {
                i.setUserId(t.openid), e(t);
              }, r);
            }), this.initPromise = Promise.all([ this.getSystemInfoPromise, this.getSettingPromise, this.authPromise ])["catch"](function(t) {
              i.data.err = t.message;
            }).then(function() {
              return l.post("https://gw.api.umgame.cn/api/v1/init", {}, i.data).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return i.data.inited = 1, i.data.cid = t.d.cid, i.data.utmsrc = t.d.utmsrc, i.data.its = t.d.its, 
                i.bald = t.d.bald, i.config = t.d.params || {}, i.shareMaterial = t.d.share_material || {}, 
                i.shareStats = {
                  new: t.d.share_new,
                  exists: t.d.share_old
                }, i.loc = {
                  province: t.d.province,
                  city: t.d.city
                }, !0;
              });
            })["catch"](function(t) {
              return i.data.inited = 0, !1;
            }), this.configPromise = this.initPromise.then(function() {
              return i.config;
            }), this.startTime = Date.now(), r = function r(t) {
              i.isShareDialogVisible ? i.isShareDialogVisible = !1 : i.startTime = Date.now();
            }, tt.onShow(r), function(t) {
              tt.onHide(t);
            }(function(t) {
              if (i.isShareDialogVisible) return;
              var e = Date.now(), r = parseInt((e - i.startTime) / 1e3);
              i.event("sdk", "exit", {
                val: r
              });
            });
          };
          _proto7.login = function login() {
            var t = this;
            return this.loginPromise = new Promise(function(e, i) {
              var r = "umsdk:auth:" + t.data.gid, s = {
                success: function success(s) {
                  if (s.code) {
                    var _a2 = "https://gw.api.umgame.cn/api/v1/auth?" + n({
                      gid: t.data.gid,
                      code: s.code
                    }, "&");
                    l.get(_a2).then(function(t) {
                      0 === t.c ? p({
                        key: r,
                        data: JSON.stringify(t.d),
                        complete: function complete(i) {
                          e(t.d);
                        }
                      }) : i(Error(t.m));
                    })["catch"](i);
                  } else i(s);
                },
                fail: function fail(e) {
                  i(e), t.loginPromise = null;
                }
              };
              f({
                key: r,
                success: function success(t) {
                  var i = JSON.parse(t.data);
                  var r;
                  r = {
                    success: function success() {
                      e(i);
                    },
                    fail: function fail() {
                      m(s);
                    }
                  }, tt.checkSession(r);
                },
                fail: function fail(t) {
                  m(s);
                }
              });
            }), this.loginPromise;
          };
          _proto7.getUserInfo = function getUserInfo() {
            var t = this;
            return new Promise(function(e, i) {
              var r = "umsdk:userinfo:" + t.data.gid;
              f({
                key: r,
                success: function success(t) {
                  var i = JSON.parse(t.data);
                  e(i);
                },
                fail: function fail(s) {
                  (t.loginPromise || t.login()).then(function(t) {
                    var s;
                    s = {
                      success: function success(t) {
                        p({
                          key: r,
                          data: JSON.stringify(t.userInfo),
                          complete: function complete(i) {
                            e(t.userInfo);
                          }
                        });
                      },
                      fail: i
                    }, tt.getUserInfo(s);
                  });
                }
              });
            });
          };
          _proto7.setUserId = function setUserId(t) {
            this.data.uid = t;
          };
          _proto7.getUserId = function getUserId() {
            return this.data.uid;
          };
          _proto7.getSetting = function getSetting() {
            return this.getSettingPromise;
          };
          _proto7.getTopVideos = function getTopVideos(t) {
            var e = P.Like;
            t.sortBy == P.Time && (e = P.Time);
            var i = q.RealTime;
            t.timeType == q.LastWeek && (i = q.LastWeek);
            var r = {
              gid: this.data.gid,
              number_of_top: t.limit || 10,
              by: e,
              time: i
            };
            return l.post("https://gw.api.umgame.cn/api/v1/get_top_video", {}, r).then(function(t) {
              if (0 !== t.c) throw Error(t.m);
              return t.d;
            });
          };
          _proto7.getTopic = function getTopic() {
            var t = "https://gw.api.umgame.cn/api/v1/topic?" + n({
              gid: this.data.gid,
              uid: this.data.uid
            }, "&");
            return l.get(t).then(function(t) {
              if (0 !== t.c) throw Error(t.m);
              return t.d;
            });
          };
          _proto7.addTopic = function addTopic(t) {
            if (!t.username) throw Error("[umsdk] username is required");
            if (!t.title) throw Error("[umsdk] title is required");
            if (!t.filePath) throw Error("[umsdk] filePath is required");
            return w({
              url: "https://gw.api.umgame.cn/api/v1/topic",
              filePath: t.filePath,
              name: "file",
              formData: {
                gid: this.data.gid,
                uid: this.data.uid,
                username: t.username,
                title: t.title
              },
              success: function success(e) {
                if (200 === e.statusCode) {
                  var _i4 = JSON.parse(e.data);
                  if (0 === _i4.c) return void (t.success && t.success(_i4.d));
                  t.fail && t.fail(Error(_i4.m));
                } else t.fail && t.fail(Error(e.statusCode));
              },
              fail: function fail(e) {
                t.fail && t.fail(e);
              }
            });
          };
          _proto7.searchTopic = function searchTopic(t) {
            if (!t.name) throw Error("[umsdk] name is required");
            var e = {
              gid: this.data.gid,
              topic_name: t.name
            };
            return l.post("https://gw.api.umgame.cn/api/v1/search_topic", {}, e).then(function(t) {
              if (0 !== t.c) throw Error(t.m);
              return t.d.topics || [];
            });
          };
          _proto7.addVote = function addVote(t) {
            if (!t.tuid) throw Error("[umsdk] tuid is required");
            var e = {
              tuid: t.tuid,
              gid: this.data.gid
            };
            return l.post("https://gw.api.umgame.cn/api/v1/vote", {}, e).then(function(t) {
              if (0 !== t.c) throw Error(t.m);
              return !0;
            });
          };
          _proto7.addMap = function addMap(t) {
            if (!t.title) throw Error("[umsdk] title is required");
            if (!t.data) throw Error("[umsdk] data is required");
            var e = this;
            return Promise.all([ this.wait4init(), this.getUserInfo() ]).then(function(_ref) {
              var i = _ref[0], r = _ref[1];
              var s = {
                uid: e.data.uid,
                username: r.nickName,
                avatar_url: r.avatarUrl,
                title: t.title,
                data: JSON.stringify(t.data)
              }, a = c("https://gw.api.umgame.cn/api/v1/maps/{gid}", {
                gid: e.data.gid
              });
              return l.post(a, {}, s).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return !0;
              });
            });
          };
          _proto7.getMaps = function getMaps() {
            var t = this;
            return this.wait4init().then(function() {
              var e = {
                uid: t.data.uid
              }, i = c("https://gw.api.umgame.cn/api/v1/maps/{gid}", {
                gid: t.data.gid
              }) + "?" + n(e, "&");
              return l.get(i).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return t.d;
              });
            });
          };
          _proto7.getMap = function getMap(t) {
            if (!t.gmid) throw Error("[umsdk] gmid is required");
            var e = c("https://gw.api.umgame.cn/api/v1/maps/{gid}/{gmid}", {
              gid: this.data.gid,
              gmid: t.gmid
            });
            return this.wait4init().then(function() {
              return l.get(e).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return t.d;
              });
            });
          };
          _proto7.openMap = function openMap(t) {
            return t.open = !0, this.updateMap(t);
          };
          _proto7.passMap = function passMap(t) {
            return t.pass = !0, this.updateMap(t);
          };
          _proto7.deleteMap = function deleteMap(t) {
            return t["delete"] = !0, this.updateMap(t);
          };
          _proto7.updateMap = function updateMap(t) {
            if (!t.gmid) throw Error("[umsdk] gmid is required");
            var e = this;
            return this.wait4init().then(function() {
              var i = c("https://gw.api.umgame.cn/api/v1/maps/{gid}/{gmid}", {
                gid: e.data.gid,
                gmid: t.gmid
              }), r = {
                uid: e.data.uid,
                open: t.open || !1,
                pass: t.pass || !1,
                delete: t["delete"] || !1
              };
              return l.post(i, {}, r).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return !0;
              });
            });
          };
          _proto7.addImage = function addImage(t) {
            if (!t.fid) throw Error("[umsdk] fid is required");
            if (!t.filePath) throw Error("[umsdk] filePath is required");
            return w({
              url: c("https://gw.api.umgame.cn/api/v1/img/{gid}/{fid}", {
                gid: this.data.gid,
                fid: t.fid
              }),
              filePath: t.filePath,
              name: "file",
              formData: {
                uid: this.data.uid,
                term: t.term || 0,
                height: t.height || 0,
                width: t.width || 0,
                count: t.count || 0
              },
              success: function success(e) {
                if (200 === e.statusCode) {
                  var _i5 = JSON.parse(e.data);
                  if (0 === _i5.c) return void (t.success && t.success(_i5.d));
                  t.fail && t.fail(Error(_i5.m));
                } else t.fail && t.fail(Error(e.statusCode));
              },
              fail: function fail(e) {
                t.fail && t.fail(e);
              }
            });
          };
          _proto7.getFnObjs = function getFnObjs(t) {
            if (!t.fid) throw Error("[umsdk] fid is required");
            var e = this;
            return this.wait4init().then(function() {
              var i = {
                uid: e.data.uid,
                term: t.term || 0
              }, r = c("https://gw.api.umgame.cn/api/v1/obj/{gid}/{fid}", {
                gid: e.data.gid,
                fid: t.fid
              }) + "?" + n(i, "&");
              return l.get(r).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return t.d;
              });
            });
          };
          _proto7.addFnObj = function addFnObj(t) {
            if (!t.fid) throw Error("[umsdk] fid is required");
            if (!t.title) throw Error("[umsdk] title is required");
            if (!t.data) throw Error("[umsdk] data is required");
            var e = this;
            return Promise.all([ this.wait4init(), this.getUserInfo() ]).then(function(_ref2) {
              var i = _ref2[0], r = _ref2[1];
              var s = c("https://gw.api.umgame.cn/api/v1/obj/{gid}/{fid}", {
                gid: e.data.gid,
                fid: t.fid
              }), a = {
                uid: e.data.uid,
                username: r.nickName,
                avatar_url: r.avatarUrl,
                title: t.title,
                data: JSON.stringify(t.data || {}),
                imgs: t.imgs || [],
                term: t.term || 0
              };
              return l.post(s, {}, a).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return !0;
              });
            });
          };
          _proto7.getFnObj = function getFnObj(t) {
            if (!t.fid) throw Error("[umsdk] fid is required");
            if (!t.oid) throw Error("[umsdk] oid is required");
            var e = this;
            return this.wait4init().then(function() {
              var i = c("https://gw.api.umgame.cn/api/v1/obj/{gid}/{fid}/{oid}", {
                gid: e.data.gid,
                fid: t.fid,
                oid: t.oid
              });
              return l.get(i).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return t.d;
              });
            });
          };
          _proto7.updateFnObj = function updateFnObj(t) {
            if (!t.fid) throw Error("[umsdk] fid is required");
            if (!t.oid) throw Error("[umsdk] oid is required");
            var e = this;
            return this.wait4init().then(function() {
              var i = c("https://gw.api.umgame.cn/api/v1/obj/{gid}/{fid}/{oid}", {
                gid: e.data.gid,
                fid: t.fid,
                oid: t.oid
              }), r = {
                uid: e.data.uid,
                load: t.load || !1,
                pass: t.pass || !1,
                fail: t.fail || !1,
                custom: t.custom || !1,
                delete: t["delete"] || !1,
                ad: t.ad || !1
              };
              return l.post(i, {}, r).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return !0;
              });
            });
          };
          _proto7.deleteFnObj = function deleteFnObj(t) {
            return t["delete"] = !0, this.updateFnObj(t);
          };
          _proto7.foeLoad = function foeLoad(t) {
            return t.load = !0, this.updateFnObj(t);
          };
          _proto7.foePass = function foePass(t) {
            return t.pass = !0, this.updateFnObj(t);
          };
          _proto7.foeFail = function foeFail(t) {
            return t.fail = !0, this.updateFnObj(t);
          };
          _proto7.foeCustom = function foeCustom(t) {
            if (void 0 === t.custom) throw Error("[umsdk] custom is required");
            return this.updateFnObj(t);
          };
          _proto7.foeAd = function foeAd(t) {
            return t.ad = !0, this.updateFuncObj(t);
          };
          _proto7.loadAttrLib = function loadAttrLib(t, e) {
            if (!t) throw Error("function fn is required by loadAttrLib(fn)");
            var i = this;
            this.initPromise.then(function(r) {
              r && (i.bald && !e || t());
            });
          };
          _proto7.fetchConfig = function fetchConfig() {
            return this.configPromise;
          };
          _proto7.fetchLocation = function fetchLocation() {
            var t = this;
            return this.initPromise.then(function() {
              return t.loc;
            });
          };
          _proto7.fetchShareData = function fetchShareData() {
            var t = this;
            return this.initPromise.then(function() {
              return t.shareStats;
            });
          };
          _proto7.fetchLaunchOptions = function fetchLaunchOptions() {
            var t = this;
            return this.initPromise.then(function() {
              return {
                cid: t.data.cid,
                src: t.data.utmsrc,
                its: t.data.its
              };
            });
          };
          _proto7.fetchShareQuery = function fetchShareQuery() {
            var t = this;
            return this.initPromise.then(function() {
              return n({
                utmsrc: "share",
                cid: t.data.cid,
                suid: t.data.uid,
                smid: t.shareMaterial.smid
              });
            });
          };
          _proto7.subscribe = function subscribe(t) {
            var e = this;
            this.wait4init().then(function() {
              var i = {};
              u(e.data, i), "subscribeAppMsg:ok" == t.errMsg && (i.tall = 1), t.ids && (i.tids = t.ids), 
              l.post("https://gw.api.umgame.cn/api/v1/sub", {}, i);
            });
          };
          _proto7.wait4init = function wait4init() {
            return this.initPromise;
          };
          _proto7.event = function event(t, e, i) {
            if (!t || !e) throw Error("cat and act is required by event(cat, act, params)");
            var r = this;
            return this.wait4init().then(function() {
              var s = {
                app: "minigame",
                cat: t,
                act: e,
                rid: d(16),
                lang: r.data.lang,
                os: r.data.os,
                osv: r.data.osv,
                make: r.data.make,
                model: r.data.model,
                w: r.data.width,
                h: r.data.height,
                did: r.data.uid,
                sub1: r.data.sdkv,
                sub2: r.data.pv,
                sub3: r.data.plv,
                sub4: r.data.utmsrc,
                subi1: r.data.gid,
                subi2: r.data.cid,
                subi3: r.data.its,
                subi7: r.data.inited
              };
              i && u(i, s);
              var a = "https://event.api.umgame.cn/api/v1/event?" + n(s, "&");
              return l.get(a);
            });
          };
          _proto7.onShareAppMessage = function onShareAppMessage(t, e) {
            var i = this;
            this.fetchShareQuery().then(function(r) {
              g(function() {
                var s = e();
                return i.shareMaterial.title && (s.title = i.shareMaterial.title), i.shareMaterial.image && (s.imageUrl = i.shareMaterial.image), 
                s.query ? s.query = s.query + "&" + r : s.query = r, s.query = s.query + "&ssid=" + t, 
                i.isShareDialogVisible = !0, i.event("user", "share", {
                  sub5: t,
                  subi10: i.shareMaterial.smid
                }), s;
              });
            }, function(t) {
              i.isShareDialogVisible = !0, g(e);
            });
          };
          _proto7.shareAppMessage = function shareAppMessage(t) {
            if (!t.slotId || !t.data) throw Error("`obj.slotId` and `obj.data` are required by shareAppMessage");
            var e = this;
            this.fetchShareQuery().then(function(i) {
              if (e.shareMaterial.title && (t.data.title = e.shareMaterial.title), e.shareMaterial.image && (t.data.imageUrl = e.shareMaterial.image), 
              t.data.query ? t.data.query = t.data.query + "&" + i : t.data.query = i, t.data.query = t.data.query + "&ssid=" + t.slotId, 
              e.isShareDialogVisible = !0, t.data.success) {
                var _i6 = t.data.success;
                t.data.success = function() {
                  e.event("user", "share", {
                    sub5: t.slotId,
                    subi10: e.shareMaterial.smid
                  }), _i6();
                }, b(t.data);
              } else b(t.data), e.event("user", "share", {
                sub5: t.slotId,
                subi10: e.shareMaterial.smid
              });
            }, function(i) {
              e.isShareDialogVisible = !0, b(t.data);
            });
          };
          _proto7.createIconAd = function createIconAd(t) {
            if (!t.slotId) throw Error("obj.slotId is required by createIconAd(obj)");
            return new I(t, this);
          };
          _proto7.createBannerAd = function createBannerAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createBannerAd(obj)");
            if (!t.slotId) throw Error("obj.slotId is required by createBannerAd(obj)");
            return new v(t, this);
          };
          _proto7.createInterstitialAd = function createInterstitialAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createInterstitialAd(obj)");
            if (!t.slotId) throw Error("obj.slotId is required by createInterstitialAd(obj)");
            return new y(t, this);
          };
          _proto7.createRewardedVideoAd = function createRewardedVideoAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createRewardedVideoAd(obj);");
            if (!t.slotId) throw Error("obj.slotId is required by createRewardedVideoAd(obj)");
            return new E(t, this);
          };
          return _class2;
        }())();
        S.TTVideoSortBy = P, S.TTVideoTimeType = q;
        e["default"] = S;
      } ]);
    });
    cc._RF.pop();
  }, {} ],
  "umsdk.wechat": [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "6af69zQk9NNF5J7i4hoRZx2", "umsdk.wechat");
    "use strict";
    function _createForOfIteratorHelperLoose(o, allowArrayLike) {
      var it;
      if ("undefined" === typeof Symbol || null == o[Symbol.iterator]) {
        if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && "number" === typeof o.length) {
          it && (o = it);
          var i = 0;
          return function() {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          };
        }
        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
      }
      it = o[Symbol.iterator]();
      return it.next.bind(it);
    }
    function _unsupportedIterableToArray(o, minLen) {
      if (!o) return;
      if ("string" === typeof o) return _arrayLikeToArray(o, minLen);
      var n = Object.prototype.toString.call(o).slice(8, -1);
      "Object" === n && o.constructor && (n = o.constructor.name);
      if ("Map" === n || "Set" === n) return Array.from(o);
      if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
    }
    function _arrayLikeToArray(arr, len) {
      (null == len || len > arr.length) && (len = arr.length);
      for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
      return arr2;
    }
    function _inheritsLoose(subClass, superClass) {
      subClass.prototype = Object.create(superClass.prototype);
      subClass.prototype.constructor = subClass;
      subClass.__proto__ = superClass;
    }
    !function(t, e) {
      "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.umsdk = e() : t.umsdk = e();
    }(void 0, function() {
      return function(t) {
        var e = {};
        function s(i) {
          if (e[i]) return e[i].exports;
          var a = e[i] = {
            i: i,
            l: !1,
            exports: {}
          };
          return t[i].call(a.exports, a, a.exports, s), a.l = !0, a.exports;
        }
        return s.m = t, s.c = e, s.d = function(t, e, i) {
          s.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: i
          });
        }, s.r = function(t) {
          "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
          }), Object.defineProperty(t, "__esModule", {
            value: !0
          });
        }, s.t = function(t, e) {
          if (1 & e && (t = s(t)), 8 & e) return t;
          if (4 & e && "object" == typeof t && t && t.__esModule) return t;
          var i = Object.create(null);
          if (s.r(i), Object.defineProperty(i, "default", {
            enumerable: !0,
            value: t
          }), 2 & e && "string" != typeof t) for (var a in t) s.d(i, a, function(e) {
            return t[e];
          }.bind(null, a));
          return i;
        }, s.n = function(t) {
          var e = t && t.__esModule ? function() {
            return t["default"];
          } : function() {
            return t;
          };
          return s.d(e, "a", e), e;
        }, s.o = function(t, e) {
          return Object.prototype.hasOwnProperty.call(t, e);
        }, s.p = "", s(s.s = 0);
      }([ function(t, e, s) {
        s.r(e);
        var i = function() {
          function i(t, e) {
            this.runtime = e, this.data = {
              sid: t.slotId
            }, this.ad = t.createAd(t.params), this.ad.onError(function(t) {
              self.runtime.event("ad", "imp", {
                lbl: self.data.stype,
                val: -2,
                sub5: self.data.sid,
                sub7: t.errMsg,
                subi4: t.errCode
              });
            });
          }
          var _proto = i.prototype;
          _proto.load = function load() {
            return this.ad.load();
          };
          _proto.show = function show() {
            var t = this;
            return this.ad.show().then(function() {
              t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              });
            }, function(e) {
              throw t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -1,
                sub5: t.data.sid,
                sub7: e.errMsg,
                subi4: e.errCode
              }), e;
            });
          };
          _proto.destroy = function destroy() {
            this.ad.destroy();
          };
          _proto.onClose = function onClose(t) {
            this.ad.onClose(t);
          };
          _proto.offClose = function offClose(t) {
            this.ad.offClose(t);
          };
          _proto.onLoad = function onLoad(t) {
            this.ad.onLoad(t);
          };
          _proto.offLoad = function offLoad(t) {
            this.ad.offLoad(t);
          };
          _proto.onError = function onError(t) {
            this.ad.onError(t);
          };
          _proto.offError = function offError(t) {
            this.ad.offError(t);
          };
          return i;
        }();
        var a = function(_i) {
          _inheritsLoose(a, _i);
          function a(t, e) {
            var _this;
            _this = _i.call(this, t, e) || this, _this.data.stype = "banner", _this.showed = !1;
            return _this;
          }
          var _proto2 = a.prototype;
          _proto2.show = function show() {
            var t = this;
            return this.ad.show().then(function() {
              t.showed || (t.showed = !0, t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              }));
            }, function(e) {
              throw t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -1,
                sub5: t.data.sid,
                subi4: e.errCode
              }), e;
            });
          };
          _proto2.hide = function hide() {
            this.ad.hide();
          };
          _proto2.offResize = function offResize(t) {
            this.ad.offResize(t);
          };
          _proto2.onResize = function onResize(t) {
            this.ad.onResize(t);
          };
          return a;
        }(i);
        var r = function(_i2) {
          _inheritsLoose(r, _i2);
          function r(t, e) {
            var _this2;
            _this2 = _i2.call(this, t, e) || this, _this2.data.stype = "interstitial";
            return _this2;
          }
          return r;
        }(i);
        var o = function(_i3) {
          _inheritsLoose(o, _i3);
          function o(t, e) {
            var _this3;
            _this3 = _i3.call(this, t, e) || this, _this3.data.stype = "video", _this3.adUnitId = t.adUnitId, 
            _this3.onCloseCallbacks = [], _this3.onLoadCallbacks = [], _this3.onErrorCallbacks = [];
            return _this3;
          }
          var _proto3 = o.prototype;
          _proto3.load = function load() {
            var t = this;
            return this.ad.offError(), this.ad.onError(function(e) {
              t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: -2,
                sub5: t.data.sid,
                sub7: e.errMsg,
                subi4: e.errCode
              });
              for (var _iterator = _createForOfIteratorHelperLoose(t.onErrorCallbacks), _step; !(_step = _iterator()).done; ) {
                var _s = _step.value;
                _s(e);
              }
            }), this.ad.offLoad(), this.ad.onLoad(function() {
              for (var _iterator2 = _createForOfIteratorHelperLoose(t.onLoadCallbacks), _step2; !(_step2 = _iterator2()).done; ) {
                var _e = _step2.value;
                _e();
              }
            }), this.ad.load();
          };
          _proto3.show = function show() {
            var t = this;
            return this.ad.offClose(), this.ad.onClose(function(e) {
              e && e.isEnded || void 0 === e ? t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 0,
                sub5: t.data.sid
              }) : t.runtime.event("ad", "imp", {
                lbl: t.data.stype,
                val: 1,
                sub5: t.data.sid
              });
              for (var _iterator3 = _createForOfIteratorHelperLoose(t.onCloseCallbacks), _step3; !(_step3 = _iterator3()).done; ) {
                var _s2 = _step3.value;
                _s2(e);
              }
            }), this.ad.show();
          };
          _proto3.onClose = function onClose(t) {
            this.onCloseCallbacks.push(t);
          };
          _proto3.onLoad = function onLoad(t) {
            this.onLoadCallbacks.push(t), this.ad.offLoad(), this.ad.onLoad(t);
          };
          _proto3.onError = function onError(t) {
            this.onErrorCallbacks.push(t);
            var e = this;
            this.ad.offError(), this.ad.onError(function(s) {
              e.runtime.event("ad", "imp", {
                lbl: e.data.stype,
                val: -2,
                sub5: e.adUnitId,
                sub7: s.errMsg,
                subi4: s.errCode
              }), t(s);
            });
          };
          return o;
        }(i);
        function n(t, e) {
          if (!t) {
            var _e2 = (t = window.location.search).indexOf("?");
            -1 != _e2 && (t = t.substr(_e2 + 1));
          }
          var s = {}, i = t.split(e || "&");
          for (var _t = 0; _t < i.length; _t++) {
            if (!i[_t]) continue;
            var _e3 = i[_t].split("=");
            s[_e3[0].trim()] = decodeURIComponent(_e3[1].trim());
          }
          return s;
        }
        function d(t, e) {
          var s = [];
          for (var _e4 in t) t[_e4] && s.push(_e4 + "=" + encodeURIComponent(t[_e4]));
          return s.join(e || "&");
        }
        function u(t) {
          var e = "";
          var s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          for (var _i4 = 0; _i4 < t; _i4++) e += s.charAt(Math.floor(Math.random() * s.length));
          return e;
        }
        function c(t, e) {
          for (var _s3 in t) "__proto__" != _s3 && t[_s3] && (e[_s3] = t[_s3]);
        }
        function h(t, e, s, i) {
          return new Promise(function(a, r) {
            var o;
            o = {
              url: e,
              method: t,
              header: s,
              data: i,
              success: function success(t) {
                200 == t.statusCode ? a(t.data) : r(Error(t.statusCode));
              },
              fail: function fail(t) {
                r(t);
              }
            }, wx.request(o);
          });
        }
        var l = new (function() {
          function _class() {}
          var _proto4 = _class.prototype;
          _proto4.get = function get(t, e) {
            return h("GET", t, e)["catch"](function(s) {
              if ("request:fail timeout" == s.errMsg) return h("GET", t, e);
              throw s;
            });
          };
          _proto4.post = function post(t, e, s) {
            return h("POST", t, e, s)["catch"](function(i) {
              if ("request:fail timeout" == i.errMsg) return h("POST", t, e, s);
              throw i;
            });
          };
          return _class;
        }())();
        function f(t) {
          wx.login(t);
        }
        function m(t) {
          wx.onShareAppMessage(t);
        }
        function p(t) {
          wx.shareAppMessage(t);
        }
        var b = function(_a) {
          _inheritsLoose(b, _a);
          function b(t, e) {
            var _this4;
            t.createAd = wx.createBannerAd, t.params = {
              adUnitId: t.adUnitId,
              adIntervals: t.adIntervals,
              style: t.style
            }, _this4 = _a.call(this, t, e) || this, _this4.style = _this4.ad.style;
            return _this4;
          }
          return b;
        }(a);
        var g = function(_r) {
          _inheritsLoose(g, _r);
          function g(t, e) {
            var _this5;
            t.createAd = wx.createInterstitialAd, t.params = {
              adUnitId: t.adUnitId
            }, _this5 = _r.call(this, t, e) || this;
            return _this5;
          }
          return g;
        }(r);
        var w = function(_o) {
          _inheritsLoose(w, _o);
          function w(t, e) {
            var _this6;
            t.createAd = wx.createRewardedVideoAd, t.params = {
              adUnitId: t.adUnitId,
              multiton: t.multiton
            }, _this6 = _o.call(this, t, e) || this;
            return _this6;
          }
          return w;
        }(o);
        var y = function() {
          function y(t, e) {
            this.data = {}, c(t, this.data), this.runtime = e, this.showed = !1;
          }
          var _proto5 = y.prototype;
          _proto5.show = function show(t) {
            if (this.showed) return;
            var e = this.data.impurl, s = {};
            var i = this.data.impurl.indexOf("?");
            -1 != i && (e = this.data.impurl.substr(0, i), s = n(this.data.impurl.substr(i + 1))), 
            t && (s.em = t, s.ec = -1), e = e + "?" + d(s), l.get(e), this.showed = !0;
          };
          _proto5.click = function click(t) {
            t = t || {}, function(t, e) {
              wx.navigateToMiniProgram({
                appId: t.appid,
                path: t.path,
                extraData: e.extraData,
                envVersion: e.envVersion,
                success: e.success,
                fail: e.fail,
                complete: function complete(s) {
                  var i = t.clkurl, a = {};
                  var r = t.clkurl.indexOf("?");
                  -1 != r && (i = t.clkurl.substr(0, r), a = n(t.clkurl.substr(r + 1))), "navigateToMiniProgram:ok" != s.errMsg && (a.em = s.errMsg, 
                  a.ec = -1), i = i + "?" + d(a), l.get(i), e.complete && e.complete(s);
                }
              });
            }(this.data, t);
          };
          _proto5.dot = function dot() {
            return this.data.dot;
          };
          return y;
        }();
        var v = function() {
          function v(t, e) {
            this.runtime = e, this.data = {
              stype: "icon",
              sid: t.slotId,
              limit: t.limit || 10,
              min_size: t.min_size || 0,
              max_size: t.max_size || 0
            }, this.data.limit <= 0 && (this.data.limit = 1);
          }
          var _proto6 = v.prototype;
          _proto6.load = function load() {
            var t = this;
            return t.runtime.wait4init().then(function() {
              var e = {};
              return c(t.data, e), c(t.runtime.data, e), l.post("https://gw.api.umgame.cn/api/v1/slot", {}, e).then(function(e) {
                if (0 !== e.c) throw Error(e.m);
                var s = e.d, i = [];
                for (var _e5 = 0; _e5 < s.length; _e5++) {
                  s[_e5].sid = t.data.sid;
                  var _a2 = new y(s[_e5], t.runtime);
                  i.push(_a2);
                }
                return t.onLoadCallback && t.onLoadCallback(i), i;
              })["catch"](function(e) {
                return t.onErrorCallback && t.onErrorCallback(e), e;
              });
            });
          };
          _proto6.onLoad = function onLoad(t) {
            this.onLoadCallback = t;
          };
          _proto6.onError = function onError(t) {
            this.onErrorCallback = t;
          };
          return v;
        }();
        e["default"] = new (function() {
          function _class2() {}
          var _proto7 = _class2.prototype;
          _proto7.init = function init(t) {
            if (!t.gameId) throw Error("obj.gameId is required by init(obj)");
            var e = wx.getLaunchOptionsSync();
            this.data = {
              sdkv: "0.0.12",
              gid: parseInt(t.gameId),
              uid: t.userId,
              uinfo: t.userInfo || {},
              cid: parseInt(e.query.cid || 0),
              utmsrc: e.query.utmsrc || "",
              suid: e.query.suid || "",
              ssid: e.query.ssid || "",
              smid: parseInt(e.query.smid || 0)
            };
            var s = this;
            var i;
            this.getSystemInfoPromise = new Promise(function(t, e) {
              new Promise(function(t, e) {
                wx.getSystemInfo({
                  success: function success(e) {
                    var _e$system$split = e.system.split(" "), s = _e$system$split[0], i = _e$system$split[1], a = {
                      lang: e.language,
                      os: s,
                      osv: i,
                      make: e.brand,
                      model: e.model,
                      width: e.screenWidth,
                      height: e.screenHeight,
                      pv: e.version,
                      plv: e.SDKVersion
                    };
                    e.windowWidth > e.windowHeight ? a.o = "landscape" : a.o = "portrait", t(a);
                  },
                  fail: e
                });
              }).then(function(e) {
                c(e, s.data), t(e);
              }, e);
            }), this.getSettingPromise = new Promise(function(t, e) {
              !function(t) {
                wx.getSetting(t);
              }({
                withSubscriptions: !0,
                success: function success(e) {
                  var i = (e.subscriptionsSetting || {}).itemSettings || {}, a = [];
                  for (var _t2 in i) "accept" == i[_t2] && a.push(_t2);
                  s.data.tids = a, t(e);
                },
                fail: e
              });
            }), this.authPromise = new Promise(function(e, i) {
              t.userId ? e({
                openid: t.userId
              }) : s.login().then(function(t) {
                s.data.uid = t.openid, e(t);
              }, i);
            }), this.initPromise = Promise.all([ this.getSystemInfoPromise, this.getSettingPromise, this.authPromise ])["catch"](function(t) {
              s.data.err = t.message;
            }).then(function() {
              return l.post("https://gw.api.umgame.cn/api/v1/init", {}, s.data).then(function(t) {
                if (0 !== t.c) throw Error(t.m);
                return s.data.inited = 1, s.data.cid = t.d.cid, s.data.utmsrc = t.d.utmsrc, s.data.its = t.d.its, 
                s.bald = t.d.bald, s.config = t.d.params || {}, s.shareMaterial = t.d.share_material || {}, 
                s.shareStats = {
                  new: t.d.share_new,
                  exists: t.d.share_old
                }, s.loc = {
                  province: t.d.province,
                  city: t.d.city
                }, !0;
              });
            })["catch"](function(t) {
              return s.data.inited = 0, !1;
            }), this.configPromise = this.initPromise.then(function() {
              return s.config;
            }), this.startTime = Date.now(), i = function i(t) {
              s.isShareDialogVisible ? s.isShareDialogVisible = !1 : s.startTime = Date.now();
            }, wx.onShow(i), function(t) {
              wx.onHide(t);
            }(function(t) {
              if (s.isShareDialogVisible) return;
              var e = Date.now(), i = parseInt((e - s.startTime) / 1e3);
              s.event("sdk", "exit", {
                val: i
              });
            });
          };
          _proto7.login = function login() {
            var t = this;
            return new Promise(function(e, s) {
              var i = "umsdk:auth:" + t.data.gid, a = {
                success: function success(a) {
                  if (a.code) {
                    var _r2 = "https://gw.api.umgame.cn/api/v1/auth?" + d({
                      gid: t.data.gid,
                      code: a.code
                    }, "&");
                    l.get(_r2).then(function(t) {
                      var a;
                      0 === t.c ? (a = {
                        key: i,
                        data: JSON.stringify(t.d),
                        complete: function complete(s) {
                          s.errMsg, e(t.d);
                        }
                      }, wx.setStorage(a)) : s(Error(t.m));
                    })["catch"](s);
                  } else s(a);
                },
                fail: function fail(t) {
                  s(t);
                }
              };
              !function(t) {
                wx.getStorage(t);
              }({
                key: i,
                success: function success(t) {
                  var s = JSON.parse(t.data);
                  var i;
                  i = {
                    success: function success() {
                      e(s);
                    },
                    fail: function fail() {
                      f(a);
                    }
                  }, wx.checkSession(i);
                },
                fail: function fail(t) {
                  f(a);
                }
              });
            });
          };
          _proto7.getSetting = function getSetting() {
            return this.getSettingPromise;
          };
          _proto7.loadAttrLib = function loadAttrLib(t, e) {
            if (!t) throw Error("function fn is required by loadAttrLib(fn)");
            var s = this;
            this.initPromise.then(function(i) {
              i && (s.bald && !e || t());
            });
          };
          _proto7.fetchConfig = function fetchConfig() {
            return this.configPromise;
          };
          _proto7.fetchLocation = function fetchLocation() {
            var t = this;
            return this.initPromise.then(function() {
              return t.loc;
            });
          };
          _proto7.fetchShareData = function fetchShareData() {
            var t = this;
            return this.initPromise.then(function() {
              return t.shareStats;
            });
          };
          _proto7.fetchLaunchOptions = function fetchLaunchOptions() {
            var t = this;
            return this.initPromise.then(function() {
              return {
                cid: t.data.cid,
                src: t.data.utmsrc,
                its: t.data.its
              };
            });
          };
          _proto7.fetchShareQuery = function fetchShareQuery() {
            var t = this;
            return this.initPromise.then(function() {
              return d({
                utmsrc: "share",
                cid: t.data.cid,
                suid: t.data.uid,
                smid: t.shareMaterial.smid
              });
            });
          };
          _proto7.subscribe = function subscribe(t) {
            var e = this;
            this.wait4init().then(function() {
              var s = {};
              c(e.data, s), "subscribeAppMsg:ok" == t.errMsg && (s.tall = 1), t.ids && (s.tids = t.ids), 
              l.post("https://gw.api.umgame.cn/api/v1/sub", {}, s);
            });
          };
          _proto7.auth = function auth() {
            return this.authPromise;
          };
          _proto7.wait4init = function wait4init() {
            return this.initPromise;
          };
          _proto7.event = function event(t, e, s) {
            if (!t || !e) throw Error("cat and act is required by event(cat, act, params)");
            var i = this;
            return this.wait4init().then(function() {
              var a = {
                app: "minigame",
                cat: t,
                act: e,
                rid: u(16),
                lang: i.data.lang,
                os: i.data.os,
                osv: i.data.osv,
                make: i.data.make,
                model: i.data.model,
                w: i.data.width,
                h: i.data.height,
                did: i.data.uid,
                sub1: i.data.sdkv,
                sub2: i.data.pv,
                sub3: i.data.plv,
                sub4: i.data.utmsrc,
                subi1: i.data.gid,
                subi2: i.data.cid,
                subi3: i.data.its,
                subi7: i.data.inited
              };
              s && c(s, a);
              var r = "https://event.api.umgame.cn/api/v1/event?" + d(a, "&");
              return l.get(r);
            });
          };
          _proto7.onShareAppMessage = function onShareAppMessage(t, e) {
            var s = this;
            this.fetchShareQuery().then(function(i) {
              m(function() {
                var a = e();
                return s.shareMaterial.title && (a.title = s.shareMaterial.title), s.shareMaterial.image && (a.imageUrl = s.shareMaterial.image), 
                a.query ? a.query = a.query + "&" + i : a.query = i, a.query = a.query + "&ssid=" + t, 
                s.isShareDialogVisible = !0, s.event("user", "share", {
                  sub5: t,
                  subi10: s.shareMaterial.smid
                }), a;
              });
            }, function(t) {
              s.isShareDialogVisible = !0, m(e);
            });
          };
          _proto7.shareAppMessage = function shareAppMessage(t) {
            if (!t.slotId || !t.data) throw Error("`obj.slotId` and `obj.data` are required by shareAppMessage");
            var e = this;
            this.fetchShareQuery().then(function(s) {
              if (e.shareMaterial.title && (t.data.title = e.shareMaterial.title), e.shareMaterial.image && (t.data.imageUrl = e.shareMaterial.image), 
              t.data.query ? t.data.query = t.data.query + "&" + s : t.data.query = s, t.data.query = t.data.query + "&ssid=" + t.slotId, 
              e.isShareDialogVisible = !0, t.data.success) {
                var _s4 = t.data.success;
                t.data.success = function() {
                  e.event("user", "share", {
                    sub5: t.slotId,
                    subi10: e.shareMaterial.smid
                  }), _s4();
                }, p(t.data);
              } else p(t.data), e.event("user", "share", {
                sub5: t.slotId,
                subi10: e.shareMaterial.smid
              });
            }, function(s) {
              e.isShareDialogVisible = !0, p(t.data);
            });
          };
          _proto7.createIconAd = function createIconAd(t) {
            if (!t.slotId) throw Error("obj.slotId is required by createIconAd(obj)");
            return new v(t, this);
          };
          _proto7.createBannerAd = function createBannerAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createBannerAd(obj)");
            if (!t.slotId) throw Error("obj.slotId is required by createBannerAd(obj)");
            return new b(t, this);
          };
          _proto7.createInterstitialAd = function createInterstitialAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createInterstitialAd(obj)");
            if (!t.slotId) throw Error("obj.slotId is required by createInterstitialAd(obj)");
            return new g(t, this);
          };
          _proto7.createRewardedVideoAd = function createRewardedVideoAd(t) {
            if (!t.adUnitId) throw Error("obj.adUnitId is required by createRewardedVideoAd(obj);");
            if (!t.slotId) throw Error("obj.slotId is required by createRewardedVideoAd(obj)");
            return new w(t, this);
          };
          return _class2;
        }())();
      } ]);
    });
    cc._RF.pop();
  }, {} ],
  use_reversed_rotateTo: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "5e616CfixNG25IIwXmaUiai", "use_reversed_rotateTo");
    "use strict";
    cc.RotateTo._reverse = true;
    cc._RF.pop();
  }, {} ]
}, {}, [ "PolygonMask", "ExportAction", "ExportContent", "ExportHand", "ExportIcon", "ExportLayout", "ExportPage", "MoreGamePage", "OldMoreGamePage", "OldUsersPage", "ReturnPage", "SDKEnum", "Audio", "Config", "Databus", "Event", "ExportPageController", "Platform", "PopUp", "Tips", "Transition", "UmSdk", "User", "Utils", "PlatformConfig", "WebPlatform", "WxPlatform", "umsdk.wechat", "use_reversed_rotateTo", "MysticBoxView", "PopupGameEndPage", "ResManager", "UIManager", "Util", "Main", "SDKManager", "AdBanner", "AdInsert", "AdRecord", "AdVideo", "Analytics", "Vibrate", "umsdk.toutiao", "EnumDef", "GameData", "BasePipe", "BaseRole", "BendsPipe", "EmptyTank", "ExpansionPipe", "FashionRewardView", "GameEvent", "Joint", "Mission", "RoleBad", "RoleDevil", "RoleFish", "RoleHero", "RoleSnake", "RoleWife", "StraightPipe", "WashTank", "WaterTank", "AddMoneyView", "AddPowerView", "BeginView", "ComicView", "Fail2View", "FailView", "FashionItem", "FashionView", "GameLoading", "GameView", "GameViewOld", "LackPowerView", "RoomDetailView", "RoomItem", "RoomView", "SetView", "SignView", "TipsView", "WifeItem", "WifeView", "Win2View", "WinView", "MusicUtil" ]);