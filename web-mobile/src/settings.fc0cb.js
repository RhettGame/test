window._CCSettings = {
    platform: "web-mobile",
    groupList: [
        "default",
        "ui",
        "water"
    ],
    collisionMatrix: [
        [
            true,
            false,
            true
        ],
        [
            false,
            false,
            false
        ],
        [
            true,
            false,
            true
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/Scenes/Client.fire",
    orientation: "portrait",
    debug: true,
    jsList: [],
    bundleVers: {
        internal: "2e4d4",
        atlas: "43ea9",
        audio: "2620a",
        mission: "557e1",
        model: "f423e",
        ui: "1fb3b",
        resources: "b6e9e",
        main: "1e219"
    }
};
